export class App {

    #plugins;
    #pluginLut;
    callPluginMethod = (pluginName, methodName, ...args) =>
        this.#pluginLut[pluginName][methodName](...args);
    hasPlugin = pluginName => this.#pluginLut[pluginName];

    constructor(options) {
        this.#plugins = options.plugins;
        this.#pluginLut = options.plugins.reduce(
            (lut, p) => {
                const lcName = p.constructor.name.toLowerCase();
                /**/if (lut[lcName]) throw Error('new App(): 79803');
                return { ...lut, [lcName]:p };
            });
    }

    #didInit = false;
    didInit(pluginName) { // TODO use it or lose it
        if (! pluginName) return this.#didInit;
        /**/if (! this.hasPlugin(pluginName)) throw Error(pf+81943);
        return this.#pluginLut[pluginName].didInit;
    }

    init() {
        /**/const pf = 'App::init(): ';
        /**/if (this.#didInit) throw Error(pf+49817);
        this.#plugins.forEach(p => p.init(this));
        /**/this.#plugins.forEach(p=>{if(!p.didInit)throw Error(pf+55117)});
        this.#didInit = true;
        this.emit('app.didInit');
    }

    // Define a simple event hub.
    #events = {}

    on(eventName, handler) {
        this.#events[eventName] = this.#events[eventName] || [];
        this.#events[eventName].push(handler);
    }

    emit(eventName, payload) {
        var handlers = this.#events[eventName];
        if (handlers) for (var handler of handlers) handler(payload);
    }
}
