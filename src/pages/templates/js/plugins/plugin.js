export class Plugin {
    app;

    #didInit = false;
    get didInit() { return this.#didInit }

    init(app) {
        this.app = app;
        this.#didInit = true;
    }
}
