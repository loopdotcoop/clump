const { Plugin } = await import('./plugin.js');

// Stores handy references to various DOM elements.
export class Dom extends Plugin {

    // References to DOM elements.
    #sections = {};
    getSections() { return this.#sections } // TODO maybe protect this.#sections

    constructor() {
        super();
        /**/const pf = 'new Dom(): ';

        // Read this language's <section> elements from the hard-coded DOM.
        const $$sections = document.getElementsByTagName('section');
        /**/if ($$sections.length === 0) throw Error(pf+75785);
        Array.from($$sections).forEach($section => {
            const { id } = $section;
            /**/if (! id || ! /^[a-z]{2,9}$/.test(id)) throw Error(pf+10288);
            const section = { $: $section, cl: $section.classList, subs: {} };
            this.#sections[id] = section;
            const $$subsections = $section.getElementsByTagName('h3');
            /**/if ($$subsections.length === 0) throw Error(pf+35491);
            Array.from($$subsections).forEach($subsection => {
                const { id } = $subsection;
                /**/if (! id) throw Error(pf+64911); // TODO regexp test
                const subKey = id.split('/').pop();
                const subsection = { $: $subsection, parts: {} };
                section.subs[subKey] = subsection;
            });
            const $$parts = $section.getElementsByTagName('h4'); // 'parts' are optional
            Array.from($$parts).forEach($part => {
                const { id } = $part;
                /**/if (! id) throw Error(pf+88658); // TODO regexp test
                const [ subKey, partKey ] = id.split('/').slice(-2);
                /**/if (! section.subs[subKey]) throw Error(pf+49014);
                const part = { $: $part };
                section.subs[subKey].parts[partKey] = part;
            });
        });
    }

}
