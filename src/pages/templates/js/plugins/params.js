const { Plugin } = await import('./plugin.js');

// Manages the parameters of the 'parametric design' parts of the app.
export class Params extends Plugin {

    // Element refs.
    #links;
    #ids;

    // State.
    #currentEncodedParams = '0';

    constructor() {
        super();

        // Record the "data-suffix" attribute on all elements which will need
        // their href or id attribute updated when the URL hash's encoded
        // parameters change. The "data-suffix" attribute will be used by
        // onRouteChange().
        const cep = this.#currentEncodedParams;
        const cepLenPlus1 = cep.length + 1;
        this.#links = document.querySelectorAll('[href^="#/"]');
        this.#ids = document.querySelectorAll('[id^="/"]');
        const getSuffix = full => full.split('#').pop().slice(cepLenPlus1);
        this.#links.forEach($el =>
            $el.setAttribute('data-suffix', getSuffix($el.href)));
        this.#ids.forEach($el =>
            $el.setAttribute('data-suffix', getSuffix($el.id)));

    }

    init(app) {
        /**/const pf = 'Params::init(): ';

        //
        app.on('route.change', this.#onRouteChange.bind(this));

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }

    #onRouteChange({ data, from, to }) {
        const cep = this.#currentEncodedParams;

        // If `data` is `null` after any "route.change" event, it means that
        // the route is invalid.
        // The only time `from` will be `undefined` is as the page loads.
        // If `to` is also an empty string, it means the user has navigated
        // to this page with no hash ".../v1/homepage.html" or just the hash
        // ".../v1/homepage.html#".
        if (data === null || (! from && to === ''))
            return window.location.hash = '/' + cep;

        // If the encoded params has not changed, do nothing.
        const { encodedParams } = data;
        if (encodedParams === cep)
            return;

        // Otherwise the encoded params has changed. In that case, many of the
        // href and id attributes will need to be updated.
        this.#links.forEach($el =>
            $el.href = `#/${encodedParams}` + $el.getAttribute('data-suffix'));
        this.#ids.forEach($el =>
            $el.id = `/${encodedParams}` + $el.getAttribute('data-suffix'));

        this.#currentEncodedParams = encodedParams;
    }

}
