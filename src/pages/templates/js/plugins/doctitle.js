const { Plugin } = await import('./plugin.js');

// Updates the page's `document.title`.
// This is often shown in the page's tab, and in browser history and bookmarks.
export class Doctitle extends Plugin {

    // Initial title as the page loads, with Unicode 'HAIR SPACE' removed.
    #baseTitle = document.title.replace(/\u200A/g, '');

    constructor() {
        super();
        // this.#baseTitle = document.title;
    }

    init(app) {
        app.on('route.change', this.#onRouteChange.bind(this));

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }

    #onRouteChange({ data }) {
        // If `data` is `null` after any "route.change" event, it means that
        // the route is invalid.
        if (! data) return document.title = this.#baseTitle;
        const { secKey, subKey, partKey } = data;

        // Assemble the new title into an array of 1 to 4 items, eg:
        //   [ "LOOP.COOP / CLUMP", "list", "subsection-2", "part-22" ]
        const newTitle = [ this.#baseTitle ];
        if (secKey) newTitle.push(secKey);
        if (subKey) newTitle.push(subKey);
        if (partKey) newTitle.push(partKey);

        // Style the new title, and set it.
        const hairSpc = '\u200A'; // U+200A HAIR SPACE (see also U+2009 THIN SPACE)
        document.title = newTitle.join(' / ')
            .toUpperCase()
            .split('')
            .join(hairSpc);
    }

}
