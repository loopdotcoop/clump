const { Plugin } = await import('./plugin.js');

export class DevCore extends Plugin {

    // Define default core dev settings.
    #showSize = 'SHOW';

    init(app) {
        // Hook up a mutator for the dev's 'size' setting.
        app.on('set.dev.size', value => {
            value = value || this.#showSize; // first ever load
            if (! /^(HIDE|SHOW)$/.test(value)) throw Error('DevCore::init() 81162'); // TODO this shouldn't break the whole app
            this.#showSize = value;
            if (window.localStorage) localStorage.setItem('CLUMPv1.dev.size', value);
            document.body.classList[this.#showSize==='HIDE'?'remove':'add']('show-dev-size');
        });

        // As the page loads, initialize the dev's size choice based on the
        // value in localStorage (if it exists). Otherwise default to 'HIDE'.
        app.emit('set.dev.size',
            window.localStorage && localStorage.getItem('CLUMPv1.dev.size'));

        // Toggle 'dev.size' visibility when the dollar key '$' is pressed.
        window.addEventListener(
            'keypress',
            evt => evt.key === '$' && app.emit(
                'set.dev.size', this.#showSize === 'HIDE' ? 'SHOW' : 'HIDE')
        );

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }
}
