const { Plugin } = await import('./plugin.js');

export class SidebarCore extends Plugin {

    // The names of the current section, subsection and part.
    #secKey;
    #subKey; // TODO use it or lose it
    #partKey; // TODO use it or lose it

    // References to various elements in the DOM.
    // TODO NEXT Dom should create `body = { $:<ref>, cl:<ref>.classList }`
    #bodyCL;
    #sectionCLs;
    #topnavCL;

    init(app) {
        /**/const pf = 'Route::init(): ';

        //
        app.on('route.change', this.#onRouteChange.bind(this));

        /**/if (! document.body) throw Error(pf+82143);
        this.#bodyCL = document.body.classList;

        this.#sectionCLs = ['dev', 'help', 'like', 'list', 'user']
            .reduce(($s, id) => {
                /**/if (! document.getElementById(id)) throw Error(pf+10472);
                return { ...$s, [id]: document.getElementById(id).classList } }, {});

        /**/if (! document.getElementById('sidebar-topnav')) throw Error(pf+58071);
        this.#topnavCL = document.getElementById('sidebar-topnav').classList;

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }

    #onRouteChange({ data, from, to }) {
        if (! data) return; // not a valid route
        const { secKey } = data;
        if (this.#secKey === secKey) return; // already showing that section

        // If a sidebar section is currently showing, hide it.
        if (this.#secKey) {
            this.#bodyCL.remove('show-section-'+this.#secKey);
            this.#sectionCLs[this.#secKey].remove('clickable');
        }

        // If the new sidebar section is `null`, hide the sidebar and update
        // SidebarCore state.
        if (! secKey) {
            this.#bodyCL.remove('show-sidebar');
            this.#topnavCL.remove('clickable');
            this.#secKey = undefined;
            return;
        }

        // Otherwise, show the new section, and update SidebarCore state.
        this.#bodyCL.add('show-section-'+secKey);
        this.#bodyCL.add('show-sidebar');
        this.#sectionCLs[secKey].add('clickable');
        this.#topnavCL.add('clickable');
        this.#secKey = secKey;
    }
}
