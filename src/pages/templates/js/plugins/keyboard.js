const { Plugin } = await import('./plugin.js');

// Manages qwerty keyboard input.
export class Keyboard extends Plugin {

    init(app) {
        window.addEventListener('keydown', this.#onKeydown.bind(this));

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }

    #onKeydown(evt) {
        // Ignore any key which is pressed along with alt, ctrl or meta.
        const { altKey, ctrlKey, metaKey } = evt;
        if (metaKey || ctrlKey || altKey ) return;

        const KEY = evt.key.toUpperCase();
        const btn = {
            $:      'dev',
            ESCAPE: 'hide',
            '/':    'hide',
            '?':    'help',
            '*':    'like',
            '=':    'list',
            '\\':   'top',
            '^':    'user',
        }[KEY];

        if (btn) {
            evt.preventDefault(); // eg "/" would open 'Quick find' in Firefox
            const $btn = document.querySelectorAll('.clickable .btn-' + btn)[0];
            if (! $btn) return; // assume the button is not currently clickable
            // this.app.emit('keyboard.press.button', btn); // TODO use it or lose it
            $btn.click();
            $btn.classList.add('just-clicked'); // same CSS as `.btn:active` TODO also when focused and ENTER is pressed
            setTimeout(() => $btn.classList.remove('just-clicked'), 99);
            return;
        }
    }
}
