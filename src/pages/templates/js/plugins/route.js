const { Plugin } = await import('./plugin.js');

// Determines whether a value is a plain JavaScript object or instance.
// TODO use it or lose it
// const isObj = val => Object.prototype.toString.call(val) == '[object Object]';

// Manages the URL fragment, for dynamic routes.
export class Route extends Plugin {

    // DOM refs.
    #sections;

    // State.
    #currentHash;

    init(app) {
        // The `sections` object has already been created by the Dom plugin.
        this.#sections = app.callPluginMethod('dom', 'getSections');
        /**/if (! this.#sections) throw Error('Route::init() 66937');

        // When the app and all of the plugins have initialized, start polling
        // the address bar's hash 10 times a second, emitting "route.change" when
        // it changes. This is more controllable and cross-platform than the
        // native "hashchange" event fired on the browser window.
        const tick = () => {
            setTimeout(tick, 99);
            const from = this.#currentHash;
            const to = location.hash;
            if (from === to) return;
            const data = Route.parseHash(to, this.#sections);
            app.emit('route.change', { data, from, to });
            this.#currentHash = to;
        }
        app.on('app.didInit', tick);

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }

    static parseHash(hash, sections) {
        // eg ["#","pArAmS"] or ["#","1aZ_~","user","theme"].
        const [ hashSymbol, encodedParams, secKey, subKey, partKey ] = hash.split('/');
        if (hashSymbol !== '#') return null; // `hashSymbol` is '' if there is no hash
        if (! /^[_~\w]{1,99}$/.test(encodedParams)) return null;
        if (secKey && ! sections[secKey]) return null;
        if (subKey && ! sections[secKey].subs[subKey]) return null;
        if (partKey && ! sections[secKey].subs[subKey].parts[partKey]) return null;
        return { encodedParams, secKey, subKey, partKey };
    }

}
