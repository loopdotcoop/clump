const { Plugin } = await import('./plugin.js');

// Manages the <canvas> element and Rust app.
export class CanvasAndRust extends Plugin {

    // Elements.
    #$canvas = document.getElementsByTagName('canvas')[0];
    #$text = document.getElementById('text-canvas');

    // State.
    #prevNow; // previous value of `performance.now()`
    #prevCanvasWidth = this.#$canvas.offsetWidth;
    #prevCanvasHeight = this.#$canvas.offsetHeight;
    #tmpCanvasDimensions; // TODO NEXT move into the Rust app

    constructor() {
        super();
        this.#updateTmpCanvasDimensions(this.#prevCanvasWidth, this.#prevCanvasHeight);
    }

    init(app) {
        /**/if (! this.#$canvas) throw Error('new CanvasAndRust(): 91784');
        /**/if (! this.#$text) throw Error('new CanvasAndRust(): 19987');

        // Start driving the Rust app. TODO NEXT Rust app!
        this.#prevNow = window.performance.now();
        window.requestAnimationFrame(this.#tick.bind(this));

        super.init(app); // set `didInit` to `true` and keep a ref to `app`
    }

    #tick(newNow) {
        // Calculate the number of milliseconds since the last tick.
        const deltaNow = newNow - this.#prevNow;
        this.#prevNow = newNow; // ready for the next tick()

        // Check whether the <canvas> and #text-canvas should be resized.
        const newCanvasWidth = this.#$canvas.offsetWidth;
        const newCanvasHeight = this.#$canvas.offsetHeight;
        if (this.#prevCanvasWidth !== newCanvasWidth ||
            this.#prevCanvasHeight !== newCanvasHeight) {
            this.#prevCanvasWidth = newCanvasWidth;
            this.#prevCanvasHeight = newCanvasHeight;
            this.#updateTmpCanvasDimensions(newCanvasWidth, newCanvasHeight);
        }

        this.#$text.innerText =
            '\n ' + ~~(1000 / deltaNow) + ' FPS\n' +
            ' [][][]\n' +
            ' [][][]\n' +
            ' [][][]\n' +
            this.#tmpCanvasDimensions;

        // Schedule the next tick() call.
        window.requestAnimationFrame(this.#tick.bind(this));
    }

    #updateTmpCanvasDimensions(newCanvasWidth, newCanvasHeight) {
        this.#tmpCanvasDimensions =
            '+'.repeat(newCanvasWidth / 12) +
            '\n                / ' +
            ~~(newCanvasWidth / 12) +
            ' X ' +
            ~~(newCanvasHeight / 24) +
            '\n                /'.repeat(newCanvasHeight / 24 - 7);
    }
}
