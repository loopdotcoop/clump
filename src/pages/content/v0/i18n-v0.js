// src/pages/content/v0/i18n-v0.js

import { en } from './i18n-v0-en.js';
import { pt } from './i18n-v0-pt.js';

export const v0 = { en, pt };
