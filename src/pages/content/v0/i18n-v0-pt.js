// src/pages/content/v0/i18n-v0-pt.js

export const pt = {
    nav_title_ldc: 'Conheça os outros projetos do LOOP.COOP',
    noscript: 'O JavaScript deve estar ativado para usar este aplicativo',
    widen_your_window: 'Amplie sua janela, por favor',
};
