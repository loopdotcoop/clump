// src/pages/content/v0/i18n-v0-en.js

export const en = {
    nav_title_ldc: "Check out LOOP.COOP's other projects",
    noscript: 'JavaScript must be enabled to use this app',
    widen_your_window: 'Please widen your window!',
};
