// src/pages/content/v1/i18n-v1.js

import { en } from './i18n-v1-en.js';
import { es } from './i18n-v1-es.js';
import { pt } from './i18n-v1-pt.js';

export const v1 = { en, es, pt };
