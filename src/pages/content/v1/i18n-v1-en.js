// src/pages/content/v1/i18n-v1-en.js

import { en as v0 } from '../v0/i18n-v0-en.js';

export const en = {
    ...v0,
    nav_title_dev: 'Developer tools',
    nav_title_help_homepage: 'Find out how to explore the CLUMP world',
    nav_title_like_homepage: 'Bookmark, save or share the current LOCATION',
    nav_title_list_expert: 'Browse collections of LOCATIONS, PATCHES, SEQS, SYNTHS and TUNES',
    nav_title_list: 'Browse collections of LOCATIONS, PATCHES and TUNES',
    nav_title_user: 'Change settings like language and dark-mode',
    sidebar_title_hide: 'Hide the sidebar',
    sidebar_title_top: 'Scroll to top',
};
