// src/pages/content/v1/i18n-v1-es.js

export const es = {
    nav_title_dev: 'Developer tools TODO es',
    nav_title_help_homepage: 'Descubra cómo explorar el mundo CLUMP',
    nav_title_ldc: 'Conozca los otros proyectos de LOOP.COOP',
    nav_title_like_homepage: 'Marcar, guardar o compartir la LOCATION actual',
    nav_title_list_expert: 'Explore colecciones de LOCATIONS, PATCHES, SEQS, SYNTHS y TUNES',
    nav_title_list: 'Explore colecciones de LOCATIONS, PATCHES y TUNES',
    nav_title_user: 'Cambiar configuraciones como idioma y modo oscuro',
    noscript: 'JavaScript debe estar habilitado para usar esta aplicación',
    sidebar_title_hide: 'Ocultar la barra lateral',
    sidebar_title_top: 'Vuelve al comienzo',
    widen_your_window: 'Amplía tu ventana por favor',
};
