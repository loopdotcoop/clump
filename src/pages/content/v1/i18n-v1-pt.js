// src/pages/content/v1/i18n-v1-pt.js

import { pt as v0 } from '../v0/i18n-v0-pt.js';

export const pt = {
    ...v0,
    nav_title_dev: 'Developer tools TODO pt',
    nav_title_help_homepage: 'Descubra como explorar o mundo CLUMP',
    nav_title_like_homepage: 'Marque, salve ou compartilhe o LOCATION atual',
    nav_title_list_expert: 'Navegue por coleções de LOCATIONS, PATCHES, SEQS, SYNTHS e TUNES',
    nav_title_list: 'Navegue por coleções de LOCATIONS, PATCHES e TUNES',
    nav_title_user: 'Alterar configurações como idioma e modo escuro',
    sidebar_title_hide: 'Ocultar a barra lateral',
    sidebar_title_top: 'Role para cima',
};
