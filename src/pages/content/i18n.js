// src/pages/content/i18n.js

import { v0 } from './v0/i18n-v0.js';
import { v1 } from './v1/i18n-v1.js';

export const i18n = { v0, v1 };
