// src/pages/config.js

import { i18n } from './content/i18n.js';

const appV0 = {
    'index.html': {
        content: 'v0/homepage-{{languageCode}}.md', // eg "Down for maintenance"
        template: 'v0/homepage.html',
    },
};

const appV1 = {
    'index.html': {
        content: 'v1/homepage-{{languageCode}}.md',
        hasSidebar: true, // TODO validate and unit test `hasSidebar`
        template: 'v1/homepage.html',
    },
};

export const config = {
    landingPage: { // /index.html redirects to v0 for now - "Coming Soon"
        i18n: i18n.v0,
        redirectTo: 'v0/index.html', // TODO initially, 'v0/index.html'
        template: 'redirect.html',
    },
    notFound: { // /404.html shows a "404 Not Found" page
        i18n: i18n.v0,
        template: '404.html',
    },
    versions: [
        { // /v0/index.html redirects to the detected language
            i18n: i18n.v0,
            redirectTo: 'en/index.html',
            template: 'redirect-to-language.html',
            languages: {
                en: appV0, // /v0/en/index.html
                pt: appV0, // /v0/pt/index.html
            },
        },
        { // /v1/index.html redirects to the detected language
            i18n: i18n.v1,
            redirectTo: 'en/index.html',
            template: 'redirect-to-language.html',
            languages: {
                en: appV1, // /v1/en/...
                es: appV1, // /v1/es/...
                pt: appV1, // /v1/pt/...
            },
        },
    ],
};
