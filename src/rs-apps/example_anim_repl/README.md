<!-- src/rs-apps/example_anim_repl/README.md -->

# example_anim_repl

> An example Rust app which shows an animation, and runs a read-eval-print loop

- 0.0.1
- Created February 2024, in Lisbon, Portugal, by Richi Plus
- <https://gitlab.com/loopdotcoop/clump/>
- <https://loopdotcoop.gitlab.io/clump/index.html>

example_anim_repl is a Rust app which can be:

1. __Compiled to a standalone CLI binary for the current platform:__  
   dist/example_anim_repl/cli-binary/
2. __Compiled to a WebAssembly .wasm file for the Node.js runtime:__  
   dist/example_anim_repl/node-wasm/
3. __Compiled to a WebAssembly .wasm file for web browsers:__  
   dist/example_anim_repl/web-browser-wasm/

It's confirmed to work on:

- macOS Monterey
- ~~Windows 11 PowerShell~~ TODO check
- ~~Windows Subsystem for Linux (wsl)~~ TODO check

## Run example_anim_repl from source, during development

Show the help page:

```sh
# From the root of the 'clump' repo:
npm run rs -- example_anim_repl --help
node scripts/run-rs.js example_anim_repl -h
# Or from the src/rs-apps/example_anim_repl/ folder:
cargo run -- --help
```

Print a greeting:

```sh
# From the root of the 'clump' repo:
npm run rs -- example_anim_repl --who "clump developer"
node scripts/run-rs.js example_anim_repl -w "clump developer"
# Or from the src/rs-apps/example_anim_repl/ folder:
cargo run -- --who "clump developer"
```

## Handy commands

Build and open the example_anim_repl documentation (without dependencies):

```sh
# From the root of the 'clump' repo:
npm run doc:rs -- example_anim_repl --open
node scripts/doc-rs.js example_anim_repl --open
# Or from the src/rs-apps/example_anim_repl/ folder:
cargo clean --doc
cargo doc --no-deps --open
```

Run the example_anim_repl unit tests and integration tests:

```sh
# From the root of the 'clump' repo:
npm run test:internal:rs -- example_anim_repl
node scripts/run-rs-internal-tests.js example_anim_repl
# Or from the src/rs-apps/example_anim_repl/ folder:
cargo test
```

Periodically, make sure Rust is using up-to-date dependencies:

```sh
# From the root of the 'clump' repo:
npm run update:rs -- example_anim_repl
node scripts/update-rs.js example_anim_repl
# Or from the src/rs-apps/example_anim_repl/ folder:
cargo update --dry-run # for extra safety
cargo update
```

## Set up your machine

To set up your machine, see ['Set up your machine' in the root README.md
](../../../README.md#set-up-your-machine) file.

## Build, test and dev

### Build just the example_anim_repl Rust app and/or library

To build only example_anim_repl, use the NPM script, or invoke build-rs.js directly:

```sh
npm run build:rs -- example_anim_repl
node scripts/build-rs.js example_anim_repl
# ------------------------------------------------------------------------------
# Running buildCliBinary('example_anim_repl')...
#  OK  rustc succeeded
# Running buildNodeWasm('example_anim_repl')...
# ...
#  OK  wasm-pack build succeeded
# Running buildNodeWrapper('example_anim_repl')...
#  OK  Created example_anim_repl-cli.js, example_anim_repl-wrapper.js
# Running buildWebBrowserWasm('example_anim_repl')...
# ...
#  OK  All three 'example_anim_repl' builds succeeded
# ==============================================================================
#  Done!  3 builds for 1 Rust app and/or library
# ==============================================================================
```

It is also possible to build all the rs-apps using one command.  
See ['Build all the rs-apps'](
../../../README.md#build-all-the-rs-apps) in the root README.md file.

### Run just the 'example_anim_repl' JavaScript end-to-end tests

To test only example_anim_repl, use the NPM script, or invoke run-rs-e2e-tests.js
directly:

```sh
npm run test:e2e:rs -- example_anim_repl
node scripts/run-rs-e2e-tests.js example_anim_repl
# ------------------------------------------------------------------------------
# Running testCliBinary('example_anim_repl')...
#  PASS  Both 'example_anim_repl' e2e cli-binary tests passed
# Running testNodeWasm('example_anim_repl')...
#  PASS  Both 'example_anim_repl' e2e node-wasm tests passed
# ==============================================================================
#  Done!  2 e2e test suites for 1 Rust apps
#         Web browser .wasm files will also need to be tested in the browser,
#         as part of the end-to-end and integration tests of the HTML pages.
#         Alternatively the web browser .wasm files can be tested using:
#         node scripts/run-rs-e2e-tests/test-web-browser-wasm.js example_anim_repl
# ==============================================================================
```

It is also possible to run all the Rust e2e tests.  
See ['Run all the Rust app and/or library e2e tests'](
../../../README.md#run-all-the-rust-e2e-tests) in the root README.md file.

### Run just the 'example_anim_repl' Rust unit and integration tests

To test only example_anim_repl, use the NPM script, or invoke run-rs-internal-tests.js
directly:

```sh
npm run test:internal:rs -- example_anim_repl
node scripts/run-rs-internal-tests.js example_anim_repl
#------------------------------------------------------------------------------
#running 2 tests
#test config::config_try_from::tests::config_try_from_errors ... ok
#test config::config_try_from::tests::config_try_from_ok ... ok
#test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
#running 2 tests
#test config::config_try_from::tests::config_try_from_errors ... ok
#test config::config_try_from::tests::config_try_from_ok ... ok
#test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
#running 0 tests
#test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
# PASS  'example_anim_repl' internal tests passed
#==============================================================================
# PASS  The Rust crate 'example_anim_repl' internal tests passed
#==============================================================================
```

It is also possible to run all the Rust unit and integration tests.  
See ['Run all the Rust app and/or library unit and integration tests'](
../../../README.md#run-all-the-rust-unit-and-integration-tests)
in the root README.md file.
