// src/rs-apps/example_anim_repl/tests/e2e/web-browser-wasm.js

import { commonE2eTests } from './example_anim_repl-common.js';

export const testE2eWebBrowserWasm = ({ equal }, wasm) => {
    const { example_anim_repl } = wasm;

    const e2eTests = commonE2eTests({ equal }, example_anim_repl);

    const results = e2eTests.map((e2eTest) => {
        const { attempt, exp } = e2eTest;
        const kind = e2eTest.equal ? 'equal' : 'UNKNOWN!';
        let actually, expected; try {
            actually = attempt(false);
            expected = typeof exp === 'function' ? exp(false) : exp;
            switch (kind) { // TODO more kinds of assertion
                case 'equal': e2eTest.equal(actually, expected); break;
                default: throw Error(`No such assertion kind '${kind}'`);
            }
        } catch (err) { return { actually, err, expected, kind } }
        return { actually, err:null, expected, kind };
    });

    return results;
};
