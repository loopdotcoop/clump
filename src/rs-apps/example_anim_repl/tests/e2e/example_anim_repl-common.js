// src/rs-apps/example_anim_repl/tests/e2e/example_anim_repl-common.js

export const commonE2eTests = ({ equal }, example_anim_repl) => [
    // {
    //     equal: (a, e) => equal(a, e), // provides location in the stack
    //     attempt: _ => example_anim_repl(''),
    //     exp: 'A 80x24 canvas, filled with "."\n' +
    //         ('.'.repeat(80) + '\n').repeat(24).slice(0, -1),
    // },
    // {
    //     equal: (a, e) => equal(a, e),
    //     attempt: _ => example_anim_repl("--columns \"1\" --rows 20 --fill 'X'"),
    //     exp: 'A 1x20 canvas, filled with "X"\n' +
    //         'X\n'.repeat(20).slice(0, -1),
    // },
    {
        equal: (a, e) => equal(a, e),
        attempt: _ => example_anim_repl('-f "#" -c 10 -r 5'),
        exp: 'A 10x5 canvas, filled with "#"\n' +
            ('#'.repeat(10) + '\n').repeat(5).slice(0, -1),
    },
    {
        equal: (a, e) => equal(a, e),
        attempt: _ => example_anim_repl('--version'),
        exp: isCli => isCli
            ? "example_anim_repl 0.0.1"
            : JSON.stringify({ TODO: 'NEXT' }),
    },
    {
        equal: (a, e) => equal(a, e),
        attempt: _ => example_anim_repl('--no-such-option'),
        exp: isCli => isCli
            ? "Error 2_1028 64:\n" +
              "    Key '--no-such-option' not recognised. For help, try:\n" +
              '    example_anim_repl --help'
            : JSON.stringify({
                code: 2_1028,
                status: 64, // EX_USAGE
                text: "Key '--no-such-option' not recognised. For help, try:\n" +
                    "example_anim_repl('--help')",
            }),
    },
];
