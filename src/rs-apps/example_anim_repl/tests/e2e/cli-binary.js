// src/rs-apps/example_anim_repl/tests/e2e/cli-binary.js

import { commonE2eTests } from './example_anim_repl-common.js';

export const testE2eCliBinary = ({ equal, execSync }, path) => {
    const example_anim_repl = raw_args => execSync(
        `${path} ${raw_args}`
    ).toString().slice(0, -1); // remove the trailing newline

    const e2eTests = commonE2eTests({ equal }, example_anim_repl);

    const results = e2eTests.map((e2eTest) => {
        const { attempt, exp } = e2eTest;
        const kind = e2eTest.equal ? 'equal' : 'UNKNOWN!';
        let actually, expected; try {
            actually = attempt(true);
            expected = typeof exp === 'function' ? exp(true) : exp;
            switch (kind) { // TODO more kinds of assertion
                case 'equal': e2eTest.equal(actually, expected); break;
                default: throw Error(`No such assertion kind '${kind}'`);
            }
        } catch (err) { return { actually, err, expected, kind } }
        return { actually, err:null, expected, kind };
    });

    return results;
};
