//! src/rs-apps/example_anim_repl/src/loglines/loglines_new.rs
//! ### `Loglines::new()`
//!
//! Creates a Loglines instance.

// Local imports.
use super::Loglines;

impl Loglines {

    /// Creates a Loglines instance.
    pub fn new() -> Result<Self, (u16, u8, String)> {

        Ok(Self {
            loglines: vec![],
        })
    }
}
