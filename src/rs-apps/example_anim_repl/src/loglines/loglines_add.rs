//! src/rs-apps/example_anim_repl/src/loglines/loglines_add.rs
//! ### `Loglines::add()`
//!
//! TODO describe.

// Local imports.
use super::Loglines;

impl Loglines {

    //! TODO describe.
    pub fn add(&mut self, logline: String) -> Result<&Self, (u16, u8, String)> {

        // The line is valid, so append it to the vector.
        self.loglines.push(logline);

        Ok(self)
    }
}
