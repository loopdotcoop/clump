//! src/rs-apps/example_anim_repl/src/loglines/loglines_clear.rs
//! ### `Loglines::clear()`
//!
//! TODO describe.

// Local imports.
use super::Loglines;

impl Loglines {

    /// TODO describe.
    pub fn clear(&mut self) -> Result<(), (u16, u8, String)> {

        // Permanently delete everything that has been logged so far.
        self.loglines = vec![];

        Ok(())
    }
}
