//! src/rs-apps/example_anim_repl/src/loglines/mod.rs
//! ### `loglines`
//!
//! TODO describe.

// Export the Loglines struct.
mod loglines;
pub use self::loglines::Loglines;

// Add static and instance methods to Loglines.
mod loglines_new;
// mod loglines_render;
mod loglines_add;
// mod loglines_clear;
