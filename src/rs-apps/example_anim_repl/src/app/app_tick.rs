//! src/rs-apps/example_anim_repl/src/app/app_tick.rs
//! ### `App::tick()`
//!
//! TODO describe.

// Local imports.
use super::{app_enum_outer_op::OuterOp, App};

impl App {

    /// TODO describe.
    pub fn tick(
        &mut self,
        interval_ms: u128,
        recent_text: String,
    ) -> Result<OuterOp, (u16, u8, String)> {
        let columns = self.canvas.columns;
        let fill = self.canvas.fill;
        let rows = self.canvas.rows;

        // If there was any stdin since the last `tick()`, record it.
        // If the stdin contains newlines, set_text() will run some commands.
        // If a command is "exit", tick() should return `true`.
        let outer_op = self.set_text(recent_text).unwrap();

        // Start with a blank canvas.
        self.canvas.clear().unwrap();

        // If in command-line mode, draw the "   > " before the command-line text.
        let mut r = self.canvas.rows - 1; // bottom row
        if columns > 0 { self.canvas.grid[r][0] = fill }
        if columns > 1 { self.canvas.grid[r][1] = fill }
        if columns > 2 { self.canvas.grid[r][2] = fill }
        if columns > 3 { self.canvas.grid[r][3] = ' ' }
        if columns > 4 { self.canvas.grid[r][4] = '>' }
        if columns > 5 { self.canvas.grid[r][5] = ' ' }

        // If in command-line mode, draw the command-line text.
        let mut c = 6; // next column after the "   > "
        for ch in self.commandline.chars() {
            if c >= columns { break }
            self.canvas.grid[r][c] = ch;
            c += 1;
        }

        // If in command-line mode, draw a blinking cursor.
        self.uptime += interval_ms;
        let secs = (self.uptime + 500) / 1000;
        if secs % 2 == 0 && c < columns && r < rows {
            self.canvas.grid[self.cursor.1][self.cursor.0 + 6] = '_'
        }

        // If in command-line mode, draw the lines of logged text.
        for (line_num, logline) in self.loglines.loglines.iter().enumerate().rev() {
            if r == 0 { break }
            r -= 1; // move up one line
            let line_num_string = format!("{: >3}", line_num + 1);
            let line_num_chars = line_num_string.chars();
            let mut c = 0;
            for (i, ch) in line_num_chars.enumerate() {
                c = i;
                if c >= columns { break }
                self.canvas.grid[r][c] = if ch == ' ' { fill } else { ch };
            }
            c += 1;
            self.canvas.grid[r][c] = ' ';
            for ch in logline.chars() {
                c += 1;
                if c >= columns { break }
                self.canvas.grid[r][c] = ch;
            }
        }

        // Pass the result of calling set_text() to the caller.
        Ok(outer_op)
    }
}
