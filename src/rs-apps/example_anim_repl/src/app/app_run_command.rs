//! src/rs-apps/example_anim_repl/src/app/app_run_command.rs
//! ### `App::run_command()`
//!
//! TODO describe.

// Local imports.
use super::{app_enum_outer_op::OuterOp, App};

impl App {

    /// TODO describe.
    pub fn run_command(
        &self,
        commandline: String,
    ) -> Result<(OuterOp, String), (u16, u8, String)> {
        match commandline.as_str() {
            "exit" => Ok((OuterOp::ShouldExit, "OK, exiting gracefully".into())),
            "help" => Ok((OuterOp::NoOperation, "Help!!".into())),
            _ => Ok((OuterOp::NoOperation, "Huh?".into())),
        }
    }
}
