//! src/rs-apps/example_anim_repl/src/app/app_set_text.rs
//! ### `App::set_text()`
//!
//! TODO describe.

// Local imports.
use super::{app_enum_outer_op::OuterOp, App};

impl App {

    /// TODO describe.
    pub fn set_text(
        &mut self,
        text: String,
    ) -> Result<OuterOp, (u16, u8, String)> {

        for ch in text.chars() {
            if (ch >= ' ' && ch <= '~') || ch == 'é' { // from 32 to 126, or lowercase e-acute
                self.commandline.push(ch);
                self.cursor.0 += 1;
            } else if ch == '\n' { // newline
                let commandline = format!("{}", self.commandline);
                let commandline_fmt = format!("> {}", commandline);
                let (outer_op, response_text) = self.run_command(commandline)?;
                if outer_op == OuterOp::ShouldExit {
                    self.loglines.add(format!("{} -> {}", commandline_fmt, response_text))?;
                } else {
                    self.loglines.add(commandline_fmt)?;
                    self.loglines.add(response_text)?;
                }
                self.commandline = String::new();
                self.cursor.0 = 0;
                if outer_op == OuterOp::ShouldExit { return Ok(OuterOp::ShouldExit) }
            } else if ch == '\x7f' || ch == '\x08' { // POSIX or Windows delete
                self.commandline.pop();
                if self.cursor.0 != 0 { self.cursor.0 -= 1 }
            }
        }

        Ok(OuterOp::NoOperation)
    }
}
