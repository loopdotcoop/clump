//! src/rs-apps/example_anim_repl/src/app/app_render.rs
//! ### `App::render()`
//!
//! TODO describe.

// Local imports.
use super::App;

impl App {

    /// TODO describe.
    pub fn render(&self) -> Result<String, (u16, u8, String)> {
        // print!("{}", self.uptime);
        self.canvas.render(self.is_cli)
    }
}
