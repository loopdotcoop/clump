//! src/rs-apps/example_anim_repl/src/app/app_enum_outer_op.rs
//! ### `OuterOp`
//!
//! Category of action that the outer-app ought to perform.

/// Category of action that the outer-app ought to perform.
#[derive(PartialEq)]
pub enum OuterOp {
    /// Running the command did not produce an actionable result.
    NoOperation,
    /// The app should quit, eg after receiving an `exit` command.
    ShouldExit,
}
