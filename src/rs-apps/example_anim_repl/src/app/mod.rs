//! src/rs-apps/example_anim_repl/src/app/mod.rs
//! ### `app`
//!
//! TODO describe.

// Export the App struct.
mod app;
pub use self::app::App;

// Export enums and types.
pub mod app_enum_outer_op;

// Add static and instance methods to App.
mod app_new;
mod app_render;
mod app_run_command;
mod app_set_text;
mod app_tick;
