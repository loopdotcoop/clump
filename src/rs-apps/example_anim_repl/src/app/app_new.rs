//! src/rs-apps/example_anim_repl/src/app/app_new.rs
//! ### `App::new()`
//!
//! Creates an App instance.

// Local imports.
use super::App;
use crate::basic_ascii_canvas::BasicAsciiCanvas;
use crate::config::Config;
use crate::loglines::Loglines;

impl App {

    /// Creates an App instance.
    pub fn new(config: Config, is_cli: bool) -> Result<Self, (u16, u8, String)> {

        // Use default values if any optional config is missing.
        let columns = if config.columns == 0 { 80 } else { config.columns as usize };
        let fill = if config.fill == '\0' { '.' } else { config.fill };
        let rows = if config.rows == 0 { 24 } else { config.rows as usize };

        // Create a new BasicAsciiCanvas instance.
        let canvas = BasicAsciiCanvas::new(
            columns,
            fill,
            rows,
        )?;

        // Create a new Loglines instance.
        let mut loglines = Loglines::new()?;
        loglines.add("Enter commands, eg 'help' or 'exit'".into())?;

        Ok(Self {
            canvas,
            commandline: String::new(),
            cursor: (0, rows - 1),
            is_cli,
            loglines,
            uptime: 0,
        })
    }
}
