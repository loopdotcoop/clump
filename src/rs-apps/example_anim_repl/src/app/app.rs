//! src/rs-apps/example_anim_repl/src/app/app.rs
//! ### `App`
//!
//! TODO describe.

// Local imports.
use crate::basic_ascii_canvas::BasicAsciiCanvas;
use crate::loglines::Loglines;

/// TODO describe.
#[derive(Clone,Debug,Default)]
pub struct App {
    pub canvas: BasicAsciiCanvas,
    pub commandline: String,
    pub cursor: (usize, usize),
    pub is_cli: bool,
    pub loglines: Loglines,
    pub uptime: u128, // in milliseconds
}
