//! src/rs-apps/example_anim_repl/src/basic_ascii_canvas/basic_ascii_canvas_clear.rs
//! ### `BasicAsciiCanvas::clear()`
//!
//! Resets the canvas, by filling it with the --fill character.

// Local imports.
use super::BasicAsciiCanvas;

impl BasicAsciiCanvas {

    //! Resets the canvas, by filling it with the --fill character.
    pub fn clear(&mut self) -> Result<&Self, (u16, u8, String)> {
        for y in 0..self.rows as usize {
            for x in 0..self.columns as usize { self.grid[y][x] = self.fill }
        }
        Ok(self)
    }
}
