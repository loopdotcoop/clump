//! src/rs-apps/example_anim_repl/src/basic_ascii_canvas/mod.rs
//! ### `basic_ascii_canvas`
//!
//! TODO describe.

// Export the BasicAsciiCanvas struct.
mod basic_ascii_canvas;
pub use self::basic_ascii_canvas::BasicAsciiCanvas;

// Add static and instance methods to BasicAsciiCanvas.
mod basic_ascii_canvas_clear;
mod basic_ascii_canvas_new;
mod basic_ascii_canvas_render;
