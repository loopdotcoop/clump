//! src/rs-apps/example_anim_repl/src/basic_ascii_canvas/basic_ascii_canvas_new.rs
//! ### `BasicAsciiCanvas::new()`
//!
//! Creates a BasicAsciiCanvas instance.

// Local imports.
use super::BasicAsciiCanvas;

impl BasicAsciiCanvas {

    /// Creates a BasicAsciiCanvas instance.
    pub fn new(columns: usize, fill: char, rows: usize) -> Result<Self, (u16, u8, String)> {
        let mut grid: Vec<Vec<char>> = vec![];
        for _y in 0..rows {
            let mut row: Vec<char> = vec![];
            for _x in 0..columns { row.push(fill) }
            grid.push(row);
        }
        Ok(Self {
            columns,
            fill,
            rows,
            grid,
        })
    }
}
