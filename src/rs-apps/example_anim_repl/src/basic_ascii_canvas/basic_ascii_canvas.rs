//! src/rs-apps/example_anim_repl/src/basic_ascii_canvas/basic_ascii_canvas.rs
//! ### `BasicAsciiCanvas`
//!
//! TODO describe.

/// TODO describe.
#[derive(Clone,Debug,Default)]
pub struct BasicAsciiCanvas {
    pub columns: usize, // usize simplifies out-of-bounds test followed by `grid[r][c]`
    pub fill: char,
    pub rows: usize,
    pub grid: Vec<Vec<char>>,
}
