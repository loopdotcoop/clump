//! src/rs-apps/example_anim_repl/src/basic_ascii_canvas/basic_ascii_canvas_render.rs
//! ### `BasicAsciiCanvas::render()`
//!
//! TODO describe.

// Local imports.
use super::BasicAsciiCanvas;

impl BasicAsciiCanvas {

    /// TODO describe.
    pub fn render(&self, is_cli: bool) -> Result<String, (u16, u8, String)> {
        let mut rendered_rows: Vec<String> = vec![];
        for row in &self.grid {
            rendered_rows.push(row.iter().collect());
        }
        let rendered_grid: String = rendered_rows.join("\n");

        // TODO prepare for JSON.
        if ! is_cli { return Ok(rendered_grid) }

        // Use '\x1bc' to clear the screen. Windows terminals like PowerShell
        // would keep scrolling down on every `tick()` if '\x1b[2J' (clear
        // screen), and '\x1b[H' (send cursor to top left) were used.
        Ok(format!("\x1bc{}", rendered_grid))

        // // Use '\x1b[2J' to clear the screen, and '\x1b[H' to send the cursor to
        // // the home position (top left).
        // Ok(format!("\x1b[2J\x1b[H{}", rendered_grid))
    }
}
