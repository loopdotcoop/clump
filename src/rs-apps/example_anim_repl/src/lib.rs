//! src/rs-apps/example_anim_repl/src/lib.rs
//! ### `lib`
//!
//! Node.js and web browser .wasm entrypoint for the `example_anim_repl` Rust library.
//! #[cfg(target_family = "wasm")]

extern crate console_error_panic_hook;
use wasm_bindgen::prelude::wasm_bindgen; // used by wasm-pack to generate JS bindings

// src/rs-libs/ dependencies.
use parse::parse_args::parse_args;

// Local imports.
mod app; use app::App;
mod basic_ascii_canvas;
mod config; use config::{Config,config_show_help,config_show_version};
mod loglines;

#[wasm_bindgen] // marks this struct to be exported by wasm-pack
pub struct ExampleAnimRepl {
    pub(self) app: App,
}

#[wasm_bindgen]
impl ExampleAnimRepl {

    pub fn new(raw_args: &str) -> Self {

        // If a panic occurs, pass it to the browser’s `console.error()`.
        // See <https://github.com/rustwasm/console_error_panic_hook#usage>
        console_error_panic_hook::set_once();

        let mut wasm_errors: Vec<String> = vec![];

        // Parse the arguments, and deal with simple commands.
        let args = match parse_args(raw_args.into()) { // TODO parse_args() should accept String or &str
            Err(err) => {
                wasm_errors.push(format_wasm_error(err));
                vec![]
            },
            Ok(ok) => ok,
        };
        let config = match Config::try_from(args) {
            Err(err) => {
                wasm_errors.push(format_wasm_error(err));
                Config::try_from(vec![]).unwrap() // assume this always works!
            },
            Ok(ok) => ok,
        };

        let show_help = config.show_help;
        let show_version = config.show_version;
        let mut app = App::new(config, false).unwrap();

        if show_help { let _ = app.set_text(config_show_help()).unwrap(); }
        if show_version { let _ = app.set_text(config_show_version()).unwrap(); }

        Self { app }
    }

    #[wasm_bindgen]
    pub fn tick(&mut self, delta_ms: u32, recent_text: String) -> String {
        self.app.tick(delta_ms as u128, recent_text).unwrap();
        "ticked!".into()
    }

    #[wasm_bindgen]
    pub fn render(self) -> String {
        self.app.render().unwrap()
    }

}

/*
/// Shows the user an error message when something goes wrong.
/// TODO move to a new rs-lib called 'error'
fn handle_error((code, status, text): (u16, u8, String)) -> String {
    // `EX_USAGE` is error status 64, meaning the user supplied invalid CLI args.
    let h_1 = "For help, try:";
    let h_2 = "example_anim_repl";
    let h_3 = "--help";
    match status {
        EX_USAGE => format!("{{\"code\":{},\"status\":64,\"text\":\"{}. {}\\n{}('{}')\"}}", code, text, h_1, h_2, h_3),
        status => format!("{{\"code\":{},\"status\":{},\"text\":\"{}\"}}", code, status, text),
    }
}
*/

/// Shows the user an error message when something goes wrong.
/// TODO move to a new rs-lib called 'error'
fn format_wasm_error((code, status, text): (u16, u8, String)) -> String {
    format!("{{\"code\":{},\"status\":{},\"text\":\"{}\"}}", code, status, text)
}
