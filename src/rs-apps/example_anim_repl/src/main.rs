//! src/rs-apps/example_anim_repl/src/main.rs
//! ### `main`
//!
//! Command-line entrypoint for the `example_anim_repl` Rust app.
//! #[cfg(not(target_family = "wasm"))]

// Standard Rust.
use std::io::{Read,stdin};
use std::sync::mpsc::{channel,Receiver};
use std::str;
use std::thread::{sleep,spawn};
use std::time::{Duration, Instant};

// src/rs-libs/ dependencies.
use constants::err_status::EX_USAGE;
use terminal; use terminal::raw_mode;

// Local imports.
mod app; use app::{app_enum_outer_op::OuterOp,App};
mod basic_ascii_canvas;
mod config; use config::{Config,config_show_help,config_show_version};
mod loglines;

// ### `spawn_stdin_channel()`
// Reads standard-input on a different thread from the main render loop, which
// prevents `read_exact()` from blocking the animation.
// See <https://stackoverflow.com/a/55201400>
fn spawn_stdin_channel() -> Receiver<[u8; 1]> {
    let (tx, rx) = channel::<[u8; 1]>();
    spawn(move || loop {
        let mut buffer = [0];
        stdin().read_exact(&mut buffer).unwrap();
        tx.send(buffer).unwrap();
    });
    rx
}

/// ### `main()`
/// Responds to command line invocation, eg `example_anim_repl --who somebody`.
fn main() -> Result<(), (u16, u8, String)> {

    // Parse the arguments, and deal with simple commands.
    // Note that `args[0]` is just the path to the binary.
    let args: Vec<String> = std::env::args().skip(1).collect();
    let config = match Config::try_from(args) {
        Err(err) => { println!("{}", format_cli_error(err)); return Ok(()) }, // TODO return the error?
        Ok(ok) => ok,
    };
    if config.show_help { println!("{}", config_show_help()); return Ok(()) }
    if config.show_version { println!("{}", config_show_version()); return Ok(()) }

    // Put the terminal in 'raw mode', because this app handles all keypresses.
    let already_had_raw_mode_enabled = raw_mode::is_enabled()?;
    if ! already_had_raw_mode_enabled { raw_mode::enable()? }

    // Start reading standard-input.
    let stdin_channel = spawn_stdin_channel();

    // Create the App instance.
    let mut app = App::new(config, true).unwrap();

    // Start the render loop.
    let interval = Duration::from_millis(50); // > 20 fps
    // let interval = Duration::from_millis(16); // > 60 fps
    // let interval = Duration::from_millis(1000); // 1 fps
    let mut next_tick = Instant::now() + interval;
    loop {
        // Sleep for a 60th of a second.
        let now = Instant::now();
        sleep(next_tick - now);
        next_tick += interval;

        // Get the stdin characters entered since the last `tick()`.
        let mut recent_bytes: Vec<u8> = vec![];
        while let Ok(received) = stdin_channel.try_recv() {
            recent_bytes.push(received[0]);
        }
        let recent_text = match str::from_utf8(&recent_bytes) {
            Ok(val) => val,
            Err(err) => panic!("Invalid UTF-8 sequence: {}", err),
        };

        // Send the delta-time to the app, along with any recently entered text.
        let outer_op = app.tick(now.elapsed().as_millis(), recent_text.into()).unwrap();

        // Render the ASCII canvas.
        println!("{}", app.render().unwrap());
        // println!("{}\n\n{:?}", app.render(true).unwrap(), recent_text)

        // If the app wants to exit, break out of this infinite loop.
        if outer_op == OuterOp::ShouldExit { break }
    }

    // Return the terminal to its original mode.
    if ! already_had_raw_mode_enabled { raw_mode::disable()? }

    Ok(())
}

/// Shows the user an error message when something goes wrong.
/// TODO move to a new rs-lib called 'error'
fn format_cli_error((code, status, text): (u16, u8, String)) -> String {
    let c_string = code.to_string(); // eg "23456"
    let c_1 = &c_string[0..1]; // eg "2"
    let c_2 = &c_string[1..]; // eg "3456"
    // `EX_USAGE` is error status 64, meaning the user supplied invalid CLI args.
    let h_1 = "For help, try:";
    let h_2 = "example_anim_repl";
    let h_3 = "--help";
    if status == EX_USAGE {
        format!("Error {}_{} 64:\n    {}. {}\n    {} {}", c_1, c_2, text, h_1, h_2, h_3)
    } else {
        format!("Error {}_{} {}: {}", c_1, c_2, status, text)
    }
}
