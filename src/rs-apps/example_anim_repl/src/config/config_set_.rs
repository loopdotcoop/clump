//! src/rs-apps/example_anim_repl/src/config/config_set_.rs
//! ### `Config::set_*()`
//!
//! Methods which set various `Config` fields, based on command-line arguments.

// src/rs-libs/ dependencies.
use constants::err_status::EX_DATAERR;

// Local imports.
use super::Config;

impl Config {

    /// Sets `Config::columns` based on the '-c' or '--columns' argument.
    pub fn set_columns(self, v: String) -> Result<Self, (u16, u8, String)> {
        let mut new_config = self.clone();
        if v.len() > 12 { return Err((2_3651, EX_DATAERR, format!(
            "--columns '{:.12}...' is longer than 5 chars", v))) }
        if v.len() > 5 { return Err((2_1439, EX_DATAERR, format!(
            "--columns '{}' is longer than 5 chars", v))) }
        for (_, c) in v.chars().enumerate() {
            if c < '0' || c > '9' { return Err((2_1580, EX_DATAERR, format!(
                "--columns value contains invalid char '{}'", c))) }
        }
        let int: u32 = v.parse().unwrap(); // u32 allows 65536 or 99999
        if int == 0 { return Err((2_4519, EX_DATAERR, format!(
            "--columns '{}' is less than 1", v))) }
        if int > 65535 { return Err((2_0018, EX_DATAERR, format!(
            "--columns '{}' is greater than 65535", v))) }
        new_config.columns = int as u16;
        Ok(new_config)
    }

    /// Sets `Config::fill` based on the '-f' or '--fill' argument.
    pub fn set_fill(self, v: String) -> Result<Self, (u16, u8, String)> {
        let mut new_config = self.clone();
        if v.len() > 12 { return Err((2_1418, EX_DATAERR, format!(
            "--fill '{:.12}...' is not 1 char long", v))) }
        if v.chars().count() != 1 as usize { return Err((2_5416, EX_DATAERR, format!(
            "--fill '{}' is not 1 char long", v))) }
        let chr = v.chars().next().unwrap();
        if chr < ' ' || chr > '~' { return Err((2_5261, EX_DATAERR, format!(
            "--fill value is invalid char '{}'", chr))) }
        new_config.fill = chr;
        Ok(new_config)
    }

    /// Sets `Config::rows` based on the '-r' or '--rows' argument.
    pub fn set_rows(self, v: String) -> Result<Self, (u16, u8, String)> {
        let mut new_config = self.clone();
        if v.len() > 12 { return Err((2_1522, EX_DATAERR, format!(
            "--rows '{:.12}...' is longer than 5 chars", v))) }
        if v.len() > 5 { return Err((2_2181, EX_DATAERR, format!(
            "--rows '{}' is longer than 5 chars", v))) }
        for (_, c) in v.chars().enumerate() {
            if c < '0' || c > '9' { return Err((2_1442, EX_DATAERR, format!(
                "--rows value contains invalid char '{}'", c))) }
        }
        let int: u32 = v.parse().unwrap(); // u32 allows 65536 or 99999
        if int == 0 { return Err((2_1401, EX_DATAERR, format!(
            "--rows '{}' is less than 1", v))) }
        if int > 65535 { return Err((2_1962, EX_DATAERR, format!(
            "--rows '{}' is greater than 65535", v))) }
        new_config.rows = int as u16;
        Ok(new_config)
    }
}
