//! src/rs-apps/example_anim_repl/src/config/config_show_version.rs
//! ### `config_show_version`
//!
//! Generates the app's '--version' string.

/// Generates the app's '--version' string.
pub fn config_show_version() -> String {
    "example_anim_repl 0.0.1".into()
}
