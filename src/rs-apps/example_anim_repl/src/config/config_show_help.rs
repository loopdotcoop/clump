//! src/rs-apps/example_anim_repl/src/config/config_show_help.rs
//! ### `config_show_help`
//!
//! Generates the app's '--help' documentation.

// Local imports.
use super::config_show_version;

/// Generates the app's '--help' documentation.
pub fn config_show_help() -> String {
    let name_and_version = config_show_version();
    format!(r#"
{}
{}

{}

{name} <options>

Startup examples
----------------

# Start the app, as a 40 x 12 grid of plus signs:
{name} --columns 40 --rows 12 --fill "+"

# Start the app, as an 80 x 24 grid of dots:
{name}

# Just show the app's name and version, without starting the app:
{name} --version

Startup options (defaults)
--------------------------
-c  --columns <20-65535>  (80)    Set the app's initial width in characters
-f  --fill <[ -~]{{1}}>    (".")    Set the app's initial background character
-h  --help                        Show this help
-r  --rows <2-65535>     (24)     Set the app's initial height in lines
-v  --version                     Show the app's name and version

Runtime examples
----------------

# Show a list of available commands:
> help

# Show help about the 'columns' command:
> h columns

# Change the app's width to 60 characters:
> columns 60

# Shut down the app gracefully:
> exit

Runtime commands
----------------
> c  > clear               Clear the log
     > columns <20-65535>  Change the app's width in characters
     > fill <[ -~]{{1}}>     Change the app's background character
> h  > help                List available commands
> h  > help <command>      Show help about a given command
     > rows <2-65535>      Change the app's height in lines
> v  > version             Show the app's name and version
> x  > exit                Quit the app

"#,
        name_and_version,
        "=".repeat(name_and_version.len()),
        "An example Rust app which shows an animation, and runs a read-eval-print loop.",
        name="example_anim_repl",
    )
}
