//! src/rs-apps/example_anim_repl/src/config/config.rs
//! ### `Config`
//!
//! Representation of the options set by the user when the app was invoked.

/// Representation of the options set by the user when the app was invoked.
#[derive(Clone,Debug,Default)]
pub struct Config {
    pub columns: u16,
    pub fill: char,
    pub rows: u16,
    pub show_help: bool,
    pub show_version: bool,
}
