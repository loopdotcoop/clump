//! src/rs-apps/example_anim_repl/src/config/config_try_from.rs
//! ### `Config::try_from()`
//!
//! Interpret a vector of arguments as a sequence of key/value pairs, where some
//! keys (eg --version) don’t have a value.

// Rust standard library.
use std::convert::TryFrom;

// src/rs-libs/ dependencies.
use constants::err_status::EX_USAGE;

// Local imports.
use super::Config;

// Used by try_from() to keep track of what the next expected argument is.
#[derive(Debug,PartialEq)]
enum NextArg {
    Columns,
    Fill,
    Key,
    Rows,
}

/// Interpret a vector of arguments as a sequence of key/value pairs, where some
/// keys (eg --version) don’t have a value.
impl TryFrom<Vec<String>> for Config {
    type Error = (u16, u8, String);

    // Interprets a vector of arguments as a sequence of key/value pairs, where
    // some keys (eg '--version') don’t have a value.
    fn try_from(args: Vec<String>) -> Result<Self, Self::Error> {
        let mut config = Config::default();
        let mut next_arg = NextArg::Key; // expect the first arg to be a key

        // Step through each command line argument.
        for arg in args {

            // Recognise a key. These all begin with a hyphen.
            let first_char = arg.chars().next().unwrap();
            if first_char == '-' {
                if next_arg != NextArg::Key {
                    return Err((2_7723, EX_USAGE, format!(
                        "Missing '{:?}' value", next_arg)))}

                match &arg[0..] {
                    "-c" | "--columns" => next_arg = NextArg::Columns,
                    "-f" | "--fill" => next_arg = NextArg::Fill,
                    "-h" | "--help" => config.show_help = true,
                    "-r" | "--rows" => next_arg = NextArg::Rows,
                    "-v" | "--version" => config.show_version = true,
                    _ => return Err((2_1028, EX_USAGE, format!(
                        "Key '{}' not recognised", arg))),
                }

            // Record a value.
            } else {
                config = match next_arg {
                    NextArg::Key => {
                        if arg.len() > 12 { return Err((2_0917, EX_USAGE, format!(
                            "Expected a key but got value '{:.12}...'", arg))) }
                        return Err((2_1482, EX_USAGE, format!(
                            "Expected a key but got value '{}'", arg)))
                    },
                    NextArg::Columns => config.set_columns(arg)?,
                    NextArg::Fill => config.set_fill(arg)?,
                    NextArg::Rows => config.set_rows(arg)?,
                };
                next_arg = NextArg::Key
            }

        };

        // Return the Config, or an error if we are still expecting a value.
        if next_arg != NextArg::Key {
            return Err((2_1076, EX_USAGE, format!(
                "No '{:?}' value", next_arg)))}
        Ok(config)
    }
}


/* ---------------------------------- TEST ---------------------------------- */

#[cfg(test)]
mod tests {
    use constants::err_status::{EX_DATAERR,EX_USAGE};
    use super::super::*;

    fn args1(a0: &str) -> Vec<String> {
        vec![a0.into()]}
    fn args2(a0: &str, a1: &str) -> Vec<String> {
        vec![a0.into(), a1.into()]}
    fn args3(a0: &str, a1: &str, a2: &str) -> Vec<String> {
        vec![a0.into(), a1.into(), a2.into()]}
    fn args4(a0: &str, a1: &str, a2: &str, a3: &str) -> Vec<String> {
        vec![a0.into(), a1.into(), a2.into(), a3.into()]}

    fn config_columns(v: u16) -> String {
        let mut o = Config::default(); o.columns = v; format!("{:?}", o)}
    fn config_fill(v: char) -> String {
        let mut o = Config::default(); o.fill = v; format!("{:?}", o)}
    fn config_rows(v: u16) -> String {
        let mut o = Config::default(); o.rows = v; format!("{:?}", o)}
    fn config_show_help(v: bool) -> String {
        let mut o = Config::default(); o.show_help = v; format!("{:?}", o)}
    fn config_show_version(v: bool) -> String {
        let mut o = Config::default(); o.show_version = v; format!("{:?}", o)}

    #[test]
    fn config_try_from_errors() {

        // Errors due to invalid keys.
        assert_eq!(
            Config::try_from(args1("nope")).unwrap_err(),
            (2_1482, EX_USAGE, "Expected a key but got value 'nope'".into()));
        assert_eq!(
            Config::try_from(args2("--help", "ABCDEFGHIJKL")).unwrap_err(),
            (2_1482, EX_USAGE, "Expected a key but got value 'ABCDEFGHIJKL'".into()));
        assert_eq!(
            Config::try_from(args3("--rows", "123", "ABCDEFGHIJKLM")).unwrap_err(),
            (2_0917, EX_USAGE, "Expected a key but got value 'ABCDEFGHIJKL...'".into()));
        assert_eq!(
            Config::try_from(args1("--nope")).unwrap_err(),
            (2_1028, EX_USAGE, "Key '--nope' not recognised".into()));
        assert_eq!(
            Config::try_from(args1("--columns")).unwrap_err(),
            (2_1076, EX_USAGE, "No 'Columns' value".into()));

        // --columns errors.
        assert_eq!(
            Config::try_from(args3("--columns", "123456", "-v")).unwrap_err(),
            (2_1439, EX_DATAERR, "--columns '123456' is longer than 5 chars".into()));
        assert_eq!(
            Config::try_from(args2("-c", "12345678901234567890")).unwrap_err(),
            (2_3651, EX_DATAERR, "--columns '123456789012...' is longer than 5 chars".into()));
        assert_eq!( // does not interpret '-h' as the --columns value
            Config::try_from(args2("--columns", "-h")).unwrap_err(),
            (2_7723, EX_USAGE, "Missing 'Columns' value".into()));
        assert_eq!(
            Config::try_from(args2("-c", "12.3")).unwrap_err(),
            (2_1580, EX_DATAERR, "--columns value contains invalid char '.'".into()));
        assert_eq!(
            Config::try_from(args4("-r", "1", "--columns", "000")).unwrap_err(),
            (2_4519, EX_DATAERR, "--columns '000' is less than 1".into()));
        assert_eq!(
            Config::try_from(args2("-c", "65536")).unwrap_err(),
            (2_0018, EX_DATAERR, "--columns '65536' is greater than 65535".into()));

        // --fill errors.
        assert_eq!(
            Config::try_from(args3("--version", "--fill", "ab")).unwrap_err(),
            (2_5416, EX_DATAERR, "--fill 'ab' is not 1 char long".into()));
        assert_eq!(
            Config::try_from(args2("--fill", "ABCDEFGHIJKLM")).unwrap_err(),
            (2_1418, EX_DATAERR, "--fill 'ABCDEFGHIJKL...' is not 1 char long".into()));
        assert_eq!(
            Config::try_from(args2("-f", "\n")).unwrap_err(),
            (2_5261, EX_DATAERR, "--fill value is invalid char '\n'".into()));
        assert_eq!(
            Config::try_from(args4("-r", "12345", "--fill", "§")).unwrap_err(),
            (2_5261, EX_DATAERR, "--fill value is invalid char '§'".into()));
        assert_eq!(
            Config::try_from(args2("-f", "😊")).unwrap_err(),
            (2_5261, EX_DATAERR, "--fill value is invalid char '😊'".into()));

        // --rows errors.
        assert_eq!(
            Config::try_from(args2("--rows", "abcdefghijkl")).unwrap_err(),
            (2_2181, EX_DATAERR, "--rows 'abcdefghijkl' is longer than 5 chars".into()));
        assert_eq!(
            Config::try_from(args3("-h", "-r", "ABCDEFGHIJKLM")).unwrap_err(),
            (2_1522, EX_DATAERR, "--rows 'ABCDEFGHIJKL...' is longer than 5 chars".into()));
        assert_eq!( // does not interpret '--columns' as the --rows value
            Config::try_from(args2("--rows", "--columns")).unwrap_err(),
            (2_7723, EX_USAGE, "Missing 'Rows' value".into()));
        assert_eq!(
            Config::try_from(args4("--fill", "#", "-r", "1😊")).unwrap_err(),
            (2_1442, EX_DATAERR, "--rows value contains invalid char '😊'".into()));
        assert_eq!(
            Config::try_from(args4("-c", "00001", "--rows", "0")).unwrap_err(),
            (2_1401, EX_DATAERR, "--rows '0' is less than 1".into()));
        assert_eq!(
            Config::try_from(args2("-r", "99999")).unwrap_err(),
            (2_1962, EX_DATAERR, "--rows '99999' is greater than 65535".into()));
    }

    #[test]
    fn config_try_from_ok() {

        // --columns ok.
        assert_eq!(config_columns(0), // internal value of `0` means "use default"
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_columns(55555),
            format!("{:?}", Config::try_from(args2("--columns", &"5".repeat(5))).unwrap()));
        assert_eq!(config_columns(1),
            format!("{:?}", Config::try_from(args2("-c", "00001")).unwrap())); // leading zeros are ignored

        // --fill ok.
        assert_eq!(config_fill('\0'), // internal value of NUL means "use default"
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_fill(' '),
            format!("{:?}", Config::try_from(args2("--fill", " ")).unwrap()));
        assert_eq!(config_fill('~'),
            format!("{:?}", Config::try_from(args2("-f", "~")).unwrap()));

        // --help ok.
        assert_eq!(config_show_help(false), // use `options_show_help(true)` here if passing no options should show help
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_show_help(true),
            format!("{:?}", Config::try_from(args1("--help")).unwrap()));
        assert_eq!(config_show_help(true),
            format!("{:?}", Config::try_from(args1("-h")).unwrap()));

        // --rows ok.
        assert_eq!(config_rows(0), // internal value of `0` means "use default"
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_rows(65535),
            format!("{:?}", Config::try_from(args2("--rows", "65535")).unwrap()));
        assert_eq!(config_rows(9),
            format!("{:?}", Config::try_from(args2("-r", "09")).unwrap()));

        // --version ok.
        assert_eq!(config_show_version(false),
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_show_version(true),
            format!("{:?}", Config::try_from(args1("--version")).unwrap()));
        assert_eq!(config_show_version(true),
            format!("{:?}", Config::try_from(args1("-v")).unwrap()));
    }
}
