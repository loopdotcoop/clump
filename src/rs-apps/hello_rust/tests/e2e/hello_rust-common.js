// src/rs-apps/hello_rust/tests/e2e/hello_rust-common.js

export const commonE2eTests = ({ equal }, hello_rust) => [
    {
        equal: (a, e) => equal(a, e), // provides location in the stack
        attempt: _ => hello_rust('--who "long arguments user"'),
        exp: 'Hello from Rust, long arguments user!',
    },
    {
        equal: (a, e) => equal(a, e),
        attempt: _ => hello_rust("-w 'short arguments user'"),
        exp: 'Hello from Rust, short arguments user!',
    },
    {
        equal: (a, e) => equal(a, e),
        attempt: _ => hello_rust("-who 'one dash not two!'"),
        exp: isCli => isCli
            ? "Error 2_5218 64:\n" +
              "    Key '-who' not recognised. For help, try:\n" +
              '    hello_rust --help'
            : JSON.stringify({
                code: 2_5218,
                status: 64, // EX_USAGE
                text: "Key '-who' not recognised. For help, try:\nhello_rust('--help')",
            }),
    },
    {
        equal: (a, e) => equal(a, e),
        attempt: _ => hello_rust("--who Unescaped'Quotes'Here"),
        exp: isCli => isCli
            ? 'Hello from Rust, UnescapedQuotesHere!' // TODO same in all shells?
            : JSON.stringify({
                code: 3_1794,
                status: 65, // EX_DATAERR
                text: "Single-quote at index 15 should be prefixed with a backslash",
            }),
    },
];
