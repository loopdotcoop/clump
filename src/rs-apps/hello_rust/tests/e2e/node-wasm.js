// src/rs-apps/hello_rust/tests/e2e/node-wasm.js

import { commonE2eTests } from './hello_rust-common.js';

export const testE2eNodeWasm = ({ equal }, wasm) => {
    const { hello_rust } = wasm;

    const e2eTests = [
        {
            equal: (a, e) => equal(a, e), // provides location in the stack
            attempt: _ => hello_rust(''),
            exp: 'Hello from Rust, wasm app!',
        },
        ...commonE2eTests({ equal }, hello_rust),
    ];

    const results = e2eTests.map((e2eTest) => {
        const { attempt, exp } = e2eTest;
        const kind = e2eTest.equal ? 'equal' : 'UNKNOWN!';
        let actually, expected; try {
            actually = attempt(false);
            expected = typeof exp === 'function' ? exp(false) : exp;
            switch (kind) { // TODO more kinds of assertion
                case 'equal': e2eTest.equal(actually, expected); break;
                default: throw Error(`No such assertion kind '${kind}'`);
            }
        } catch (err) { return { actually, err, expected, kind } }
        return { actually, err:null, expected, kind };
    });

    return results;
};
