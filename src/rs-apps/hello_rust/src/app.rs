//! src/rs-apps/hello_rust/src/app.rs
//! ### `app`
//!
//! The common `hello_rust` entrypoint, used by both CLI and WebAssembly.

// src/rs-libs/ dependencies.
use constants::err_status::EX_USAGE;

// Local imports.
use crate::config::{Config,config_show_help,config_show_version};

/// ### `app()`
/// The common `hello_rust` entrypoint, used by both CLI and WebAssembly.
pub fn app(args: Vec<String>, is_cli: bool) -> String {
    // format!("args.len() is {} {} {}", args.len(), EX_USAGE, EX_DATAERR)

    // Parse the arguments, and deal with simple commands.
    let config = match Config::try_from(args) {
        Err(err) => return handle_error(err, is_cli),
        Ok(ok) => ok,
    };
    if config.show_help { return config_show_help() }
    if config.show_version { return config_show_version() }

    // Use default values if any optional config is missing.
    let who = if config.who == "" {
        if is_cli { "cli binary app".to_string() } else { "wasm app".to_string() }
    } else { config.who };

    format!("Hello from Rust, {}!", who)
}

/// Shows the user an error message when something goes wrong.
/// TODO move to a new rs-lib called 'error'
fn handle_error((code, status, text): (u16, u8, String), is_cli: bool) -> String {
    let c_string = code.to_string(); // eg "23456"
    let c_1 = &c_string[0..1]; // eg "2"
    let c_2 = &c_string[1..]; // eg "3456"
    // `EX_USAGE` is error status 64, meaning the user supplied invalid CLI args.
    let h_1 = "For help, try:";
    let h_2 = "hello_rust";
    let h_3 = "--help";
    match (status, is_cli) {
        (EX_USAGE, false) => format!("{{\"code\":{},\"status\":64,\"text\":\"{}. {}\\n{}('{}')\"}}", code, text, h_1, h_2, h_3),
        (EX_USAGE, true) => format!("Error {}_{} 64:\n    {}. {}\n    {} {}", c_1, c_2, text, h_1, h_2, h_3),
        (status, false) => format!("{{\"code\":{},\"status\":{},\"text\":\"{}\"}}", code, status, text),
        (status, true) => format!("Error {}_{} {}: {}", c_1, c_2, status, text),
    }
}
