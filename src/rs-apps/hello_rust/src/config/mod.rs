//! src/rs-apps/hello_rust/src/config/mod.rs
//! ### `config`
//!
//! TODO describe.

// Export the Config struct.
mod config;
pub use self::config::Config;

// Add the set_...() and try_from() methods to Config.
mod config_set_;
mod config_try_from;

// Export functions to call if --help or --version options are present.
mod config_show_help;
pub use self::config_show_help::config_show_help;

mod config_show_version;
pub use self::config_show_version::config_show_version;
