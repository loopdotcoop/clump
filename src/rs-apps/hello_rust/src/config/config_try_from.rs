//! src/rs-apps/hello_rust/src/config/config_try_from.rs
//! ### `Config::try_from()`
//!
//! Interpret a vector of arguments as a sequence of key/value pairs, where some
//! keys (eg --version) don’t have a value.

// Rust standard library.
use std::convert::TryFrom;

// src/rs-libs/ dependencies.
use constants::err_status::EX_USAGE;

// Local imports.
use super::Config;

// Used by try_from() to keep track of what the next expected argument is.
#[derive(Debug,PartialEq)]
enum NextArg {
    Key,
    Who,
}

/// Interpret a vector of arguments as a sequence of key/value pairs, where some
/// keys (eg --version) don’t have a value.
impl TryFrom<Vec<String>> for Config {
    type Error = (u16, u8, String);

    // Interprets a vector of arguments as a sequence of key/value pairs, where
    // some keys (eg '--version') don’t have a value.
    fn try_from(args: Vec<String>) -> Result<Self, Self::Error> {
        let mut config = Config::default();
        let mut next_arg = NextArg::Key; // expect the first arg to be a key

        // Step through each command line argument.
        for arg in args {

            // Recognise a key. These all begin with a hyphen.
            let first_char = arg.chars().next().unwrap();
            if first_char == '-' {
                if next_arg != NextArg::Key {
                    return Err((2_2274, EX_USAGE, format!(
                        "Missing '{:?}' value", next_arg)))}

                match &arg[0..] {
                    "-h" | "--help" => config.show_help = true,
                    "-v" | "--version" => config.show_version = true,
                    "-w" | "--who" => next_arg = NextArg::Who,
                    _ => return Err((2_5218, EX_USAGE, format!(
                        "Key '{}' not recognised", arg))),
                }

            // Record a value.
            } else {
                config = match next_arg {
                    NextArg::Key => {
                        if arg.len() > 12 { return Err((2_8418, EX_USAGE, format!(
                            "Expected a key but got value '{:.12}...'", arg))) }
                        return Err((2_4951, EX_USAGE, format!(
                            "Expected a key but got value '{}'", arg)))
                    },
                    NextArg::Who => config.set_who(arg)?,
                };
                next_arg = NextArg::Key
            }

        };

        // Return the Config, or an error if we are still expecting a value.
        if next_arg != NextArg::Key {
            return Err((2_2541, EX_USAGE, format!(
                "No '{:?}' value", next_arg)))}
        Ok(config)
    }
}


/* ---------------------------------- TEST ---------------------------------- */

#[cfg(test)]
mod tests {
    use constants::err_status::{EX_DATAERR,EX_USAGE};
    use super::super::*;

    fn args1(a0: &str) -> Vec<String> {
        vec![a0.into()]}
    fn args2(a0: &str, a1: &str) -> Vec<String> {
        vec![a0.into(), a1.into()]}
    fn args3(a0: &str, a1: &str, a2: &str) -> Vec<String> {
        vec![a0.into(), a1.into(), a2.into()]}
    fn args4(a0: &str, a1: &str, a2: &str, a3: &str) -> Vec<String> {
        vec![a0.into(), a1.into(), a2.into(), a3.into()]}

    fn config_show_help(v: bool) -> String {
        let mut o = Config::default(); o.show_help = v; format!("{:?}", o)}
    fn config_show_version(v: bool) -> String {
        let mut o = Config::default(); o.show_version = v; format!("{:?}", o)}
    fn config_who(v: String) -> String {
        let mut o = Config::default(); o.who = v; format!("{:?}", o)}

    #[test]
    fn config_try_from_errors() {

        // Errors due to invalid keys.
        assert_eq!(
            Config::try_from(args1("nope")).unwrap_err(),
            (2_4951, EX_USAGE, "Expected a key but got value 'nope'".into()));
        assert_eq!(
            Config::try_from(args2("--help", "ABCDEFGHIJKL")).unwrap_err(),
            (2_4951, EX_USAGE, "Expected a key but got value 'ABCDEFGHIJKL'".into()));
        assert_eq!(
            Config::try_from(args3("--who", "Abc", "ABCDEFGHIJKLM")).unwrap_err(),
            (2_8418, EX_USAGE, "Expected a key but got value 'ABCDEFGHIJKL...'".into()));
        assert_eq!(
            Config::try_from(args1("--nope")).unwrap_err(),
            (2_5218, EX_USAGE, "Key '--nope' not recognised".into()));

        // --who errors.
        assert_eq!(
            Config::try_from(args4("-h", "--who", "123456789012345678901234567890123", "-v")).unwrap_err(),
            (2_3992, EX_DATAERR, "--who '123456789012...' is longer than 32 chars".into()));
        assert_eq!( // the value must not look like an option
            Config::try_from(args3("--who", "--version", "-v")).unwrap_err(),
            (2_2274, EX_USAGE, "Missing 'Who' value".into()));
        assert_eq!(
            Config::try_from(args1("--who")).unwrap_err(),
            (2_2541, EX_USAGE, "No 'Who' value".into()));
        assert_eq!(
            Config::try_from(args2("-w", "ABC\nXYZ")).unwrap_err(),
            (2_3856, EX_DATAERR, "--who char at index 3 is invalid".into()));
    }

    #[test]
    fn config_try_from_ok() {

        // --who ok.
        assert_eq!(config_who("".into()), // the empty string means "use default"
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_who("aaaaa".into()),
            format!("{:?}", Config::try_from(args2("--who", &"a".repeat(5))).unwrap()));
        assert_eq!(config_who("12345678901234567890123456789012".into()),
            format!("{:?}", Config::try_from(args2("-w", "12345678901234567890123456789012")).unwrap()));

        // --help ok.
        assert_eq!(config_show_help(false), // use `config_show_help(true)` here if passing no options should show help
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_show_help(true),
            format!("{:?}", Config::try_from(args1("--help")).unwrap()));
        assert_eq!(config_show_help(true),
            format!("{:?}", Config::try_from(args1("-h")).unwrap()));

        // --version ok.
        assert_eq!(config_show_version(false),
            format!("{:?}", Config::try_from(vec![]).unwrap()));
        assert_eq!(config_show_version(true),
            format!("{:?}", Config::try_from(args1("--version")).unwrap()));
        assert_eq!(config_show_version(true),
            format!("{:?}", Config::try_from(args1("-v")).unwrap()));
    }
}
