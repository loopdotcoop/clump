//! src/rs-apps/hello_rust/src/config/config_show_help.rs
//! ### `config_show_help`
//!
//! Generates the app's '--help' documentation.

// Local imports.
use super::config_show_version;

/// Generates the app's '--help' documentation.
pub fn config_show_help() -> String {
    let name_and_version = config_show_version();
    format!(r#"
{}
{}

{}

{name} <options>

Examples
--------

# Print a custom greeting:
{name} --who "example user"

# Print the default greeting:
{name}

Options (defaults)
------------------
-h --help               Show this help
-v --version            Show the current {name} version
-w --who      ("")      Who should be greeted, [ -~]{{0,24}}

"#,
        name_and_version,
        "=".repeat(name_and_version.len()),
        std::env::var("CARGO_PKG_DESCRIPTION").unwrap(),
        name=std::env::var("CARGO_PKG_NAME").unwrap(),
    )
}
