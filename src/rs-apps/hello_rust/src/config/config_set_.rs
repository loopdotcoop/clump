//! src/rs-apps/hello_rust/src/config/config_set_.rs
//! ### `Config::set_*()`
//!
//! Methods which set various `Config` fields, based on command-line arguments.

// src/rs-libs/ dependencies.
use constants::err_status::EX_DATAERR;

// Local imports.
use super::Config;

impl Config {

    /// Sets `Config::who` based on the '-w' or '--who' argument.
    /// Note that config_try_from.rs tests this method.
    pub fn set_who(self, v: String) -> Result<Self, (u16, u8, String)> {
        let mut new_config = self.clone();
        if v.len() > 32 { return Err((2_3992, EX_DATAERR, format!(
            "--who '{:.12}...' is longer than 32 chars", v))) }
        for (i, c) in v.chars().enumerate() {
            if c < ' ' || c > '~' { return Err((2_3856, EX_DATAERR, format!(
                "--who char at index {} is invalid", i))) }
        }
        new_config.who = v;
        Ok(new_config)
    }
}
