//! src/rs-apps/hello_rust/src/config/config_show_version.rs
//! ### `config_show_version`
//!
//! Generates the app's '--version' string.

/// Generates the app's '--version' string.
pub fn config_show_version() -> String {
    format!(
        "{} {}",
        std::env::var("CARGO_PKG_NAME").unwrap(),
        std::env::var("CARGO_PKG_VERSION").unwrap(),
    )
}
