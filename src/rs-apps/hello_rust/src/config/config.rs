//! src/rs-apps/hello_rust/src/config/config.rs
//! ### `Config`
//!
//! Representation of the options set by the user when the app was invoked.

/// Representation of the options set by the user when the app was invoked.
#[derive(Clone,Debug,Default)]
pub struct Config {
    pub show_help: bool,
    pub show_version: bool,
    pub who: String,
}
