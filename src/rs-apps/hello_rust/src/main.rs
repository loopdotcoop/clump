//! src/rs-apps/hello_rust/src/main.rs
//! ### `main`
//!
//! Command-line entrypoint for the `hello_rust` Rust app.

// Local imports.
mod app; use app::app;
mod config;

/// ### `main()`
/// Responds to command line invocation, eg `hello_rust --who somebody`.
fn main() {
    // Get the command line arguments (`args[0]` is just the path to the binary).
    // Pass the arguments to app(), and print the result.
    let args = std::env::args().skip(1).collect();
    println!("{}", app(args, true))
}
