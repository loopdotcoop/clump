//! src/rs-apps/hello_rust/src/lib.rs
//! ### `lib`
//!
//! Node.js and web browser .wasm entrypoint for the `hello_rust` Rust library.

use wasm_bindgen::prelude::*; // used by wasm-pack to generate JS bindings

// src/rs-libs/ dependencies.
use constants::err_status::EX_USAGE;
use parse::parse_args::parse_args;

// Local imports.
mod app; use app::app;
mod config;

/// ### `hello_rust()`
/// An example function, which exposes the app() to .wasm users.
#[wasm_bindgen] // marks this function to be exported by wasm-pack
pub fn hello_rust(raw_args: &str) -> String {
    match parse_args(raw_args.into()) { // TODO parse_args() should accept String or &str
        Ok(args) => app(args, false),
        Err(err) => handle_error(err),
    }
}

/// Shows the user an error message when something goes wrong.
/// TODO move to a new rs-lib called 'error'
fn handle_error((code, status, text): (u16, u8, String)) -> String {
    // `EX_USAGE` is error status 64, meaning the user supplied invalid CLI args.
    let h_1 = "For help, try:";
    let h_2 = "example_anim_repl";
    let h_3 = "--help";
    match status {
        EX_USAGE => format!("{{\"code\":{},\"status\":64,\"text\":\"{}. {}\\n{}('{}')\"}}", code, text, h_1, h_2, h_3),
        status => format!("{{\"code\":{},\"status\":{},\"text\":\"{}\"}}", code, status, text),
    }
}
