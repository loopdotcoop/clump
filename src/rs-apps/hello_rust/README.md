<!-- src/rs-apps/hello_rust/README.md -->

# hello_rust

> TODO description

- 0.0.1
- Created February 2024, in Lisbon, Portugal, by Richi Plus
- <https://gitlab.com/loopdotcoop/clump/>
- <https://loopdotcoop.gitlab.io/clump/index.html>

hello_rust is a Rust app which can be:

1. __Compiled to a standalone CLI binary for the current platform:__  
   dist/hello_rust/cli-binary/
2. __Compiled to a WebAssembly .wasm file for the Node.js runtime:__  
   dist/hello_rust/node-wasm/
3. __Compiled to a WebAssembly .wasm file for web browsers:__  
   dist/hello_rust/web-browser-wasm/

It's confirmed to work on:

- macOS Monterey
- ~~Windows 11 PowerShell~~ TODO check
- ~~Windows Subsystem for Linux (wsl)~~ TODO check

## Run hello_rust from source, during development

Show the help page:

```sh
# From the root of the 'clump' repo:
npm run rs -- hello_rust --help
node scripts/run-rs.js hello_rust -h
# Or from the src/rs-apps/hello_rust/ folder:
cargo run -- --help
```

Print a greeting:

```sh
# From the root of the 'clump' repo:
npm run rs -- hello_rust --who "clump developer"
node scripts/run-rs.js hello_rust -w "clump developer"
# Or from the src/rs-apps/hello_rust/ folder:
cargo run -- --who "clump developer"
```

## Handy commands

Build and open the hello_rust documentation (without dependencies):

```sh
# From the root of the 'clump' repo:
npm run doc:rs -- hello_rust --open
node scripts/doc-rs.js hello_rust --open
# Or from the src/rs-apps/hello_rust/ folder:
cargo clean --doc
cargo doc --no-deps --open
```

Run the hello_rust unit tests and integration tests:

```sh
# From the root of the 'clump' repo:
npm run test:internal:rs -- hello_rust
node scripts/run-rs-internal-tests.js hello_rust
# Or from the src/rs-apps/hello_rust/ folder:
cargo test
```

Periodically, make sure Rust is using up-to-date dependencies:

```sh
# From the root of the 'clump' repo:
npm run update:rs -- hello_rust
node scripts/update-rs.js hello_rust
# Or from the src/rs-apps/hello_rust/ folder:
cargo update --dry-run # for extra safety
cargo update
```

## Set up your machine

To set up your machine, see ['Set up your machine' in the root README.md
](../../../README.md#set-up-your-machine) file.

## Build, test and dev

### Build just the hello_rust Rust app and/or library

To build only hello_rust, use the NPM script, or invoke build-rs.js directly:

```sh
npm run build:rs -- hello_rust
node scripts/build-rs.js hello_rust
# ------------------------------------------------------------------------------
# Running buildCliBinary('hello_rust')...
#  OK  rustc succeeded
# Running buildNodeWasm('hello_rust')...
# ...
#  OK  wasm-pack build succeeded
# Running buildNodeWrapper('hello_rust')...
#  OK  Created hello_rust-cli.js, hello_rust-wrapper.js
# Running buildWebBrowserWasm('hello_rust')...
# ...
#  OK  All three 'hello_rust' builds succeeded
# ==============================================================================
#  Done!  3 builds for 1 Rust app and/or library
# ==============================================================================
```

It is also possible to build all the rs-apps using one command.  
See ['Build all the rs-apps'](
../../../README.md#build-all-the-rs-apps) in the root README.md file.

### Run just the 'hello_rust' JavaScript end-to-end tests

To test only hello_rust, use the NPM script, or invoke run-rs-e2e-tests.js
directly:

```sh
npm run test:e2e:rs -- hello_rust
node scripts/run-rs-e2e-tests.js hello_rust
# ...
# ------------------------------------------------------------------------------
# Running testCliBinary('hello_rust')...
#  PASS  Both 'hello_rust' e2e cli-binary tests passed
# Running testNodeWasm('hello_rust')...
#  PASS  Both 'hello_rust' e2e node-wasm tests passed
# ==============================================================================
#  Done!  2 e2e test suites for 1 Rust apps
#         Web browser .wasm files will also need to be tested in the browser,
#         as part of the end-to-end and integration tests of the HTML pages.
#         Alternatively the web browser .wasm files can be tested using:
#         node scripts/run-rs-e2e-tests/test-web-browser-wasm.js hello_rust
# ==============================================================================
```

It is also possible to run all the Rust e2e tests.  
See ['Run all the Rust app and/or library e2e tests'](
../../../README.md#run-all-the-rust-e2e-tests) in the root README.md file.

### Run just the 'hello_rust' Rust unit and integration tests

To test only hello_rust, use the NPM script, or invoke run-rs-internal-tests.js
directly:

```sh
npm run test:internal:rs -- hello_rust
node scripts/run-rs-internal-tests.js hello_rust
# ------------------------------------------------------------------------------
# running 2 tests
# test config::config_try_from::tests::config_try_from_ok ... ok
# test config::config_try_from::tests::config_try_from_errors ... ok
# test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
# running 2 tests
# test config::config_try_from::tests::config_try_from_errors ... ok
# test config::config_try_from::tests::config_try_from_ok ... ok
# test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
# running 0 tests
# test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
#  PASS  'hello_rust' internal tests passed
# ==============================================================================
#  PASS  The Rust crate 'hello_rust' internal tests passed
# ==============================================================================
```

It is also possible to run all the Rust unit and integration tests.

See ['Run all the Rust app and/or library unit and integration tests'](
../../../README.md#run-all-the-rust-unit-and-integration-tests)
in the root README.md file.
