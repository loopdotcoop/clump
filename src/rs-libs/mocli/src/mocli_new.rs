//! src/rs-libs/mocli/src/mocli_new.rs
//! ### `Mocli::new()`
//!
//! Creates a Mocli instance.

// Local imports.
use crate::config_mocli::config_mocli::ConfigMocli;
use crate::mocli::Mocli;
use crate::output_bkg::output_bkg::OutputBkg;
use crate::output_cmd::output_cmd::OutputCmd;
use crate::output_err::output_err::OutputErr;
use crate::output_log::output_log::OutputLog;

impl Mocli {

    /// Creates a Mocli instance.
    pub fn new(config: &ConfigMocli) -> Result<Self, (u16, u8, String)> {

        // Validate all config fields.
        config.validate()?;

        // Fall back to defaults for optional configuration fields.
        // Note that OutputBkg::new() etc do something similar.
        let view_size = config.view_size.unwrap_or((20, 4));

        Ok(Self {
            bkg: if config.has_bkg { Some(OutputBkg::new(&config)?) } else { None },
            cmd: if config.has_cmd { Some(OutputCmd::new(&config)?) } else { None },
            err: if config.has_err { Some(OutputErr::new(&config)?) } else { None },
            log: if config.has_log { Some(OutputLog::new(&config)?) } else { None },
            view_size,
        })
    }
}


/* ---------------------------------- TEST ---------------------------------- */

#[cfg(test)]
mod tests {
    use constants::err_status::EX_CONFIG;
    use super::*;

    fn err(config: &ConfigMocli, code: u16, status: u8, msg: &str) {
        let actually = Mocli::new(config).unwrap_err();
        assert_eq!(actually.0, code, "Incorrect code");
        assert_eq!(actually.1, status, "Incorrect status");
        assert_eq!(actually.2, msg, "Incorrect message");
    }

    fn ok(config: &ConfigMocli, expected: &str) {
        let actually = Mocli::new(config).unwrap();
        assert_eq!(
            format!("{:?}", actually),
            expected,
        );
    }

    #[test]
    fn mocli_new_errors() {

        // Check that bkg_fill is being validated.
        // See config_mocli_validate_bkg_.rs for a complete set of tests.
        let mut bkg_fill_not_none = ConfigMocli::default();
        bkg_fill_not_none.bkg_fill = Some("X".into());
        err(&bkg_fill_not_none, 3_7162, EX_CONFIG,
            "has_bkg is false, but bkg_fill is not None" );

        // Check that view_size is being validated.
        // See config_mocli_validate_view_.rs for a complete set of tests.
        let mut view_size_too_narrow = ConfigMocli::default();
        view_size_too_narrow.view_size = Some((0, 4));
        err(&view_size_too_narrow, 38177, EX_CONFIG,
            "view_size.0 is 0, less than minimum columns 20" );
    }

    #[test]
    fn mocli_new_ok() {

        // With default configuration, Mocli has no outputs.
        ok(&ConfigMocli::default(),
            "Mocli { bkg: None, cmd: None, err: None, log: None, \
            view_size: (20, 4) }" );

        // Setting `config.has_bkg` to `true` should enable background-output.
        // Setting `config.view_size` should set the viewport columns and rows.
        // Note that `config.bkg_fill` is set to a default value.
        let mut just_bkg = ConfigMocli::default();
        just_bkg.has_bkg = true;
        just_bkg.view_size = Some((123, 45));
        ok(&just_bkg,
            "Mocli { bkg: Some(OutputBkg { fill: \".\" }), \
            cmd: None, err: None, log: None, \
            view_size: (123, 45) }" );

        // Setting all `has_*` config properties to `true` should enable all
        // Mocli outputs.
        ok(
            &ConfigMocli {
                bkg_fill: Some("+".into()),
                has_bkg: true,
                has_cmd: true,
                has_err: true,
                has_log: true,
                view_size: None,
            },
            "Mocli { bkg: Some(OutputBkg { fill: \"+\" }), \
            cmd: Some(OutputCmd), err: Some(OutputErr), log: Some(OutputLog), \
            view_size: (20, 4) }",
        );

    }
}
