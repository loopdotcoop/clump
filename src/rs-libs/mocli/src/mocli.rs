//! src/rs-libs/mocli/src/mocli.rs
//! ### `Mocli`
//!
//! The core of a multi-output command line interface.

use crate::output_bkg::output_bkg::OutputBkg;
use crate::output_cmd::output_cmd::OutputCmd;
use crate::output_err::output_err::OutputErr;
use crate::output_log::output_log::OutputLog;

/// The core of a multi-output command line interface.
#[derive(Debug)]
pub struct Mocli {
    pub bkg: Option<OutputBkg>,
    pub cmd: Option<OutputCmd>,
    pub err: Option<OutputErr>,
    pub log: Option<OutputLog>,

    /// Dimensions of the viewport `(columns = 20, rows = 4)`.
    pub view_size: (usize, usize),
}
