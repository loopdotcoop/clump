//! src/rs-libs/mocli/src/output_bkg/output_bkg_new.rs
//! ### `OutputBkg::new()`
//!
//! Creates an OutputBkg instance.

// Local imports.
use crate::config_mocli::config_mocli::ConfigMocli;
use super::output_bkg::OutputBkg;

impl OutputBkg {

    /// Creates an OutputBkg instance.
    pub fn new(config: &ConfigMocli) -> Result<Self, (u16, u8, String)> {

        // Fall back to defaults for optional configuration fields.
        let fill = config.bkg_fill.clone().unwrap_or(".".into());

        Ok(Self {
            fill,
        })
    }
}
