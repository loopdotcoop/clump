//! src/rs-libs/mocli/src/output_log/output_log_new.rs
//! ### `OutputLog::new()`
//!
//! Creates an OutputLog instance.

// Local imports.
use crate::config_mocli::config_mocli::ConfigMocli;
use super::output_log::OutputLog;

impl OutputLog {

    /// Creates an OutputLog instance.
    pub fn new(_config: &ConfigMocli) -> Result<Self, (u16, u8, String)> {
        Ok(Self {
        })
    }
}
