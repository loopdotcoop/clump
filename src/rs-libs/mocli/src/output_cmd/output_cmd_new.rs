//! src/rs-libs/mocli/src/output_cmd/output_cmd_new.rs
//! ### `OutputCmd::new()`
//!
//! Creates an OutputCmd instance.

// Local imports.
use crate::config_mocli::config_mocli::ConfigMocli;
use super::output_cmd::OutputCmd;

impl OutputCmd {

    /// Creates an OutputCmd instance.
    pub fn new(_config: &ConfigMocli) -> Result<Self, (u16, u8, String)> {
        Ok(Self {
        })
    }
}
