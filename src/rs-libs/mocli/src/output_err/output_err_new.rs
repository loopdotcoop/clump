//! src/rs-libs/mocli/src/output_err/output_err_new.rs
//! ### `OutputErr::new()`
//!
//! Creates an OutputErr instance.

// Local imports.
use crate::config_mocli::config_mocli::ConfigMocli;
use super::output_err::OutputErr;

impl OutputErr {

    /// Creates an OutputErr instance.
    pub fn new(_config: &ConfigMocli) -> Result<Self, (u16, u8, String)> {
        Ok(Self {
        })
    }
}
