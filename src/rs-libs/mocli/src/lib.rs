//! src/rs-libs/mocli/src/lib.rs
//! ### `mocli`
//!
//! A library for building multi-output command line interfaces.

// The output structs.
mod output_bkg;
mod output_cmd;
mod output_err;
// mod output_kbd; // TODO keyboard
mod output_log;
// mod output_opt; // TODO options menu
// mod output_prc; // TODO sub-process list
// mod output_tip; // TODO helpful tip

// Publish the Mocli configuration struct.
pub mod config_mocli;

// Publish the Mocli struct, and add instance and static methods to it.
pub mod mocli;
mod mocli_new;
