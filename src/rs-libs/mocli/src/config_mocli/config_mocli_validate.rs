//! src/rs-libs/mocli/src/config_mocli/config_mocli_validate.rs
//! ### `ConfigMocli::validate()`
//!
//! Calls all `ConfigMocli` validation methods, to validate all fields.

// Local imports.
use super::config_mocli::ConfigMocli;

impl ConfigMocli {

    /// Calls all `ConfigMocli` validation methods, to validate all fields.
    pub fn validate(&self) -> Result<&Self, (u16, u8, String)> {
        self
            .validate_bkg_fill()?
            .validate_view_size()
    }

}
