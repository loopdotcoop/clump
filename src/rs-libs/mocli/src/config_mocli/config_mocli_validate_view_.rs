//! src/rs-libs/mocli/src/config_mocli/config_mocli_validate_view_.rs
//! ### `ConfigMocli::validate_view_*()`
//!
//! Methods which validate `ConfigMocli` fields related to the viewport.

// src/rs-libs/ dependencies.
use constants::err_status::EX_CONFIG;

// Local imports.
use super::config_mocli::ConfigMocli;

/// Maximum number of characters in the viewport. 40960 is fairly arbitrary.
const CHARS_MAX: usize = 256 * 160;

/// Maximum viewport width, in characters. 1024 is fairly arbitrary.
const COLS_MAX: usize = 1024;

/// Minimum viewport width, in characters. Supports 20 x 4 LCD displays.
const COLS_MIN: usize = 20;

/// Maximum viewport height, in lines. 640 is fairly arbitrary.
const ROWS_MAX: usize = 640;

/// Minimum viewport height, in lines. Supports 20 x 4 LCD displays.
const ROWS_MIN: usize = 4;

impl ConfigMocli {

    /// Validates `ConfigMocli.view_size`, if present.
    pub fn validate_view_size(&self) -> Result<&Self, (u16, u8, String)> {

        // view_size is an optional field, so return early if not set.
        if self.view_size == None { return Ok(self) }

        // Otherwise, the columns and rows must conform to maximum and minimums.
        let (cols, rows) = self.view_size.unwrap();
        if cols > COLS_MAX { return Err((3_5142, EX_CONFIG, format!(
            "view_size.0 is {}, more than maximum columns {}", cols, COLS_MAX))) }
        if cols < COLS_MIN { return Err((3_8177, EX_CONFIG, format!(
            "view_size.0 is {}, less than minimum columns {}", cols, COLS_MIN))) }
        if rows > ROWS_MAX { return Err((3_9163, EX_CONFIG, format!(
            "view_size.1 is {}, more than maximum rows {}", rows, ROWS_MAX))) }
        if rows < ROWS_MIN { return Err((3_7260, EX_CONFIG, format!(
            "view_size.1 is {}, less than minimum rows {}", rows, ROWS_MIN))) }

        // Also, the number of characters must not exceed the maximum.
        let chars = cols * rows;
        if chars > CHARS_MAX { return Err((3_3356, EX_CONFIG, format!(
            "view_size.0 * view_size.1 is {}, more than maximum characters {}",
            chars,
            CHARS_MAX,
        ))) }

        Ok(self)
    }

}


/* ---------------------------------- TEST ---------------------------------- */

#[cfg(test)]
mod tests {
    use constants::err_status::EX_CONFIG;
    use super::*;

    fn err_view_size(config: &ConfigMocli, code: u16, status: u8, msg: String) {
        let actually = config.validate_view_size().unwrap_err();
        assert_eq!(actually.0, code, "Incorrect code");
        assert_eq!(actually.1, status, "Incorrect status");
        assert_eq!(actually.2, msg, "Incorrect message");
    }

    // Errors due to invalid view_size.
    #[test]
    fn view_size_errors() {

        let mut cols_max_plus_1 = ConfigMocli::default();
        cols_max_plus_1.view_size = Some((COLS_MAX + 1, ROWS_MAX)); // would also exceed CHARS_MAX
        err_view_size(&cols_max_plus_1, 3_5142, EX_CONFIG,
            format!("view_size.0 is {}, more than maximum columns {}", COLS_MAX + 1, COLS_MAX) );

        let mut cols_min_minus_1 = ConfigMocli::default();
        cols_min_minus_1.view_size = Some((COLS_MIN - 1, ROWS_MIN));
        err_view_size(&cols_min_minus_1, 3_8177, EX_CONFIG,
            format!("view_size.0 is {}, less than minimum columns {}", COLS_MIN - 1, COLS_MIN) );

        let mut rows_max_plus_1 = ConfigMocli::default();
        rows_max_plus_1.view_size = Some((COLS_MAX, ROWS_MAX + 1)); // would also exceed CHARS_MAX
        err_view_size(&rows_max_plus_1, 3_9163, EX_CONFIG,
            format!("view_size.1 is {}, more than maximum rows {}", ROWS_MAX + 1, ROWS_MAX) );

        let mut rows_min_minus_1 = ConfigMocli::default();
        rows_min_minus_1.view_size = Some((COLS_MIN, ROWS_MIN - 1));
        err_view_size(&rows_min_minus_1, 3_7260, EX_CONFIG,
            format!("view_size.1 is {}, less than minimum rows {}", ROWS_MIN - 1, ROWS_MIN) );

        let mut cols_max_rows_max = ConfigMocli::default();
        cols_max_rows_max.view_size = Some((COLS_MAX, ROWS_MAX));
        err_view_size(&cols_max_rows_max, 3_3356, EX_CONFIG,
            format!(
                "view_size.0 * view_size.1 is {}, more than maximum characters {}",
                COLS_MAX * ROWS_MAX,
                CHARS_MAX
            )
        );

    }

    // Valid view_size.
    #[test]
    fn view_size_ok() {

        let mut none = ConfigMocli::default();
        none.view_size = None;
        none.validate_view_size().unwrap();

        let mut cols_max_rows_min = ConfigMocli::default();
        cols_max_rows_min.view_size = Some((COLS_MAX , ROWS_MIN));
        cols_max_rows_min.validate_view_size().unwrap();

        let mut cols_min_rows_max = ConfigMocli::default();
        cols_min_rows_max.view_size = Some((COLS_MIN , ROWS_MAX));
        cols_min_rows_max.validate_view_size().unwrap();

        let mut cols_min_rows_min = ConfigMocli::default();
        cols_min_rows_min.view_size = Some((COLS_MIN , ROWS_MIN));
        cols_min_rows_min.validate_view_size().unwrap();

        // Note that `Some((COLS_MAX , ROWS_MAX))` is invalid, see 3_3356 above.
    }
}
