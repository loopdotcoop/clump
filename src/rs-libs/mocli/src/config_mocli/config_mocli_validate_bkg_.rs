//! src/rs-libs/mocli/src/config_mocli/config_mocli_validate_bkg_.rs
//! ### `ConfigMocli::validate_bkg_*()`
//!
//! Methods which validate `ConfigMocli` fields related to `bkg`.

// src/rs-libs/ dependencies.
use constants::err_status::EX_CONFIG;

// Local imports.
use super::config_mocli::ConfigMocli;

impl ConfigMocli {

    /// Validates `ConfigMocli.bkg_fill`, if present.
    pub fn validate_bkg_fill(&self) -> Result<&Self, (u16, u8, String)> {

        // In simple cases:
        // - Mocli has no background output, so bkg_fill must be None; or
        // - Mocli has a bkg output but bkg_fill is None, so a default is used
        if self.has_bkg == false {
            if self.bkg_fill != None { return Err((3_7162, EX_CONFIG, format!(
                "has_bkg is false, but bkg_fill is not None"))) }
        }
        if self.bkg_fill == None { return Ok(self) }

        // Otherwise, bkg_fill must be a single-character printable ASCII string.
        let v = self.bkg_fill.as_ref().unwrap();
        if v.len() > 12 { return Err((3_9801, EX_CONFIG, format!(
            "bkg_fill '{:.12}...' is not 1 char long", v))) }
        if v.chars().count() != 1 as usize { return Err((3_6150, EX_CONFIG, format!(
            "bkg_fill '{}' is not 1 char long", v))) }
        let c = v.chars().next().unwrap();
        if c < ' ' || c > '~' { return Err((3_4858, EX_CONFIG, format!(
            "bkg_fill is invalid char 'U+{:0>4X}'", c as u32))) }

        Ok(self)
    }

}


/* ---------------------------------- TEST ---------------------------------- */

#[cfg(test)]
mod tests {
    use constants::err_status::EX_CONFIG;
    use super::*;

    fn err_bkg_fill(config: &ConfigMocli, code: u16, status: u8, msg: &str) {
        let actually = config.validate_bkg_fill().unwrap_err();
        assert_eq!(actually.0, code, "Incorrect code");
        assert_eq!(actually.1, status, "Incorrect status");
        assert_eq!(actually.2, msg, "Incorrect message");
    }

    // Errors due to invalid bkg_fill.
    #[test]
    fn bkg_fill_errors() {

        let mut not_none = ConfigMocli::default();
        not_none.bkg_fill = Some("!".into());
        err_bkg_fill(&not_none, 3_7162, EX_CONFIG,
            "has_bkg is false, but bkg_fill is not None" );

        let mut is_13_chars = ConfigMocli::default();
        is_13_chars.has_bkg = true;
        is_13_chars.bkg_fill = Some("abcdefghijklm".into());
        err_bkg_fill(&is_13_chars, 3_9801, EX_CONFIG,
            "bkg_fill 'abcdefghijkl...' is not 1 char long" );

        let mut is_12_chars = ConfigMocli::default();
        is_12_chars.has_bkg = true;
        is_12_chars.bkg_fill = Some("0123456789AB".into());
        err_bkg_fill(&is_12_chars, 3_6150, EX_CONFIG,
            "bkg_fill '0123456789AB' is not 1 char long" );

        let mut low_ascii = ConfigMocli::default();
        low_ascii.has_bkg = true;
        low_ascii.bkg_fill = Some("\0".into()); // null
        err_bkg_fill(&low_ascii, 3_4858, EX_CONFIG,
            "bkg_fill is invalid char 'U+0000'" );
        low_ascii.bkg_fill = Some("\n".into()); // newline
        err_bkg_fill(&low_ascii, 3_4858, EX_CONFIG,
            "bkg_fill is invalid char 'U+000A'" );

        let mut is_unicode = ConfigMocli::default();
        is_unicode.has_bkg = true;
        is_unicode.bkg_fill = Some("の".into()); // Hiragana Letter No
        err_bkg_fill(&is_unicode, 3_4858, EX_CONFIG,
            "bkg_fill is invalid char 'U+306E'" );
        is_unicode.bkg_fill = Some("😎".into()); // Smiling Face with Sunglasses
        err_bkg_fill(&is_unicode, 3_4858, EX_CONFIG,
            "bkg_fill is invalid char 'U+1F60E'" );

    }
}
