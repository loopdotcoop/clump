//! src/rs-libs/mocli/src/config_mocli/config_mocli.rs
//! ### `ConfigMocli`
//!
//! Configuration for a new Mocli instance.

/// Configuration for a new Mocli instance.
#[derive(Clone,Debug,Default)]
pub struct ConfigMocli {
    pub bkg_fill: Option<String>,
    pub has_bkg: bool,
    pub has_cmd: bool,
    pub has_err: bool,
    pub has_log: bool,

    /// Dimensions of the viewport `(columns = 20, rows = 4)`.
    pub view_size: Option<(usize, usize)>,
}
