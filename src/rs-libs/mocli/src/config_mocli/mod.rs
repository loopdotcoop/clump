//! src/rs-libs/mocli/src/config_mocli/mod.rs

// Publish the ConfigMocli struct, and add instance methods to it.
pub mod config_mocli;
mod config_mocli_validate_bkg_;
mod config_mocli_validate_view_;
mod config_mocli_validate;
