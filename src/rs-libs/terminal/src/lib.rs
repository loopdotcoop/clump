//! src/rs-libs/terminal/src/lib.rs
//! ### `terminal`
//!
//! Low-level support for MacOS, Unix and Windows terminals.

#[cfg(target_family = "unix")] extern crate libc;

pub mod raw_mode;
#[cfg(target_family = "unix")] pub mod unix;
#[cfg(target_family = "windows")] pub mod windows;
