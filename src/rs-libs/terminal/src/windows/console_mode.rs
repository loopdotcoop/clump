//! src/rs-libs/terminal/src/windows/console_mode.rs
//! ### `console_mode`
//!
//! From:
//! - <https://github.com/crossterm-rs/crossterm-winapi/blob/master/src/lib.rs>
//! - <https://github.com/crossterm-rs/crossterm-winapi/blob/master/src/console_mode.rs>

use std::io;

use super::winapi::shared_minwindef::BOOL;
use super::winapi::um_consoleapi::{GetConsoleMode,SetConsoleMode};

// use super::{result, Handle, HandleType};
use super::handle::Handle;

/// Get the result of a call to WinAPI as an [`io::Result`].
#[inline]
pub fn result(return_value: BOOL) -> io::Result<()> {
    if return_value != 0 {
        Ok(())
    } else {
        Err(io::Error::last_os_error())
    }
}

/// A wrapper around a screen buffer, focusing on calls to get and set the console mode.
///
/// This wraps [`SetConsoleMode`](https://docs.microsoft.com/en-us/windows/console/setconsolemode)
/// and [`GetConsoleMode`](https://docs.microsoft.com/en-us/windows/console/getconsolemode).
#[derive(Debug, Clone)]
pub struct ConsoleMode {
    // the handle used for the functions of this type.
    handle: Handle,
}

impl ConsoleMode {
    /// Set the console mode to the given console mode.
    ///
    /// This function sets the `dwMode`.
    ///
    /// This wraps
    /// [`SetConsoleMode`](https://docs.microsoft.com/en-us/windows/console/setconsolemode).
    pub fn set_mode(&self, console_mode: u32) -> io::Result<()> {
        result(unsafe { SetConsoleMode(*self.handle, console_mode) })
    }

    /// Get the console mode.
    ///
    /// This function returns the `lpMode`.
    ///
    /// This wraps
    /// [`GetConsoleMode`](https://docs.microsoft.com/en-us/windows/console/getconsolemode).
    pub fn mode(&self) -> io::Result<u32> {
        let mut console_mode = 0;
        result(unsafe { GetConsoleMode(*self.handle, &mut console_mode) })?;
        Ok(console_mode)
    }
}

impl From<Handle> for ConsoleMode {
    fn from(handle: Handle) -> Self {
        ConsoleMode { handle }
    }
}
