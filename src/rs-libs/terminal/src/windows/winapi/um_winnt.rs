//! src/rs-libs/terminal/src/windows/winapi/um_winnt.rs
//! ### `um_winnt`
//!
//! This module defines the 32-Bit Windows types and constants that are defined
//! by NT, but exposed through the Win32 API.
//!
//! From:
//! - <https://crates.io/crates/winapi>
//! - <https://docs.rs/winapi/latest/winapi/um/winnt/index.html>
//! - <https://github.com/retep998/winapi-rs/tree/0.3/src/um/winnt.rs>

// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option.
// All files in the project carrying such notice may not be copied, modified, or distributed
// except according to those terms.
// use ctypes::{__int64, __uint64, c_char, c_int, c_long, c_short, c_uint, c_ulong, c_void, wchar_t};
use super::ctypes::{c_void,wchar_t};
// use shared::basetsd::{
//     DWORD64, KAFFINITY, LONG64, LONG_PTR, PDWORD64, PLONG64, SIZE_T, ULONG64, ULONG_PTR,
// };
// use shared::guiddef::{CLSID, GUID};
// use shared::ktmtypes::UOW;
// use shared::minwindef::{BYTE, DWORD, FALSE, PDWORD, TRUE, ULONG, USHORT, WORD};
use super::shared_minwindef::DWORD;
// #[cfg(target_arch = "aarch64")]
// use shared::minwindef::PBYTE;
// use vc::excpt::EXCEPTION_DISPOSITION;
// use vc::vcruntime::size_t;
// pub const ANYSIZE_ARRAY: usize = 1;
// #[cfg(target_pointer_width = "32")]
// IFDEF!{
// pub const MAX_NATURAL_ALIGNMENT: usize = 4;
// pub const MEMORY_ALLOCATION_ALIGNMENT: usize = 8;
// }
// #[cfg(target_pointer_width = "64")]
// IFDEF!{
// pub const MAX_NATURAL_ALIGNMENT: usize = 8;
// pub const MEMORY_ALLOCATION_ALIGNMENT: usize = 16;
// }
// pub const SYSTEM_CACHE_ALIGNMENT_SIZE: usize = 64;
// pub type PVOID = *mut c_void;
// pub type PVOID64 = u64; // This is a 64-bit pointer, even when in 32-bit
// pub type VOID = c_void;
// pub type CHAR = c_char;
// pub type SHORT = c_short;
// pub type LONG = c_long;
// pub type INT = c_int;
pub type WCHAR = wchar_t;
// pub type PWCHAR = *mut WCHAR;
// pub type LPWCH = *mut WCHAR;
// pub type PWCH = *mut WCHAR;
// pub type LPCWCH = *const WCHAR;
// pub type PCWCH = *const WCHAR;
// pub type NWPSTR = *mut WCHAR;
// pub type LPWSTR = *mut WCHAR;
// pub type PWSTR = *mut WCHAR;
// pub type PZPWSTR = *mut PWSTR;
// pub type PCZPWSTR = *const PWSTR;
// pub type LPUWSTR = *mut WCHAR; // Unaligned pointer
// pub type PUWSTR = *mut WCHAR; // Unaligned pointer
pub type LPCWSTR = *const WCHAR;
// pub type PCWSTR = *const WCHAR;
// pub type PZPCWSTR = *mut PCWSTR;
// pub type PCZPCWSTR = *const PCWSTR;
// pub type LPCUWSTR = *const WCHAR; // Unaligned pointer
// pub type PCUWSTR = *const WCHAR; // Unaligned pointer
// pub type PZZWSTR = *mut WCHAR;
// pub type PCZZWSTR = *const WCHAR;
// pub type PUZZWSTR = *mut WCHAR; // Unaligned pointer
// pub type PCUZZWSTR = *const WCHAR; // Unaligned pointer
// pub type PNZWCH = *mut WCHAR;
// pub type PCNZWCH = *const WCHAR;
// pub type PUNZWCH = *mut WCHAR; // Unaligned pointer
// pub type PCUNZWCH = *const WCHAR; // Unaligned pointer
// pub type LPCWCHAR = *const WCHAR;
// pub type PCWCHAR = *const WCHAR;
// pub type LPCUWCHAR = *const WCHAR; // Unaligned pointer
// pub type PCUWCHAR = *const WCHAR; // Unaligned pointer
// pub type UCSCHAR = c_ulong;
// pub const UCSCHAR_INVALID_CHARACTER: UCSCHAR = 0xffffffff;
// pub const MIN_UCSCHAR: UCSCHAR = 0;
// pub const MAX_UCSCHAR: UCSCHAR = 0x0010FFFF;
// pub type PUCSCHAR = *mut UCSCHAR;
// pub type PCUCSCHAR = *const UCSCHAR;
// pub type PUCSSTR = *mut UCSCHAR;
// pub type PUUCSSTR = *mut UCSCHAR; // Unaligned pointer
// pub type PCUCSSTR = *const UCSCHAR;
// pub type PCUUCSSTR = *const UCSCHAR; // Unaligned pointer
// pub type PUUCSCHAR = *mut UCSCHAR; // Unaligned pointer
// pub type PCUUCSCHAR = *const UCSCHAR; // Unaligned pointer
// pub type PCHAR = *mut CHAR;
// pub type LPCH = *mut CHAR;
// pub type PCH = *mut CHAR;
// pub type LPCCH = *const CHAR;
// pub type PCCH = *const CHAR;
// pub type NPSTR = *mut CHAR;
// pub type LPSTR = *mut CHAR;
// pub type PSTR = *mut CHAR;
// pub type PZPSTR = *mut PSTR;
// pub type PCZPSTR = *const PSTR;
// pub type LPCSTR = *const CHAR;
// pub type PCSTR = *const CHAR;
// pub type PZPCSTR = *mut PCSTR;
// pub type PCZPCSTR = *const PCSTR;
// pub type PZZSTR = *mut CHAR;
// pub type PCZZSTR = *const CHAR;
// pub type PNZCH = *mut CHAR;
// pub type PCNZCH = *const CHAR;
// // Skipping TCHAR things
// pub type PSHORT = *mut SHORT;
// pub type PLONG = *mut LONG;
// pub const ALL_PROCESSOR_GROUPS: WORD = 0xffff;
// STRUCT!{struct PROCESSOR_NUMBER {
//     Group: WORD,
//     Number: BYTE,
//     Reserved: BYTE,
// }}
// pub type PPROCESSOR_NUMBER = *mut PROCESSOR_NUMBER;
// STRUCT!{struct GROUP_AFFINITY {
//     Mask: KAFFINITY,
//     Group: WORD,
//     Reserved: [WORD; 3],
// }}
// pub type PGROUP_AFFINITY = *mut GROUP_AFFINITY;
// #[cfg(target_pointer_width = "32")]
// pub const MAXIMUM_PROC_PER_GROUP: BYTE = 32;
// #[cfg(target_pointer_width = "64")]
// pub const MAXIMUM_PROC_PER_GROUP: BYTE = 64;
// pub const MAXIMUM_PROCESSORS: BYTE = MAXIMUM_PROC_PER_GROUP;
pub type HANDLE = *mut c_void;
// pub type PHANDLE = *mut HANDLE;
// pub type FCHAR = BYTE;
// pub type FSHORT = WORD;
// pub type FLONG = DWORD;
// pub type HRESULT = c_long;
// pub type CCHAR = c_char;
// pub type LCID = DWORD;
// pub type PLCID = PDWORD;
// pub type LANGID = WORD;

// ... about 1500 commented-out lines removed, to reduce repo size

// pub const ACCESS_SYSTEM_SECURITY: DWORD = 0x01000000;
// pub const MAXIMUM_ALLOWED: DWORD = 0x02000000;
pub const GENERIC_READ: DWORD = 0x80000000;
pub const GENERIC_WRITE: DWORD = 0x40000000;
// pub const GENERIC_EXECUTE: DWORD = 0x20000000;
// pub const GENERIC_ALL: DWORD = 0x10000000;

// ... 1000s of commented-out lines removed, to reduce repo size

// pub const FILE_GENERIC_EXECUTE: DWORD = STANDARD_RIGHTS_EXECUTE | FILE_READ_ATTRIBUTES
//     | FILE_EXECUTE | SYNCHRONIZE;
pub const FILE_SHARE_READ: DWORD = 0x00000001;
pub const FILE_SHARE_WRITE: DWORD = 0x00000002;
// pub const FILE_SHARE_DELETE: DWORD = 0x00000004;
// pub const FILE_ATTRIBUTE_READONLY: DWORD = 0x00000001;

// ... 1000s of commented-out lines removed, to reduce repo size
