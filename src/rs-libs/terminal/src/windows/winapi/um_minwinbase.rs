//! src/rs-libs/terminal/src/windows/winapi/um_minwinbase.rs
//! ### `um_minwinbase`
//!
//! This module defines the 32-Bit Windows Base APIs
//!
//! From:
//! - <https://crates.io/crates/winapi>
//! - <https://docs.rs/winapi/latest/winapi/um/minwinbase/index.html>
//! - <https://github.com/retep998/winapi-rs/blob/0.3/src/um/minwinbase.rs>

#![allow(non_camel_case_types)]

// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option.
// All files in the project carrying such notice may not be copied, modified, or distributed
// except according to those terms
// use shared::basetsd::ULONG_PTR;
// use shared::minwindef::{BOOL, BYTE, DWORD, FILETIME, HMODULE, LPVOID, MAX_PATH, UINT, ULONG, WORD};
use super::shared_minwindef::{BOOL,DWORD,LPVOID};
// use shared::ntstatus::{
//     STATUS_ACCESS_VIOLATION, STATUS_ARRAY_BOUNDS_EXCEEDED, STATUS_BREAKPOINT,
//     STATUS_CONTROL_C_EXIT, STATUS_DATATYPE_MISALIGNMENT, STATUS_FLOAT_DENORMAL_OPERAND,
//     STATUS_FLOAT_DIVIDE_BY_ZERO, STATUS_FLOAT_INEXACT_RESULT, STATUS_FLOAT_INVALID_OPERATION,
//     STATUS_FLOAT_OVERFLOW, STATUS_FLOAT_STACK_CHECK, STATUS_FLOAT_UNDERFLOW,
//     STATUS_GUARD_PAGE_VIOLATION, STATUS_ILLEGAL_INSTRUCTION, STATUS_INTEGER_DIVIDE_BY_ZERO,
//     STATUS_INTEGER_OVERFLOW, STATUS_INVALID_DISPOSITION, STATUS_INVALID_HANDLE,
//     STATUS_IN_PAGE_ERROR, STATUS_NONCONTINUABLE_EXCEPTION, STATUS_PENDING,
//     STATUS_POSSIBLE_DEADLOCK, STATUS_PRIVILEGED_INSTRUCTION, STATUS_SINGLE_STEP,
//     STATUS_STACK_OVERFLOW,
// };
// use um::winnt::{
//     CHAR, EXCEPTION_RECORD, HANDLE, LPSTR, LPWSTR, PCONTEXT, PRTL_CRITICAL_SECTION,
//     PRTL_CRITICAL_SECTION_DEBUG, PVOID, RTL_CRITICAL_SECTION, RTL_CRITICAL_SECTION_DEBUG, WCHAR,
// };
// //MoveMemory
// //CopyMemory
// //FillMemory
// //ZeroMemory
// STRUCT!{struct SECURITY_ATTRIBUTES {
//     nLength: DWORD,
//     lpSecurityDescriptor: LPVOID,
//     bInheritHandle: BOOL,
// }}
#[allow(dead_code,non_snake_case)]
pub struct SECURITY_ATTRIBUTES {
    nLength: DWORD,
    lpSecurityDescriptor: LPVOID,
    bInheritHandle: BOOL,
}
// pub type PSECURITY_ATTRIBUTES = *mut SECURITY_ATTRIBUTES;
pub type LPSECURITY_ATTRIBUTES = *mut SECURITY_ATTRIBUTES;
// STRUCT!{struct OVERLAPPED_u_s {
//     Offset: DWORD,
//     OffsetHigh: DWORD,
// }}

// ... about 300 commented-out lines removed, to reduce repo size
