//! src/rs-libs/terminal/src/windows/winapi/um_fileapi.rs
//! ### `um_fileapi`
//!
//! ApiSet Contract for api-ms-win-core-file-l1.
//!
//! From:
//! - <https://crates.io/crates/winapi>
//! - <https://docs.rs/winapi/latest/winapi/um/fileapi/index.html>
//! - <https://github.com/retep998/winapi-rs/blob/0.3/src/um/fileapi.rs>

// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option.
// All files in the project carrying such notice may not be copied, modified, or distributed
// except according to those terms.
// use shared::minwindef::{
//     BOOL, DWORD, FILETIME, LPCVOID, LPDWORD, LPFILETIME, LPVOID, PDWORD, PUCHAR, UCHAR, UINT,
//     ULONG, WORD,
// };
use super::shared_minwindef::DWORD;
// use um::minwinbase::{
//     FILE_INFO_BY_HANDLE_CLASS, FINDEX_INFO_LEVELS, FINDEX_SEARCH_OPS, GET_FILEEX_INFO_LEVELS,
//     LPOVERLAPPED, LPOVERLAPPED_COMPLETION_ROUTINE, LPSECURITY_ATTRIBUTES, LPWIN32_FIND_DATAA,
//     LPWIN32_FIND_DATAW
// };
use super::um_minwinbase::LPSECURITY_ATTRIBUTES;
// use um::winnt::{
//     BOOLEAN, CCHAR, FILE_ID_128, FILE_SEGMENT_ELEMENT, HANDLE, LARGE_INTEGER, LONG, LONGLONG,
//     LPCSTR, LPCWSTR, LPSTR, LPWCH, LPWSTR, PLARGE_INTEGER, PLONG, PULARGE_INTEGER, PWSTR,
//     ULONGLONG, WCHAR,
// };
use super::um_winnt::{HANDLE,LPCWSTR};
// pub const CREATE_NEW: DWORD = 1;
// pub const CREATE_ALWAYS: DWORD = 2;
pub const OPEN_EXISTING: DWORD = 3;
// pub const OPEN_ALWAYS: DWORD = 4;
// pub const TRUNCATE_EXISTING: DWORD = 5;
// pub const INVALID_FILE_SIZE: DWORD = 0xFFFFFFFF;
// pub const INVALID_SET_FILE_POINTER: DWORD = 0xFFFFFFFF;
// pub const INVALID_FILE_ATTRIBUTES: DWORD = 0xFFFFFFFF;

// ...about 100 commented-out lines removed, to reduce repo size

// STRUCT!{struct FILE_ID_INFO {
//     VolumeSerialNumber: ULONGLONG,
//     FileId: FILE_ID_128,
// }}
extern "system" {
//     pub fn CompareFileTime(
//         lpFileTime1: *const FILETIME,
//         lpFileTime2: *const FILETIME,
//     ) -> LONG;
//     pub fn CreateDirectoryA(
//         lpPathName: LPCSTR,
//         lpSecurityAttributes: LPSECURITY_ATTRIBUTES,
//     ) -> BOOL;
//     pub fn CreateDirectoryW(
//         lpPathName: LPCWSTR,
//         lpSecurityAttributes: LPSECURITY_ATTRIBUTES,
//     ) -> BOOL;
//     pub fn CreateFileA(
//         lpFileName: LPCSTR,
//         dwDesiredAccess: DWORD,
//         dwShareMode: DWORD,
//         lpSecurityAttributes: LPSECURITY_ATTRIBUTES,
//         dwCreationDisposition: DWORD,
//         dwFlagsAndAttributes: DWORD,
//         hTemplateFile: HANDLE,
//     ) -> HANDLE;
    #[allow(improper_ctypes)]
    pub fn CreateFileW(
        lpFileName: LPCWSTR,
        dwDesiredAccess: DWORD,
        dwShareMode: DWORD,
        lpSecurityAttributes: LPSECURITY_ATTRIBUTES,
        dwCreationDisposition: DWORD,
        dwFlagsAndAttributes: DWORD,
        hTemplateFile: HANDLE,
    ) -> HANDLE;
//     pub fn DefineDosDeviceW(
//         dwFlags: DWORD,
//         lpDeviceName: LPCWSTR,
//         lpTargetPath: LPCWSTR,
//     ) -> BOOL;

// ... about 400 commented-out lines removed, to reduce repo size

//     pub fn GetCompressedFileSizeW(
//         lpFileName: LPCWSTR,
//         lpFileSizeHigh: LPDWORD,
//     ) -> DWORD;
}
// ENUM!{enum STREAM_INFO_LEVELS {
//     FindStreamInfoStandard,
//     FindStreamInfoMaxInfoLevel,
// }}
// extern "system" {
//     pub fn FindFirstStreamW(
//         lpFileName: LPCWSTR,
//         InfoLevel: STREAM_INFO_LEVELS,
//         lpFindStreamData: LPVOID,
//         dwFlags: DWORD,
//     ) -> HANDLE;
//     pub fn FindNextStreamW(
//         hFindStream: HANDLE,
//         lpFindStreamData: LPVOID,
//     ) -> BOOL;
//     pub fn AreFileApisANSI() -> BOOL;
//     pub fn GetTempPathA(
//         nBufferLength: DWORD,
//         lpBuffer: LPSTR,
//     ) -> DWORD;
//     pub fn FindFirstFileNameW(
//         lpFileName: LPCWSTR,
//         dwFlags: DWORD,
//         StringLength: LPDWORD,
//         LinkName: PWSTR,
//     ) -> HANDLE;
//     pub fn FindNextFileNameW(
//         hFindStream: HANDLE,
//         StringLength: LPDWORD,
//         LinkName: PWSTR,
//     ) -> BOOL;
//     pub fn GetVolumeInformationA(
//         lpRootPathName: LPCSTR,
//         lpVolumeNameBuffer: LPSTR,
//         nVolumeNameSize: DWORD,
//         lpVolumeSerialNumber: LPDWORD,
//         lpMaximumComponentLength: LPDWORD,
//         lpFileSystemFlags: LPDWORD,
//         lpFileSystemNameBuffer: LPSTR,
//         nFileSystemNameSize: DWORD,
//     ) -> BOOL;
//     pub fn GetTempFileNameA(
//         lpPathName: LPCSTR,
//         lpPrefixString: LPCSTR,
//         uUnique: UINT,
//         lpTempFileName: LPSTR,
//     ) -> UINT;
//     pub fn SetFileApisToOEM();
//     pub fn SetFileApisToANSI();
// }
