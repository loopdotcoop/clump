//! src/rs-libs/terminal/src/windows/winapi/ctypes.rs
//! ### `ctypes`
//!
//! Built in primitive types provided by the C language.
//!
//! From:
//! - <https://crates.io/crates/winapi>
//! - <https://docs.rs/winapi/latest/winapi/ctypes/index.html>
//! - <https://github.com/retep998/winapi-rs/blob/0.3/src/lib.rs#L34>
//!

#![allow(non_camel_case_types)]

#[cfg(feature = "std")]
pub use std::os::raw::c_void;
#[cfg(not(feature = "std"))]
pub enum c_void {}

// pub type c_char = i8;
// pub type c_schar = i8;
// pub type c_uchar = u8;
// pub type c_short = i16;
// pub type c_ushort = u16;
pub type c_int = i32;
// pub type c_uint = u32;
// pub type c_long = i32;
pub type c_ulong = u32;
// pub type c_longlong = i64;
// pub type c_ulonglong = u64;
// pub type c_float = f32;
// pub type c_double = f64;
// pub type __int8 = i8;
// pub type __uint8 = u8;
// pub type __int16 = i16;
// pub type __uint16 = u16;
// pub type __int32 = i32;
// pub type __uint32 = u32;
// pub type __int64 = i64;
// pub type __uint64 = u64;
pub type wchar_t = u16;
