//! src/rs-libs/terminal/src/windows/winapi/mod.rs
//! ### `winapi`
//!
//! This module takes some Windows-specific code from the 'winapi' crate:
//! - <https://crates.io/crates/winapi>
//! - <https://github.com/retep998/winapi-rs>
//!
//! See the original crate for license and in-depth documentation.

pub mod ctypes;
pub mod shared_minwindef;
pub mod um_consoleapi;
pub mod um_fileapi;
pub mod um_handleapi;
pub mod um_minwinbase;
pub mod um_wincon;
pub mod um_winnt;
