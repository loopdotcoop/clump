//! src/rs-libs/terminal/src/windows/winapi/um_wincon.rs
//! ### `um_wincon`
//!
//! This module contains the public data structures, data types, and procedures
//! exported by the NT console subsystem.
//!
//! From:
//! - <https://crates.io/crates/winapi>
//! - <https://docs.rs/winapi/latest/winapi/um/wincon/index.html>
//! - <https://github.com/retep998/winapi-rs/tree/0.3/src/um/wincon.rs>

// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option.
// All files in the project carrying such notice may not be copied, modified, or distributed
// except according to those terms.
// use ctypes::c_void;
// use shared::minwindef::{BOOL, DWORD, LPDWORD, LPVOID, LPWORD, UINT, ULONG, WORD};
use super::shared_minwindef::DWORD;
// use shared::windef::{COLORREF, HWND};
// use um::minwinbase::SECURITY_ATTRIBUTES;

// ... about 100 commented-out lines removed, to reduce repo size

// pub const CTRL_LOGOFF_EVENT: DWORD = 5;
// pub const CTRL_SHUTDOWN_EVENT: DWORD = 6;
pub const ENABLE_PROCESSED_INPUT: DWORD = 0x0001;
pub const ENABLE_LINE_INPUT: DWORD = 0x0002;
pub const ENABLE_ECHO_INPUT: DWORD = 0x0004;
// pub const ENABLE_WINDOW_INPUT: DWORD = 0x0008;
// pub const ENABLE_MOUSE_INPUT: DWORD = 0x0010;b

// ... about 350 commented-out lines removed, to reduce repo size
