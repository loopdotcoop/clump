//! src/rs-libs/terminal/src/windows/mod.rs
//! ### `windows`
//!
//! Windows has its own interface for command-line terminals (PowerShell etc),
//! which is very different to the 'termios' standard of Unix-like platforms.
//!
//! This module takes some Windows-specific code from the 'crossterm',
//! 'crossterm-winapi' and 'winapi' crates:
//! - <https://github.com/crossterm-rs/crossterm>
//! - <https://crates.io/crates/crossterm>
//!   - Has 3 direct dependencies, none are needed here
//!   - Has 8 direct `OPTIONAL` deps, 'crossterm-winapi' is needed here
//! - <https://github.com/crossterm-rs/crossterm-winapi>
//! - <https://crates.io/crates/crossterm-winapi>
//!   - Has 1 direct dependency, which is 'winapi'
//! - <https://github.com/retep998/winapi-rs>
//! - <https://crates.io/crates/winapi>
//!   - Has 2 direct dependencies, but neither are needed here
//!
//! See the original crates for licenses and in-depth documentation.

pub mod console_mode;
pub use self::console_mode::ConsoleMode;

pub mod handle;
pub use self::handle::Handle;

pub mod winapi;

/// Define bits which can't be set in raw mode.
pub const NOT_RAW_MODE_MASK: winapi::shared_minwindef::DWORD =
    winapi::um_wincon::ENABLE_LINE_INPUT |
    winapi::um_wincon::ENABLE_ECHO_INPUT |
    winapi::um_wincon::ENABLE_PROCESSED_INPUT;
