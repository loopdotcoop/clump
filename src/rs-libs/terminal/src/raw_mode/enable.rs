//! src/rs-libs/terminal/src/raw_mode/enable.rs
//! ### `enable()`
//!
//! Switches the terminal to 'raw' mode, where the app handles all keypresses.

// src/rs-libs/ dependencies.
use constants::err_status::EX_UNAVAILABLE;

// Local imports.
#[cfg(target_family = "unix")] use crate::unix;
#[cfg(target_family = "unix")] use unix::termios::{Termios, ECHO, ICANON, TCSANOW, tcsetattr};
#[cfg(target_family = "windows")] use crate::windows;
#[cfg(target_family = "windows")] use windows::{ConsoleMode,Handle,NOT_RAW_MODE_MASK};

/// ### `enable()`
/// Switches the terminal to 'raw' mode, where the app handles all keypresses.
#[cfg(target_family = "unix")]
pub fn enable() -> Result<(), (u16, u8, String)> {

    // Try to create a Termios instance.
    let mut termios = match Termios::from_fd(0) {
        Err(_err) => return Err((3_6587, EX_UNAVAILABLE,
            "enable() cannot read Termios".into())), // eg a Node `spawn` process
        Ok(ok) => ok,
    };

    // Remove echo and canonical mode from the Termios instance.
    // These operations do nothing if it didn't have these modes enabled.
    termios.c_lflag &= !(ICANON | ECHO);

    // Try to set the updated terminal modes.
    match tcsetattr(0, TCSANOW, &mut termios) {
        Err(_err) => Err((3_2847, EX_UNAVAILABLE,
            "enable() cannot modify Termios".into())),
        Ok(_) => Ok(()),
    }

    // TODO maybe alternatively, reset the stdin to original termios data - more guaranteed to restore the user's expected Terminal behavior.
    // tcsetattr(0, TCSANOW, &original_termios).unwrap();
}

/// ### `enable()`
/// Switches the terminal to 'raw' mode, where the app handles all keypresses.
#[cfg(target_family = "windows")]
pub fn enable() -> Result<(), (u16, u8, String)> {

    // Try to get the handle of the terminal input buffer.
    let handle = match Handle::current_in_handle() {
        Err(_err) => return Err((3_5403, EX_UNAVAILABLE,
            "enable() cannot get a terminal Handle".into())),
        Ok(ok) => ok,
    };

    // Wrap the handle in an instance which provides various Windows methods.
    let console_mode = ConsoleMode::from(handle);

    // Get the terminal's `lpMode`.
    let lp_mode = match console_mode.mode() {
        Err(_err) => return Err((3_4928, EX_UNAVAILABLE,
            "enable() cannot get terminal's lpMode".into())),
        Ok(ok) => ok,
    };

    let dw_mode = lp_mode & ! NOT_RAW_MODE_MASK;

    // Set the terminal's `dwMode`.
    match console_mode.set_mode(dw_mode) {
        Err(_err) => return Err((3_9607, EX_UNAVAILABLE,
            "enable() cannot set terminal's dwMode".into())),
        Ok(_) => Ok(()),
    }
}
