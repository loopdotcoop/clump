//! src/rs-libs/terminal/src/raw_mode/disable.rs
//! ### `disable()`
//!
//! Switches off 'raw' mode, and revert to the usual behavior where the terminal
//! buffers and pre-processes keypresses.

// src/rs-libs/ dependencies.
use constants::err_status::EX_UNAVAILABLE;

// Local imports.
#[cfg(target_family = "unix")] use crate::unix;
#[cfg(target_family = "unix")] use unix::termios::{Termios, ECHO, ICANON, TCSANOW, tcsetattr};
#[cfg(target_family = "windows")] use crate::windows;
#[cfg(target_family = "windows")] use windows::{ConsoleMode,Handle,NOT_RAW_MODE_MASK};

/// ### `disable()`
/// Switches off 'raw' mode, and reverts to normal behavior, where the terminal
/// buffers and pre-processes keypresses.
#[cfg(target_family = "unix")]
pub fn disable() -> Result<(), (u16, u8, String)> {

    // Try to create a Termios instance.
    let mut termios = match Termios::from_fd(0) {
        Err(_err) => return Err((3_3978, EX_UNAVAILABLE,
            "disable() cannot read Termios".into())), // eg a Node `spawn` process
        Ok(ok) => ok,
    };

    // Add echo and canonical mode to the Termios instance.
    // These operations do nothing if it already had these modes enabled.
    termios.c_lflag |= ECHO;
    termios.c_lflag |= ICANON;

    // Try to set the updated terminal modes.
    match tcsetattr(0, TCSANOW, &mut termios) {
        Err(_err) => Err((3_7520, EX_UNAVAILABLE,
            "disable() cannot modify Termios".into())),
        Ok(_) => Ok(()),
    }
}

/// ### `disable()`
/// Switches off 'raw' mode, and reverts to normal behavior, where the terminal
/// buffers and pre-processes keypresses.
#[cfg(target_family = "windows")]
pub fn disable() -> Result<(), (u16, u8, String)> {

    // Try to get the handle of the terminal input buffer.
    let handle = match Handle::current_in_handle() {
        Err(_err) => return Err((3_4879, EX_UNAVAILABLE,
            "disable() cannot get a terminal Handle".into())),
        Ok(ok) => ok,
    };

    // Wrap the handle in an instance which provides various Windows methods.
    let console_mode = ConsoleMode::from(handle);

    // Get the terminal's `lpMode`.
    let lp_mode = match console_mode.mode() {
        Err(_err) => return Err((3_4488, EX_UNAVAILABLE,
            "disable() cannot get terminal's lpMode".into())),
        Ok(ok) => ok,
    };

    let dw_mode = lp_mode | NOT_RAW_MODE_MASK;

    // Set the terminal's `dwMode`.
    match console_mode.set_mode(dw_mode) {
        Err(_err) => return Err((3_9184, EX_UNAVAILABLE,
            "disable() cannot set terminal's dwMode".into())),
        Ok(_) => Ok(()),
    }
}
