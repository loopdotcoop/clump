//! src/rs-libs/terminal/src/raw_mode/is_enabled.rs
//! ### `is_enabled()`
//!
//! Returns `true` is the terminal is currently in 'raw' mode.

// src/rs-libs/ dependencies.
use constants::err_status::EX_UNAVAILABLE;

// Local imports.
#[cfg(target_family = "unix")] use crate::unix;
#[cfg(target_family = "unix")] use unix::termios::{Termios, ECHO, ICANON};
#[cfg(target_family = "windows")] use crate::windows;
#[cfg(target_family = "windows")] use windows::{ConsoleMode,Handle,NOT_RAW_MODE_MASK};

/// ### `is_enabled()`
/// Returns `true` is the terminal is currently in 'raw' mode.
#[cfg(target_family = "unix")]
pub fn is_enabled() -> Result<bool, (u16, u8, String)> {

    // Try to create a Termios instance.
    let termios = match Termios::from_fd(0) {
        Err(_err) => return Err((3_1398, EX_UNAVAILABLE,
            "is_enabled() cannot read Termios".into())), // eg a Node `spawn` process
        Ok(ok) => ok,
    };

    // tcflag_t  c_iflag;     input modes         27394
    // tcflag_t  c_oflag;     output modes        3
    // tcflag_t  c_cflag;     control modes       19200
    // tcflag_t  c_lflag;     local modes         1219
    // cc_t      c_cc[NCCS];  special characters  [4, 255, 255, 127, 23, 21, 18, 255, 3, 28, 26, 25, 17, 19, 22, 15, 1, 0, 20, 255]
    //           c_ispeed                         9600
    //           c_ospeed                         9600

    // println!("c_lflag:              {:32b}", termios.c_lflag);
    // println!("ICANON:               {:32b}", ICANON);
    // println!("c_lflag & ICANON:     {:32b}", termios.c_lflag & ICANON);
    // println!("ECHO:                 {:32b}", ECHO);
    // println!("c_lflag & ECHO:       {:32b}", termios.c_lflag & ECHO);

    let has_echo_enabled = (termios.c_lflag & ECHO) != 0;
    let has_canonical_enabled = (termios.c_lflag & ICANON) != 0;
    let has_neither_enabled = ! has_echo_enabled && ! has_canonical_enabled;

    Ok(has_neither_enabled)
}

/// ### `is_enabled()`
/// Determines whether the terminal is currently in 'raw' mode.
#[cfg(target_family = "windows")]
pub fn is_enabled() -> Result<bool, (u16, u8, String)> {

    // Try to get the handle of the terminal input buffer.
    let handle = match Handle::current_in_handle() {
        Err(_err) => return Err((3_6189, EX_UNAVAILABLE,
            "is_enabled() cannot get a terminal Handle".into())),
        Ok(ok) => ok,
    };

    // Wrap the handle in an instance which provides various Windows methods.
    let console_mode = ConsoleMode::from(handle);

    // Get the terminal's `lpMode`.
    let lp_mode = match console_mode.mode() {
        Err(_err) => return Err((3_2317, EX_UNAVAILABLE,
            "is_enabled() cannot get terminal's lpMode".into())),
        Ok(ok) => ok,
    };

    // Return `true` if none of the 'not raw' bits are set.
    let has_no_raw_bits_enabled = lp_mode & NOT_RAW_MODE_MASK == 0;
    Ok(has_no_raw_bits_enabled)
}
