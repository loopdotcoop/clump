//! src/rs-libs/terminal/src/termios/termios.rs
//! ### `Termios`
//!
//! Provides `Termios` functionality for MacOS and many Linux flavours.
//! - <https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios/>
//! - <https://man7.org/linux/man-pages/man3/termios.3.html>
//!
//! From <https://github.com/dcuddeback/termios-rs/blob/master/src/lib.rs>
//! See the original source code for MIT license and in-depth documentation.

extern crate libc;

use std::io;
use std::mem;
use std::ops::{Deref,DerefMut};
use std::os::unix::io::RawFd;

use libc::{c_int,pid_t};

pub use super::target::{cc_t,speed_t,tcflag_t}; // types
pub use super::target::{VEOF,VEOL,VERASE,VINTR,VKILL,VMIN,VQUIT,VSTART,VSTOP,VSUSP,VTIME}; // c_cc subscripts
pub use super::target::{BRKINT,ICRNL,IGNBRK,IGNCR,IGNPAR,INLCR,INPCK,ISTRIP,IXANY,IXOFF,IXON,PARMRK}; // input modes
pub use super::target::{OPOST,ONLCR,OCRNL,ONOCR,ONLRET}; // output modes
pub use super::target::{B0,B50,B75,B110,B134,B150,B200,B300,B600,B1200,B1800,B2400,B4800,B9600,B19200,B38400}; // baud rate selection
pub use super::target::{CSIZE,CS5,CS6,CS7,CS8,CSTOPB,CREAD,PARENB,PARODD,HUPCL,CLOCAL}; // control modes
pub use super::target::{ECHO,ECHOE,ECHOK,ECHONL,ICANON,IEXTEN,ISIG,NOFLSH,TOSTOP}; // local modes
pub use super::target::{TCSANOW,TCSADRAIN,TCSAFLUSH}; // attribute selection
pub use super::target::{TCIFLUSH,TCIOFLUSH,TCOFLUSH,TCIOFF,TCION,TCOOFF,TCOON}; // line control

#[derive(Debug,Copy,Clone,Eq,PartialEq)]
pub struct Termios {
    inner: super::target::termios
}

impl Termios {
    /// Creates a `Termios` structure based on the current settings of a file descriptor.
    ///
    /// `fd` must be an open file descriptor for a terminal device.
    pub fn from_fd(fd: RawFd) -> io::Result<Self> {
        #[allow(deprecated,invalid_value)] // TODO fix this properly
        let mut termios = unsafe { mem::uninitialized() };

        match tcgetattr(fd, &mut termios) {
            Ok(_) => Ok(termios),
            Err(err) => Err(err)
        }
    }

    fn inner(&self) -> &super::target::termios {
        &self.inner
    }

    fn inner_mut(&mut self) -> &mut super::target::termios {
        &mut self.inner
    }
}

impl Deref for Termios {
    type Target = super::target::termios;

    fn deref(&self) -> &super::target::termios {
        self.inner()
    }
}

impl DerefMut for Termios {
    fn deref_mut(&mut self) -> &mut super::target::termios {
        self.inner_mut()
    }
}

/// Sets flags to disable all input and output processing.
pub fn cfmakeraw(termios: &mut Termios) {
    unsafe { super::ffi::cfmakeraw(termios.inner_mut()) };
}

/// Blocks until all output written to the file descriptor is transmitted.
pub fn tcdrain(fd: RawFd) -> io::Result<()> {
    io_result(unsafe { super::ffi::tcdrain(fd) })
}

/// Suspends or restarts transmission or reception of data.
pub fn tcflow(fd: RawFd, action: c_int) -> io::Result<()> {
    io_result(unsafe { super::ffi::tcflow(fd, action) })
}

/// Discards data waiting in the terminal device's buffers.
pub fn tcflush(fd: RawFd, queue_selector: c_int) -> io::Result<()> {
    io_result(unsafe { super::ffi::tcflush(fd, queue_selector) })
}

/// Populates a `Termios` structure with parameters associated with a terminal.
pub fn tcgetattr(fd: RawFd, termios: &mut Termios) -> io::Result<()> {
    io_result(unsafe { super::ffi::tcgetattr(fd, termios.inner_mut()) })
}

/// Sets a terminal device's parameters.
pub fn tcsetattr(fd: RawFd, action: c_int, termios: &Termios) -> io::Result<()> {
    io_result(unsafe { super::ffi::tcsetattr(fd, action, termios.inner()) })
}

/// Returns the process group ID of the controlling terminal's session.
pub fn tcgetsid(fd: RawFd) -> pid_t {
    unsafe { super::ffi::tcgetsid(fd) }
}

/// Transmits data to generate a break condition.
pub fn tcsendbreak(fd: RawFd, duration: c_int) -> io::Result<()> {
    io_result(unsafe { super::ffi::tcsendbreak(fd, duration) })
}


#[inline]
fn io_result(result: c_int) -> io::Result<()> {
    match result {
        0 => Ok(()),
        _ => Err(io::Error::last_os_error())
    }
}
