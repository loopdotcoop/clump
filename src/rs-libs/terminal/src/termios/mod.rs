//! src/rs-libs/terminal/src/termios/mod.rs
//! ### `termios`
//!
//! A trimmed-down copy of David Cuddeback's v0.3.3 'termios' library, which
//! also has a flatter file structure.
//! - <https://crates.io/crates/termios>
//! - <https://github.com/dcuddeback/termios-rs>
//!
//! See the original source code for MIT license and in-depth documentation.

pub mod ffi;
pub mod termios;

#[cfg(target_os = "macos")] pub mod os_macos;
#[cfg(target_os = "macos")] pub use self::os_macos as target;

// TODO add the various Linux flavours, and Android