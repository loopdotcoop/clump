//! src/rs-libs/terminal/src/unix/mod.rs
//! ### `unix`
//!
//! 'termios' is the standard in/out interface for command-line terminals on
//! Unix-like platforms (essentially, everything that's not Windows).
//! - <https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios/>
//! - <https://man7.org/linux/man-pages/man3/termios.3.html>
//!
//! This module is a termios implementation for MacOS and many Linux flavours.
//! The code is a trimmed-down copy of David Cuddeback's termios v0.3.3 library:
//! - <https://crates.io/crates/termios>
//! - <https://github.com/dcuddeback/termios-rs>
//!
//! See the original crate for MIT license and in-depth documentation.

pub mod ffi;
pub mod termios;

#[cfg(target_os = "macos")] pub mod os_macos;
#[cfg(target_os = "macos")] pub use self::os_macos as target;

// TODO add the various Linux flavours, and Android