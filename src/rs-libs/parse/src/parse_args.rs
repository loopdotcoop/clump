//! src/rs-libs/parse/src/parse_args.rs
//! ### `parse_args()`
//!
//! Transforms a space and/or tab delimited string into a vector of strings.
//! - Spaces are preserved inside "double-quoted" and 'single-quoted' sections
//! - A backslashed quote is treated as a literal quote
//! - A backslashed backslash is treated as a literal backslash

// src/rs-libs/ dependencies.
use constants::err_status::EX_DATAERR;

/// ### `parse_args()`
/// Transforms a string into a vector of strings - understands quoted args.
/// - Spaces are preserved inside "double-quoted" and 'single-quoted' sections
/// - A backslashed quote is treated as a literal quote
/// - A backslashed backslash is treated as a literal backslash
pub fn parse_args(raw_args: String) -> Result<Vec<String>, (u16, u8, String)> {

    // Initialise the output vector. Exit early if the input is an empty string.
    let mut args: Vec<String> = vec![];
    if raw_args.len() == 0 { return Ok(args) }

    // Initialise mutable variables used in the `for ...` loop below.
    let mut arg: String = "".into();
    let mut esc = false; // true after a backslash
    let mut in_dq = false; // true while we're "inside a double-quoted arg"
    let mut in_sq = false; // true while we're 'inside a single-quoted arg'
    let mut is_in = false; // false while we're inbetween args

    // Step through each character in the input string.
    for (i, c) in raw_args.chars().enumerate() {
        match c {

            // Found a space or tab.
            ' ' | '\t' => {
                if !is_in {
                    // inbetween args, so ignore this char and do nothing
                } else if in_dq || in_sq {
                    arg.push(c) // in a quoted arg, so keep it
                } else {
                    is_in = false;
                    args.push(arg); // record the current arg
                    arg = "".into(); // start a new arg
                }
            },

            // Found a double-quote.
            '"' => {
                if esc {
                    esc = false;
                    arg.push('"')
                } else if !is_in { // begin a double-quoted arg
                    in_dq = true;
                    is_in = true;
                } else if in_dq { // end a double-quoted arg
                    in_dq = false;
                    is_in = false;
                    args.push(arg); // record the double-quoted arg
                    arg = "".into(); // start a new arg
                } else if in_sq {
                    arg.push(c); // eg: 'Something "like" this'
                } else { // eg: Something"Like"This
                    return Err((3_1501, EX_DATAERR, format!(
                        "Double-quote at index {} should be prefixed with a backslash", i)))
                }
            },

            // Found a single-quote.
            '\'' => {
                if esc {
                    esc = false;
                    arg.push('\'')
                } else if !is_in { // begin a single-quoted arg
                    in_sq = true;
                    is_in = true;
                } else if in_sq { // end a single-quoted arg
                    in_sq = false;
                    is_in = false;
                    args.push(arg); // record the single-quoted arg
                    arg = "".into(); // start a new arg
                } else if in_dq {
                    arg.push(c); // eg: "Something 'like' this"
                } else { // eg: OrSomething'Like'This
                    return Err((3_1794, EX_DATAERR, format!(
                        "Single-quote at index {} should be prefixed with a backslash", i)))
                }
            },

            // Found a backslash.
            '\\' => {
                if esc { // a pair of backslashes become one backslash
                    esc = false;
                    arg.push('\\');
                } else {
                    esc = true;
                }
            },

            // Found some other character.
            _ => {
                is_in = true;
                if esc { // the previous backslash is treated literally
                    esc = false;
                    arg.push('\\');
                }
                arg.push(c);
            },
        }
    }

    // At the end of the input string, we should not be inside a quoted argument.
    if in_dq || in_sq { // eg: "one" "two
        return Err((3_1972, EX_DATAERR, format!(
            "Missing {}-quote at the end of the input string",
            if in_dq { "double" } else {"single"})))
    }

    // Treat a trailing backslash as a literal '\' character, and then record
    // the final argument.
    if esc { arg.push('\\') }
    if arg.len() > 0 { args.push(arg) }

    Ok(args)
}


/* ---------------------------------- TEST ---------------------------------- */

#[cfg(test)]
mod tests {
    use super::*;

    fn err(raw_args: &str, code: u16, status: u8, msg: &str) {
        let actually = parse_args(raw_args.into()).unwrap_err();
        assert_eq!(actually.0, code, "Incorrect code");
        assert_eq!(actually.1, status, "Incorrect status");
        assert_eq!(actually.2, msg, "Incorrect message");
    }

    fn ok(raw_args: &str, exp: Vec<&str>) {
        let actually = parse_args(raw_args.into()).unwrap();
        let expected = exp.iter().map(|s| s.to_string()).collect::<Vec<String>>();
        assert_eq!(
            format!("{:?}", actually),
            format!("{:?}", expected),
        );
    }

    #[test]
    fn parse_args_errors() {

        // Errors due to unescaped quotes inside a non-quoted string.
        err("Something\"Like\"This", 3_1501, EX_DATAERR,
            "Double-quote at index 9 should be prefixed with a backslash");
        err("OrSomething'Like'This", 3_1794, EX_DATAERR,
            "Single-quote at index 11 should be prefixed with a backslash");

        // Errors due to an unbalanced quote.
        err("A \"B C\" D \"E F", 3_1972, EX_DATAERR,
            "Missing double-quote at the end of the input string");
        err("A 'B C' D 'E", 3_1972, EX_DATAERR,
            "Missing single-quote at the end of the input string");
    }

    #[test]
    fn parse_args_ok() {

        // An empty string should create an empty vector.
        ok("", vec![]);

        // Same, if entirely composed of spaces and tabs.
        ok(" ", vec![]);
        ok("   ", vec![]);
        ok("\t", vec![]);
        ok("\t\t\t\t", vec![]);
        ok(" \t\t   \t \t", vec![]);

        // A single non-space character should create a vector with one string.
        ok("\n", vec!["\n"]); // 1 byte, U+000A 'Line Feed (LF)'
        ok("A", vec!["A"]); // 1 byte, U+0041 'Latin Capital Letter A'
        ok("é", vec!["é"]); // 2 bytes, U+00E9 'Latin Small Letter E with Acute'
        ok("万", vec!["万"]); // 3 bytes, U+4E07 "various, many, all" in Japanese
        ok("😊", vec!["😊"]); // 4 bytes, U+1F60A 'Smiling Face with Smiling Eyes'

        // A run of non-space characters should create a vector with one string.
        ok("Abc_Xyz", vec!["Abc_Xyz"]);
        ok("万\né😊A", vec!["万\né😊A"]);

        // Leading and trailing spaces and tabs should be ignored.
        ok(" A", vec!["A"]);
        ok("A\t", vec!["A"]);
        ok(" \t \t\t  Abc", vec!["Abc"]);
        ok("A😊\n万é   \t \t\t", vec!["A😊\n万é"]);

        // Two args separated by spaces should create a vector with 2 strings.
        ok("A B", vec!["A", "B"]);
        ok("A\tB", vec!["A", "B"]);
        ok("Abc   Xyz", vec!["Abc", "Xyz"]);
        ok("Abc\t\tXyz", vec!["Abc", "Xyz"]);
        ok("😊\n万\t\tAé", vec!["😊\n万", "Aé"]);

        // Multiple args separated and surrounded by spaces should also work.
        ok("A B C", vec!["A", "B", "C"]);
        ok("  Abc   Def    Ghi     ", vec!["Abc", "Def", "Ghi"]);
        ok("\t😊万 \t \n \t Aé\t  ", vec!["😊万", "\n", "Aé"]);

        // If entirely double-quoted, spaces, tabs and single-quotes are ok.
        ok("\"A B\"", vec!["A B"]);
        ok("\" \"", vec![" "]);
        ok("\"'\"", vec!["'"]);
        ok("\"Abc\t \tDef  \tGhi\"", vec!["Abc\t \tDef  \tGhi"]);
        ok("\"  万\né  \t  ''😊A\t\t\"", vec!["  万\né  \t  ''😊A\t\t"]);

        // If entirely single-quoted, spaces, tabs and double-quotes are ok.
        ok("'A B'", vec!["A B"]);
        ok("' '", vec![" "]);
        ok("'\"'", vec!["\""]);
        ok("'Abc\t \tDef  \tGhi'", vec!["Abc\t \tDef  \tGhi"]);
        ok("'  万\né  \t  \"\"😊A\t\t'", vec!["  万\né  \t  \"\"😊A\t\t"]);

        // Empty strings are allowed in double or single-quotes.
        ok("\"\"", vec![""]);
        ok("''", vec![""]);
        ok("A B \"\" C", vec!["A", "B", "", "C"]);
        ok("A B '' C", vec!["A", "B", "", "C"]);

        // A mixture of double and single-quoted args are ok.
        ok("  A \"B C\" \t D\tE 'F\tG' \t", vec!["A", "B C", "D", "E", "F\tG"]);
        ok("'\t😊万 \t' \nAé\t \" \t\" ", vec!["\t😊万 \t", "\nAé", " \t"]);

        // A single backslash is kept as-is, unless followed by a quote.
        ok("\\", vec!["\\"]);
        ok("\\A", vec!["\\A"]);
        ok("A\\", vec!["A\\"]);
        ok("\\'", vec!["'"]);
        ok("A\\'", vec!["A'"]);
        ok("\\'A 'B'", vec!["'A", "B"]);
        ok("\\\"", vec!["\""]);
        ok("A\\\"", vec!["A\""]);
        ok("\\\"A \"B\"", vec!["\"A", "B"]); // works outside quoted args...
        ok("A \"B\\\"\" C", vec!["A", "B\"", "C"]); // ...and inside too
        ok(" 'So that\\\'s' good ", vec!["So that's", "good"]);

        // A pair of backslashes become a single, even if followed by a quote.
        ok("\\\\", vec!["\\"]); // 2 become 1
        ok("\\\\\\", vec!["\\\\"]); // 3 become 2
        ok("\\\\\\\\", vec!["\\\\"]); // 4 also become 2
        ok("\\\\\\\\\\", vec!["\\\\\\"]); // 5 become 3
        ok("'A B\\\\'", vec!["A B\\"]); // the use-case for double-backslashes
        ok("  \"  A  B  \\\\\"  ", vec!["  A  B  \\"]);
    }
}
