# How to: Embed a font

If all resources (images, CSS, code, etc) are going to be inlined in the built
HTML files, then that should include fonts. 

To speed up page rendering times, custom fonts should have the smallest possible
file size, and we should only include the glyphs that will actually be used.

## Download candidate fonts

Since this will be used as a 'display' typeface, filesize can be reduced by only
including uppercase A-Z, digits 0-9, and a few punctuation characters.

Search for "2023 free monospace font".

- <https://hackingcpp.com/dev/coding_fonts.html>
- <https://www.fontsquirrel.com/fonts/list/classification/monospaced>
- <https://www.myfonts.com/collections/tags/monospace-fonts?toggle[has_free]=true&sortBy=universal_search_data_price_asc>
- <https://fontesk.com/tag/mono/>
- <https://fonts.google.com/?preview.text=LOOP.COOP%20%2F%20CLUMP&classification=Monospace>

On each of the sites above, download some likely looking fonts:

- <https://damieng.com/blog/2008/05/26/envy-code-r-preview-7-coding-font-released/> 5893 bytes *
- <https://github.com/zlsa/monoOne/blob/master/monoOne.otf> 6121 *
- <https://input.djr.com/download/> 7745 (InputMonoCondensed) *
- <https://pcaro.es/hermit/#downloads> 8965 *
- <https://tobiasjung.name/profont/> 4569
- <https://typeof.net/Iosevka/> <https://fontesk.com/iosevka-typeface/> 13401
- <https://www.wfonts.com/font/ocr-a-extended> 9793 *
- <https://www.fontsquirrel.com/fonts/ia-writer-duospace> 6353 (duospace is an interesting concept) *
- <https://www.fontsquirrel.com/fonts/lekton> 8654 (reg) * / 8709 (bold)
- <https://www.fontsquirrel.com/fonts/M-1m> 3497 (small) **
- <https://www.fontsquirrel.com/fonts/saxMono> 15337
- <https://www.fontsquirrel.com/fonts/space-mono> 11093
- <https://www.fontsquirrel.com/fonts/ubuntu-mono> 19865
- <https://www.myfonts.com/products/demo-realtime-414493> 4609
- <https://www.myfonts.com/products/light-demo-gravitica-mono-78793> 4201
- <https://www.myfonts.com/products/light-italic-vin-mono-pro-203650> 5201, use `-webkit-text-stroke: 0.05rem #fff`) *
- <https://www.myfonts.com/products/light-ki-238649> 3585 (small, use `-webkit-text-stroke: 0.05rem #fff`) **
- <https://www.myfonts.com/products/mono-polli-sans-14231> 8609
- <https://www.myfonts.com/products/straight-light-italic-monostep-323315> 4645 *
- <https://fontesk.com/agave-font/> 3641 (small) *
- <https://fontesk.com/astral-mono-font/> 4874 *
- <https://fontesk.com/awoof-mono-font/> 3366 *, so use the (older?) fontesk one
- <https://fontesk.com/barecast-font/> 4145
- <https://fontesk.com/binchotan-font/> 3225 (small) *
- <https://fontesk.com/caffeine-mono-font/> 6625 (too big)
- <https://fontesk.com/calling-code-font/> 5157
- <https://fontesk.com/cascadia-code-font/> 13945 (semibold, huge)
- <https://fontesk.com/chivo-mono-typeface/> 5713 (regular)
- <https://fontesk.com/codetta-typeface/> 9521
- <https://fontesk.com/compagnon-typeface/> 13105
- <https://fontesk.com/conta-typeface/> 2281 (pixel, tiny!) * / 6137 (mono) *
- <https://fontesk.com/datcub-font/> broken?
- <https://fontesk.com/deimos-font/> 4453
- <https://fontesk.com/disket-mono-typeface/> 8581 (too big)
- <https://fontesk.com/exan-3-font/> 8473 (nice but no punctuation)
- <https://fontesk.com/fantastique-mono-typeface/> 10581 (too big)
- <https://fontesk.com/feature-mono-typeface/> 14085
- <https://fontesk.com/for-personal-gain-font/> 5013 (not profit) * / 5173 (for profit) *
- <https://fontesk.com/funtauna-typeface/> 4697 (block serif) *
- <https://fontesk.com/jetbrains-mono-typeface/> 15318
- <https://fontesk.com/kraft-mono-font/> 3813
- <https://fontesk.com/lab-mono-font/> 4001 *
- <https://fontesk.com/league-mono-typeface/> 4425 (condensed) * / 4597 (narrow) *
- <https://fontesk.com/lilex-typeface/> 10561
- <https://fontesk.com/luculent-typeface/> 7273
- <https://fontesk.com/lulu-monospace-font/> 3673 *
- <https://fontesk.com/major-mono-font/> 8917
- <https://fontesk.com/manolo-mono-font/> 2689 (tiny and nice, tho < ^ > are bad) *
- <https://fontesk.com/modern-society-font/> 3617 (small and nice, tho ^ is bad) *
- <https://fontesk.com/mono-font/> 5433
- <https://fontesk.com/monoid-typeface/> 6869
- <https://fontesk.com/mononoki-typeface/> 8105
- <https://fontesk.com/moongloss-typeface/> 4649 (thin) * / 4593 (medium) **
- <https://fontesk.com/narnita-font/> (narita) 5393
- <https://fontesk.com/necto-mono-font/> 6421 *
- <https://fontesk.com/nf-code/> 4297 **
- <https://fontesk.com/punk-mono-typeface/> ridiculously big
- <https://fontesk.com/quota-typeface/> 4521 (regular cond) * / 4649 (bold) *
- <https://fontesk.com/reno-mono-font/> 3623
- <https://fontesk.com/snap-it-mono-font/> 4021 *
- <https://fontesk.com/sometype-mono-typeface/> 5345 *
- <https://fontesk.com/trispace-typeface/> 5945 (sem-cond reg) *
- <https://fontesk.com/vortex-mono-font/> 4385 (missing < ^ >) *
- <https://fontesk.com/whois-mono-font/> 4441 *
- <https://fontesk.com/wordclock-stencil-mono-font/> 3221 (tiny, no punct or digits) *
- <https://fontesk.com/yuki-code-font/> 5385 **
- <https://fonts.google.com/specimen/PT+Mono> 4985 (from woff2) ** / 18785 (from ttf)
- <https://fonts.google.com/specimen/Share+Tech+Mono> 3593 (from woff2) ** / 7089 (from ttf)
- <https://fonts.google.com/specimen/Nova+Mono> 3761 (from woff2) **

## Create a test page containing words typically found in headings and buttons

```
<a href="#">LOOP.COOP</a> / <a href="#">CLUMP</a> / LOCATIONS

<a class="btn">= LIST</a> <a class="btn">? HELP</a> <a class="btn">^ USER</a> <a class="btn">* LIKE</a>

PROFILE / SETTINGS / COLLECTIONS

TUNE / PATCH / SEQ / SYNTH / <a class="btn">&lt; PREV</a> <a class="btn">&gt; NEXT</a>

&lt; ^ &gt; ? * = + - . / 0 1 2 3 4 5 6 7 8 9

<a class="btn">=</a> <a class="btn">?</a> <a class="btn">^</a> <a class="btn">*</a>
```

## Demo each candidate, and check the filesize of a reduced character-list

1. Download each font
2. Visit <https://transfonter.org/>
3. Click 'Add fonts' and select a font file, preferably .woff2, if not .otf
4. Switch off 'Family support'
5. Enable 'Base64 encode'
6. Only select 'WOFF2'
7. Characters: `ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 <^>?*=+-./`
8. Click 'Convert'
9. Click 'Download'
10. Check the size of the 'stylesheet.css' file, eg 3150 bytes
11. If not too big, open 'stylesheet.css' in VS Code
12. Copy paste the `src: url(...) format('woff2');` into the fonts.css files
13. Note the new filesize
14. Refresh the test page
15. If it looks good, add that font to the shortlist

## Choose a shortlist from the candidate fonts

In order of filesize:

### Conta

- 2281 <https://fontesk.com/conta-typeface/> (pixel, small) * / 6137 (mono) *
- The pixelated version of Conta has the smallest filesize by quite a long way
- Available punctuation: `. , : ; … ! ? · * # / \ ( ) { } [ ] - _ " ' ^`
- With `ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 {}?#*=+-/`, it's 2389 bytes
- Spacing is a bit off, eg too much space after "L" of "LOOP" and "CLUMP"

### Manolo

- 2689 <https://fontesk.com/manolo-mono-font/> (tiny and nice, tho < ^ > are bad) *
- Nice constructivist typeface style, characterful but practical... nice!
- Up close, the wedge-corners are a bit military / sports
- Second smallest filesize by quite a long way
- Available punctuation: `! ” # $ % & ' * + , - . / : , = ? @ [ ] _ { } ~ “`
- With `ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 {}?#*=+-/`, it's 2885 bytes

### Wordclock Stencil

- 3221 <https://fontesk.com/wordclock-stencil-mono-font/> (tiny, no punct or digits) *
- Looks quite interesting for "LOOP.COOP" and "CLUMP"
- A bit much for using everywhere though... and the "S" is a bit horrible
- With just `LOP.CLUM`, it's 1561 bytes

### Binchotan Sharp

- 3225 <https://fontesk.com/binchotan-font/> (small) *
- Smallest font which has all the proper punctuation
- In fact, the font contains a pretty good Latin 1 set
- capsule-shaped "O" is nicely on-brand for "LOOP.COOP"

### Awoof

- 3365 <https://fontesk.com/awoof-mono-font/> *
- Up close, the wedge-corners are a bit military / sports
- A bit wonky here and there

### M-1m (2009 upload to fontsquirrel)

- 3497 <https://www.fontsquirrel.com/fonts/M-1m> (small) **
- Smallest filesize that I've awarded two "*"s
- LICENSE: proper free and open source
- Clear, straightforward, no-frills
- Five different weights
- Full ASCII punctuation, plus a few more handy punctuation characters
- Japanese and CJK, with fullwidth "Ｍ" (for "CLUMP"), and **fullwidth punct**
- Old version of 2024's M PLUS Code - saves 700 bytes and has no hook on the "C"

### Ki Light

- 3585 <https://www.myfonts.com/products/light-ki-238649> (small, use `-webkit-text-stroke: 0.05rem #fff`) **
- Second smallest filesize that I've awarded two "*"s, by 40 bytes
- Capsule-shaped "O" and angled "C" cut are both on-brand for "LOOP.COOP"
- Stylised "M" makes "CLUMP" look like a unique logo... it's not that nice though
- Really unusual &lt; and &gt; signs
- Four weight variants, all with italics
- Quite wide looking, and has no cond variants
- All properly designed ASCII punctuation, Latin 1 accented letters, and cryllic

### Share Tech
- 3593 <https://fonts.google.com/specimen/Share+Tech+Mono> (from woff2) ** / 7089 (from ttf)
- Third smallest filesize that I've awarded two "*"s, by 50 bytes
- Very clear, the "O" etc is a rectangle with rounded corners
- Nice open, techy "S" and "C"
- Full ASCII punctuation, plus many Latin 1 accented letters
- Only one weight, no italic or cond, and no broader sans-family to explore

### Modern Society

- 3617 <https://fontesk.com/modern-society-font/> (small and nice, tho ^ is bad) *
- The "O" etc is a rectangle with rounded corners - can mirror popups, buttons etc
- Most but not all ASCII punctuation
- Only one weight, no italic or cond, and no broader sans-family to explore

### Agave

- 3641 <https://fontesk.com/agave-font/> **
- Fourth smallest filesize that I've awarded two "*"s, by 55 bytes
- LICENSE: Open source <https://github.com/blobject/agave/blob/master/LICENSE>
- Note that the TTF from github -> 9105 bytes, so use the (older?) fontesk one
- It's a nice indie project <https://b.agaric.net/page/agave>
- Very plain (apart from the "Q"), but nice open "C" and "S"
- The best looking punctuation, I think - the punctuation gets "***"
- Very nice, properly designed Latin 1, box drawing, eg "≡"
- Bold and italic are a work in progress
- Fairly wide (not too bad though), and has no cond variants

### Nova Mono

- 3761 <https://fonts.google.com/specimen/Nova+Mono> (from woff2) **
- The fifth smallest filesize that I've awarded two "*"s, a jump of 120 bytes
- Capsule-shaped "O" and angled "C" cut are both on-brand for "LOOP.COOP"
- Stylised rounded "A" and "N"
- There's a 'Nova Round' (not monospaced) with a wide, more readable "M" for "CLUMP"
- And in fact there are a bunch of 'Nova' fonts, though no weight or cond variants
- Contains nice properly designed Latin 1, Greek, box drawing, geometric shapes, eg "≣"
- I think 'Yuki Code' is a kind of Japanese extension of this one

### M PLUS Code (2024)

- 4177 <https://fonts.google.com/specimen/M+PLUS+Code+Latin> *
- LICENSE: proper free and open source, <https://mplusfonts.github.io/>
- Clear, straightforward, has a nice hook on the "C" for "COOP" and "CLUMP"
- Seven different weights at <https://fonts.google.com/specimen/M+PLUS+1+Code>
- Various sans-serif alternates <https://fonts.google.com/?query=M+plus>
- All properly designed ASCII punctuation, Latin 1 accented letters, and cryllic
- Japanese and CJK, with fullwidth "Ｍ" (for "CLUMP"), and **fullwidth punct**
- New version of 2009's M-1m - adds 700 bytes and has a hook on the "C"

### NF Code

- 4297 <https://fontesk.com/nf-code/> **
- The sixth smallest filesize that I've awarded two "*"s
- Very clear, with on-brand capsule "O"s
- It's a nice open source indie project <https://steve.gigou.fr/fonts.php>
- TTF and OTF on <https://github.com/sgigou/NF-Code> are identical to fontesk
- Full ASCII punctuation, I think, plus many Latin 1 accented letters
- Only one weight, no italic or cond, and no broader sans-family to explore

### MoonGloss

- 4649 <https://fontesk.com/moongloss-typeface/> (thin) * / 4593 (medium) **
- The seventh with two "*"s
- LICENSE: Open Font license
- Three weights, ASCII (but no more than that), properly designed punctuation
- Rounded end-caps, which may sit better with clumps
- The "O" etc is a rectangle with rounded corners - can mirror popups, buttons etc

### PT Mono

- 4985 <https://fonts.google.com/specimen/PT+Mono> (from woff2) ** / 18785 (from ttf)
- The eighth with two "*"s
- Characterful but practical "L" and "C" serifs - "L" appears in lots of places!
- Would fit with Rockwell or Podkova, which Loop.Coop has used in the past
- Letter spacing of "CLUMP" is perfect
- Contains very nice, properly designed Latin 1, full box drawing, geometric shapes, eg "≡"
- <https://www.paratype.com/fonts/pt/pt-mono> bold WOFF2 -> 11285, TTF -> 11333
- No italic or cond
- Has (related?) PT font families to explore

### Yuki Code

- 5385 <https://fontesk.com/yuki-code-font/> **
- The ninth with two "*"s
- Really lovely and unique (but subtle!) details
- capsule-shaped "O" and angled "C" cut are both on-brand for "LOOP.COOP"
- A bit more punctuation than just ASCII, but not a lot more - properly designed
- Includes Greek, Japanese and CJK!
- I think this is a kind of Japanese extension of 'Nova Mono'

### Runners up

- 3673 <https://fontesk.com/lulu-monospace-font/> *
- 4001 <https://fontesk.com/lab-mono-font/> *
- 4021 <https://fontesk.com/snap-it-mono-font/> *
- 4385 <https://fontesk.com/vortex-mono-font/> (missing < ^ >) *
- 4425 <https://fontesk.com/league-mono-typeface/> (condensed) * / 4597 (narrow) *
- 4441 <https://fontesk.com/whois-mono-font/> *
- 4521 <https://fontesk.com/quota-typeface/> (regular cond) * / 4649 (bold) *
- 4645 <https://www.myfonts.com/products/straight-light-italic-monostep-323315> *
- 4697 <https://fontesk.com/funtauna-typeface/> (block serif) *
- 4874 <https://fontesk.com/astral-mono-font/> *
- 5013 <https://fontesk.com/for-personal-gain-font/> (not profit) * / 5173 (for profit) *
- 5201 <https://www.myfonts.com/products/light-italic-vin-mono-pro-203650>  use `-webkit-text-stroke: 0.05rem #fff`) *
- 5345 <https://fontesk.com/sometype-mono-typeface/> *
- 5893 <https://damieng.com/blog/2008/05/26/envy-code-r-preview-7-coding-font-released/> *
- 5945 <https://fontesk.com/trispace-typeface/> (sem-cond reg) *
- 6121 <https://github.com/zlsa/monoOne/blob/master/monoOne.otf> *
- 6353 <https://www.fontsquirrel.com/fonts/ia-writer-duospace> (duospace is an interesting concept) *
- 6421 <https://fontesk.com/necto-mono-font/> *
- 7745 <https://input.djr.com/download/> (InputMonoCondensed) *
- 8654 <https://www.fontsquirrel.com/fonts/lekton> (reg) * / 8709 (bold)
- 8965 <https://pcaro.es/hermit/#downloads> *
- 9793 <https://www.wfonts.com/font/ocr-a-extended> *

## Try a combo of Binchotan Sharp for capital letters and Agave for punctuation

The `@font-face` declarations are 3532 bytes in total, or 3689 with backslashed
newlines. An interesting experiment. It adds a lot of complexity though, so
ideally stick to one font.

```css
@font-face {
    font-family: caps;
    src: url('data:font/woff2;charset=utf-8;base64,d09GMgABAAAAAAVkAAwAAAAAC7wAAAUUAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGhYGYAA8EQgKjFiJLQs\
    +AAE2AiQDRAQgBYR8B14b/ghRlC5KiOxngm1Mq+FgKMMI0ha2k8WeIgSe4XsWQXXo3v2nVAqnC0aDTSATg6KQkFXxP/47bRbR24LYagu/\
    alPqgkrZHJgcxPyd4D04aft3oI7mhnhJM70fIt5c5oiXQIi0RE6U3/b/zaltXG1V3xzeyIExE2ZGTZjLT38OkwJeBoqAhZ\
    +yAOgrzISdUVuMs2KQ2ywFn4MPLJoAxgAkAsGbE53qWEwC8pYngEfr8niLC1KuIEFSoNU0fOp8rR8cP35mxe9/ill94qy4JIO8kls60AB+YMaN4BGPMKsbEEj0rNawVKbH26\
    +dtyFiK2CLxJwDOw5bjniBvHCACpAAV5TGg4iuRwrWo7MrbSW3kct9rAoJ5FEt2NhLx3Y1JP8B7eIRsGOsnKzljMSabptQVvfvFuTviELspCl9c80ADDGlmdlgJtcerfeaEWqGxaG+Kb1I1f6m/\
    qLFn1/Vm3DgNpCT7yQe7PNVms31l3oJakpfwWYcFCXZosUkzRSTBMXpfI41WAEDCMZAgRoSNa8l55plhQWygEAEA5PNc7XQUj/\
    JUAztFeDORDQDVgQjQXqG3K0YeWgLbXucSg2I0MhRDsoDRq3FpVSaZrJRMudNVSdMVzezqKhQwVefDHNzoM5adaEmMYG5ts5arJVFdBC0hGo0UUnO7eXgeBJJhKgBnbfBUu22aUBNm1NadaApjUp\
    Jrm5ANMn62kFOq5GTKnO/qdm6xxqs9Fac82Lnxjh2Lwu7RJNOV/IQs+9Y/LDkWWQHz3PGd0YOeHFrsaAr0RQl90RXxpbodCUHMwe808fxyjZeGSdEiimD6rOAEpc0Fv08fbM27o9gG8aG2Qp/\
    ZtfcPO1YMLm40GF3cQtm8WD87k3Ai1UdOiRLT\
    +2oyhzg4djB8TzHDu7rAF4UMPtrd9buhywWli59KMuSpWD4KR7EB0FgbqnQkbVua4lOZ9JkAx3nlWnYGBrFFDzBYwIegqG38AuYh7EOdJZqhTwQJ6x06RYScOA8ULF4k1YZLlR0KnmNChP\
    +ZRgnf229qOtiVTU26c7lF+CE+ax1bLqeSVa3d7BujCzmbp62hpq64OXlpqbLjXs/UKLUduXJVr/KxdF96a3VnFGVjdgy6lm37mSv36TCKlNI3sPt/\
    OEUobClkGD40B2VgDMWqYuLsFLoNwxhNfqxKXd4q8ytF4IMj2hpEI5f4ro3fmk08CdwiCGDzgrB3IpFIharDYAXhzgMsMF1AQAAiDfO3By4Q1A222Z8lTPUCwAAszEPqg+/K/zf1bDSsqc\
    +BfpIxGyo/5ZlVl4Iwv+esLa2M1DjC07OHDjmBuy70ZZ6c1bqqqSxYh7WIAW7VgjY6jl4x9QuBrZwToItjiSZHFJEapHK1YN02PmoLu4iN6IqAFx8KSH4\
    +BOSTXgRSn1EEFp0TCYSfrFLSumafxN86t1gH1XzDIM6Udv51CmX5/aMEAprD38YNNCLro8uEfq5FbV+ozR+bx+8Y5j2/+iuqxgHCpbBAPZafyQlR\
    +RLy3U3Ixqv8nlsZy7EIwnMYXgWa189ZKC33r3ZjlSmM3CiQnZLYg+0LmI7N0iI/2eYPR4mQhGWgOkBI6CL37+CfNtan2Hr7qpHpXs5LpuGi0g9x+K\
    +TamaBZQjmMzm9aB4aUSKLEL26UhJxHr264+o7h7MgTOM0h0AcZH4AhegLWpVIhz+/OblzYLQGCwOTyCSyADyiOJeFxUTl5CUkpaRlZMHAAAA') format('woff2');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}
@font-face {
    font-family: punct;
    src: url('data:font/woff2;charset=utf-8;base64,\
    d09GMgABAAAAAAPkAAwAAAAACFAAAAOUAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGhYGYABcEQgKhBiDIwscAAE2AiQDIAQgBYcZBzwb6wYjEdaDA7J/Ftht1AEVdgpNOgyEAYssH6weg9o/\
    8QODoHA2jsbuHkK0bJkt3yq1OWRXrUiERGH0OywahZJgFM4dPmiH/3JXJiU9QUzFdDO+MQmcpFFTf/8DjBM83Yx8/BSmBV+Oof7d3r4lx5pXEv64d/4rT\
    +Q1xLqUXmNOiseksTVcngW8B63lnQcWBZpGXUt6NVdBWydo1fNuAgNAEsJAKEV8qtSTukbsSE+e/yySTjf1a7e0SkoZAQCv7/GV+LGnuKVBW4KAosIegoSMJbLAEgUvGOlIWva4cucpUvG/\
    2OMSJsJe6d547ZV7jgrKI+JfCQzBBhhhgV/0MAAFEKQ01bOIAfKNB6W0aXFTXXpsPwMdfwaJyiMv1dnEczf\
    +BaUdTBLPWYblBWkR3c8oJeLgl6Sf5VZkleZl5udmF5TnlMkeSR1q5aPViHqJJjTKqNy0P6uVZ7aRfm1oSbq2/2WTbvGMgrVMvCTshmq96pJaP6uj\
    +blUTxEX3D8lxR8C9UtLvdzw1kMg6uAaw45MJHn/Ezhh6QVMOzEvF6e3uI1n1D421ln3LFU00cUQxjClDUyORR3kDVsbuLkgLj4QWwWCyk/bdVachl/\
    cTMF05rCKEPXhbTWU5C1RIAV9RqrwDzX3vPu6emj0+taDWw7aHzubnJU9kzTBNOQKADDeg3t5w5c3XoDfZWhpUhl64tI/BpSCy9sVZkAAMOBXa/6zEs2w3x2mAgC4f/\
    b9DgB4PXmscUM0YOWmDR0JBG0Nimbh0m4EvX4u3EABbtz4sfCG9N9Ktx8GN95VCcSv8QOygrVwQAC7IECodhGQtB0hIBs6RkDhQ9V3CS1D99DV9wp9ewpgYFc/\
    DGVf00dObZFF6eLKYwxCL8aQjGIdso1wZC1QrUQ0tGxEjqSbRuHQdxMfMHAVN2GolWrdyIk4sdGDu+hKxTLLPUBRZSS1wWrME5yCxgMFJwW7H5vrckQRifWSXgk3IEoo1mps5fPSzDb2mIfJ/\
    CTePMMd7fhZNd1O39pBoPJKwQSogP+2fKCxUakNWolIjHAeh8sn3HUWODKoIU6KyLK2wEpk7qE3tGl5uiitFE6/9w+1OolKiXO5eFk2IpM\
    +PzNGSK2zBEAkQWKaYgtUCiCqsnDZHtbl1kgu2c5WQgTUUIw1G3tz8u2f2+NXzFUUUJA6BLXA1cnGzt3HzogBzSUCQVEOV7Mcd9TY1BzDmSwrAAAA') format('woff2');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}
pre {
    font-family: caps, 'Times New Roman', serif;
    letter-spacing: 0.15rem;
    font-size: 1em;
}
pre b {
    display: inline-block;
    transform: translateY(-.1rem);
    font-size: 1.1em;
    font-family: punct, 'Times New Roman', serif;
    font-weight: normal;
}
.btn {
    display: inline-block; /* enables padding */
    padding: 1.9rem 1.5rem 1.7rem;
    margin-bottom: 0.5rem;
    line-height: 0;
    white-space: nowrap;
    border-radius: 0.4rem;
    background: #236;
}
```

```html
<pre>
<a href="#">LOOP<b style="margin-left:-.2rem">.</b>COOP</a><b> / </b><a href="#">CLUMP</a><b> / </b>LOCATIONS

<a class="btn"><b>=</b> LIST</a> <a class="btn"><b>?</b> HELP</a> <a class="btn"><b>^</b> USER</a> <a class="btn"><b>*</b> LIKE</a>

PROFILE<b> / </b>SETTINGS<b> / </b>COLLECTIONS

TUNE<b> / </b>PATCH<b> / </b>SEQ<b> / </b>SYNTH<b> / </b><a class="btn"><b>&lt;</b> PREV</a> <a class="btn"><b>&gt;</b> NEXT</a>

<b>&lt; ^ &gt; ? * = + - . /</b>

<a class="btn"><b>=</b></a> <a class="btn"><b>?</b></a> <a class="btn"><b>^</b></a> <a class="btn"><b>*</b></a>
</pre>
```

## The final choice

Some requirements, roughly in order of importance:

1. Must be open source - this probably rules out the ones from myfonts
2. Prefer a smaller filesize, between 3500 and 5000 is fine
3. Must look good when used as a heading or on a button - and fairly narrow
4. Some personality, without becoming overbearing
5. Must have pretty ASCII punctuation, for buttons and icons
6. Just a single font - don't try to combine two, like Binchotan Sharp and Agave
7. "LOOP.COOP" should render similarly to the old logo (capsule-shaped "O"s)
8. "CLUMP" should look a bit like a logo
9. Latin 1 may be useful for i18n
10. Geometric shapes and box drawing might come in handy
11. Different weights, italic and cond, and maybe related serif and sans
12. Circular dots are better than squares, to visually echo the clumps

### Fonts which immediately fail these requirements

- __~~Conta:~~__ Pixellated is not right, and non-pixellated is too big and plain
- __~~Manolo:~~__ Constructivist is wrong, and it's missing punctuation
- __~~Wordclock Stencil:~~__ Too much going on, and also strikes the wrong tone
- __~~Awoof:~~__ 'Military / sports' is all wrong
- __~~Modern Society:~~__ Too bold I think, and it's missing some punctuation
- __~~Ki Light:~~__ myfonts, so not open source, and maybe a bit too whacky

### Fonts with larger filesizes which may support another font

- __M PLUS Code (2024):__ Supports __M-1m,__ has CJK and the interesting "C" hook
- __Yuki Code:__ Supports __Nova Mono,__ has CJK

The remaining eight fonts are between 3225 and 4985 bytes, when subsetted to:  
`ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 <^>?*=+-./`

Their test-pages are lined up for comparison on
[font-shortlist.png,](./font-shortlist/font-shortlist.png) along with some green
dots representing nice-looking elements.

- __Binchotan Sharp:__ Gets almost no green dots - too wonky looking
- __M-1m:__ Square dot, but renders a good "LOOP.COOP" and wide-"M" "CLUMP"
- __Share Tech:__ The best "LOOP.COOP", but let down by sq. dot and fussy "M N"
- __Agave:__ Square dot and a bad "LOOP.COOP", but good "CLUMP" and great "S"
- __Nova Mono:__ "TUNE" and "SYNTH" look great, but not much else
- __NF Code:__ A bit 'closed' looking. I think
- __MoonGloss:__ A good "LOOP.COOP" and "CLUMP", but not much else
- __PT Mono:__ "CLUMP" is quite nice, but the serifs don't work anywhere else

### Final Decision

I had a good feeling about Agave, but it was actually the very first font that
I looked at. So I didn't really want to choose it, because then the rest of the
research would be a bit of a waste of time!

But it's not right for the "LOOP.COOP" text, so I think that should be in the
runner up, which is Share Tech.

1. ✅ Agave and Share Tech are both open source
2. ✅ Third and fourth smallest filesizes that I've awarded two "*"s
3. ✅ Agave looks good when used as a heading or on a button - and fairly narrow
4. ✅ Agave has a little personality - very 'open' "C" and "S"
5. ✅ Agave has the most pretty ASCII punctuation, I think
6. ❌ Combining two fonts, but just for "LOOP.COOP", which should look distinct
7. ✅ Share Tech renders "LOOP.COOP" like the old logo (capsule-shaped "O"s)
8. ✅ Agave renders "CLUMP" very nicely, so looks a bit like a logo
9. ✅ Agave has properly designed Latin 1
10. ✅ Agave has lots of geometric shapes, box drawing, etc
11. ❌ Hardly any variants
12. ❌ Neither has circular dots

