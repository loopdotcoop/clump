# How to: Create a project like this

Step-by-step instructions for making a similar project, from scratch.

## Set up the Git repo and GitLab Pages

### Create a new GitLab project

1. Visit <https://gitlab.com/dashboard/projects>
2. Click 'New project' > 'Create from template'
3. Scroll to 'Pages/Plain HTML' and click the 'Use template' button next to it
4. Project name: clump
5. Project description: A synth and sequencer based on soft body physics
6. Select 'Public'
7. Click 'Create project'

### Add yourself as a 'Maintainer'

1. Visit <https://gitlab.com/{ORGANIZATION}/clump>
2. In the sidebar, 'Manage' > 'Members'
3. Click 'Invite members'
4. Under 'Username, name or email address', search for you GitLab username
5. Select a role: Maintainer
6. Leave 'Access expiration date' blank
7. Click 'Invite'

It seems that in GitLab (unlike GitHub) invited members automatically accept an
invitation to maintain a project.

### Clone the project to you local machine

1. Visit <https://gitlab.com/{ORGANIZATION}/clump>
2. Click the 'Code' button
3. Click the 'Copy URL' icon next to 'Clone with HTTPS'
4. In you terminal, `cd` to wherever you keep your repos
5. `git clone https://{USERNAME}@gitlab.com/{ORGANIZATION}/clump.git`
6. `cd clump`

### Check that `git push` and GitLab Pages are working

Make some minor change to a file, eg change README.md to:

```md
# clump

> A synth and sequencer based on soft body physics

- 0.0.1
- January 2024
- <https://gitlab.com/{ORGANIZATION}/clump/>
- <https://{ORGANIZATION}.gitlab.io/clump/>
```

Commit and push the change:

```sh
git add .
git status
git commit -am 'Adds initial README.md'
git push
```

Currently <https://gitlab.com/{ORGANIZATION}/clump/-/pipelines> should be empty,
because GitLab did not run .gitlab-ci.yml when the project was created.

Assuming your local Git username is the maintainer, and you have an up-to-date
GitLab PAT token set up, then you should be asked for a password.

Now <https://gitlab.com/{ORGANIZATION}/clump/-/pipelines> should show a pipeline
with 'Passed' status. Visiting <https://{ORGANIZATION}.gitlab.io/clump/> should
show GitLab's default placeholder webpage for signed-in users only.

### Make the webpage available to anyone

1. Visit <https://gitlab.com/{ORGANIZATION}/clump>
2. In the sidebar, 'Settings' > 'General'
3. Scroll to 'Visibility, project features, permissions' and click 'Expand'
4. Scroll to 'Pages' and change 'Only Project Members' to 'Everyone With Access'
5. Scroll down and click 'Save changes'
6. Visit <https://gitlab.com/{ORGANIZATION}/clump/pages>
7. Tick the 'Force HTTPS (requires valid certificates)' box
8. Untick the 'Use unique domain' box
9. Click 'Save changes'

You may need to wait 10 minutes for GitLab's cache to update. Then, in a browser
not signed in to GitLab, check that the default placeholder webpage is visible.

## Add a custom build process

The `script` section of '.gitlab-ci.yml' can be used to invoke a Node.js build
script.

### Add package.json

At this stage, it's mostly just for the `"type": "module",` line:

```json
{
  "name": "clump",
  "version": "0.0.1",
  "description": "A synth and sequencer based on soft body physics",
  "type": "module",
  "scripts": {
    "build": "node scripts/build.js"
  },
  "keywords": [],
  "author": "Loop.Coop",
  "license": "MIT"
}
```

### Check that building with Node.js works, in principal

In '.gitlab-ci.yml', change:

```yml
image: busybox
...
  script:
    - echo "The site will be deployed to $CI_PAGES_URL"
```

to:

```yml
image: "node:lts-alpine"
...
  script:
    - node scripts/build.js
```

Create 'scripts/build.js', which will just create a plain text file in
'public' called 'test.txt'.

```js
// scripts/build.js

import { writeFileSync } from 'node:fs';

writeFileSync('public/test.txt', 'A test file');
```

Commit and push the change:

```sh
git add .
git status
git commit -am 'Tries invoking scripts/build.js from .gitlab-ci.yml'
git push
```

Now <https://gitlab.com/{ORGANIZATION}/clump/-/pipelines> should show a new
pipeline. Clicking its link should show something like:

```log
Running with gitlab-runner 16.6.0~beta.105.gd2263193 (d2263193)
...
Preparing the "docker+machine" executor
00:07
Using Docker executor with image node:lts-alpine ...
Pulling docker image node:lts-alpine ...
...
Preparing environment
00:02
...
Getting source from Git repository
00:01
Fetching changes with git depth set to 20...
...
Created fresh repository.
Checking out 2e61cfdf as detached HEAD (ref is master)...
Skipping Git submodules setup
$ git remote set-url origin "${CI_REPOSITORY_URL}"
Executing "step_script" stage of the job script
00:00
...
$ node scripts/build.js
Uploading artifacts for successful job
00:02
Uploading artifacts...
public: found 4 matching artifact files and directories 
...
Cleaning up project directory and file based variables
00:01
Job succeeded
```

Visit <https://{ORGANIZATION}.gitlab.io/clump/test.txt>

If the Node.js script was able to add a file to the 'public/' folder, then you
should see:

```txt
A test file
```

### Create a fully-fledged, custom build process

This can live in 'scripts/build/', and be as simple or as complex as you like.

Typically it will combine template files with content to generate the finished
web app. It should be runnable locally using `node scripts/build.js`, which will
allow local testing.
