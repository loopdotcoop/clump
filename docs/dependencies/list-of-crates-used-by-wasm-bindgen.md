## List of crates used by wasm-bindgen

One of clump's goals is to reduce 3rd party dependencies to an absolute minimum.

wasm-bindgen (used for compiling Rust apps into WebAssembly) is one of the very
few dependencies that clump does require. It's a dev-dependency, not a runtime
dependency. It has 12 direct and indirect runtime dependencies, which become
clump dev-dependencies, so there is a case to be made for using them in clump's
runtime Rust too.

```
wasm-bindgen v0.2.91
  cfg-if v1.0.0
  wasm-bindgen-macro v0.2.91
    quote v1.0.35
      proc-macro2 v1.0.78
        unicode-ident v1.0.12
    wasm-bindgen-macro-support v0.2.91
      proc-macro2 v1.0.78
        unicode-ident v1.0.12
      syn v2.0.50
      wasm-bindgen-shared v0.2.91
        bumpalo v3.15.2
        log v0.4.20
        once_cell v1.19.0
      wasm-bindgen-backend v0.2.91
```


### wasm-bindgen's 2 direct runtime dependencies

> Easy support for interacting between JS and Rust.

<https://crates.io/crates/wasm-bindgen/0.2.91>

Currently we are using version v0.2.91 of wasm-bindgen, which has:
- 2 direct runtime dependencies (cfg-if and wasm-bindgen-macro)
- 2 'OPTIONAL' direct dependencies (serde and serde_json), but these do not seem
  to be downloaded during compilation
- 6 direct dev-dependencies, but clump's `npm run build` script never needs to
  use those

#### cfg-if v1.0.0

> A macro to ergonomically define an item depending on a large number of `#[cfg]`
> parameters. Structured like an if-else chain, the first matching branch is the
> item that gets emitted.

<https://crates.io/crates/cfg-if/1.0.0>

- No runtime dependencies
- 2 'OPTIONAL' direct deps (compiler_builtins and rustc-std-workspace-core), but
  these do not seem to be downloaded during compilation
- No dev-dependencies

#### wasm-bindgen-macro v0.2.91

> Definition of the `#[wasm_bindgen]` attribute, an internal dependency

<https://crates.io/crates/wasm-bindgen-macro/0.2.91>

- 2 direct runtime dependencies (quote and wasm-bindgen-macro-support)
- No 'OPTIONAL' dependencies
- 4 direct dev-dependencies

### wasm-bindgen-macro's 2 direct runtime dependencies

#### quote v1.0.35

> Quasi-quoting macro quote!(...)

<https://crates.io/crates/quote/1.0.35>

- 1 direct runtime dependency (proc-macro2)
- No 'OPTIONAL' dependencies
- 2 direct dev-dependencies

#### wasm-bindgen-macro-support v0.2.91

> The part of the implementation of the `#[wasm_bindgen]` attribute that is not
> in the shared backend crate

<https://crates.io/crates/wasm-bindgen-macro-support/0.2.91>

- 5 direct runtime dependencies (proc-macro2, quote, syn, wasm-bindgen-backend,
  wasm-bindgen-shared)
- No 'OPTIONAL' dependencies
- No dev-dependencies

### quote and wasm-bindgen-macro-support both use proc-macro2

#### proc-macro2 v1.0.78

> A substitute implementation of the compiler's `proc_macro` API to decouple
> token-based libraries from the procedural macro use case.

<https://crates.io/crates/proc-macro2/1.0.78>

- 1 direct runtime dependency (unicode-ident)
- No 'OPTIONAL' dependencies
- 5 direct dev-dependencies

#### unicode-ident v1.0.12

> Determine whether characters have the XID_Start or XID_Continue properties
> according to Unicode Standard Annex #31.

<https://crates.io/crates/unicode-ident/1.0.12>

- No dependencies
- No 'OPTIONAL' dependencies
- 6 direct dev-dependencies

### wasm-bindgen-macro-support has 3 direct runtime deps that only it uses

### syn v2.0.50

> Parser for Rust source code

<https://crates.io/crates/syn/2.0.50>

- 2 direct runtime dependencies (proc-macro2 and unicode-ident) which can both
  be found higher up the dependency tree
- 1 'OPTIONAL' direct dependency (quote) which can also be found higher up the
  dependency tree
- 12 direct dev-dependencies

### wasm-bindgen-backend v0.2.91

> Backend code generation of the wasm-bindgen tool

<https://crates.io/crates/wasm-bindgen-backend/0.2.91>

- 7 direct runtime dependencies - 3 of which (bumpalo, log and once_cell) are
  only used by wasm-bindgen-backend, and 4 of which can be found elsewhare in
  the dependency tree
- No 'OPTIONAL' dependencies
- No dev-dependencies

### wasm-bindgen-shared v0.2.91

> Shared support between wasm-bindgen and wasm-bindgen cli, an internal
> dependency.

<https://crates.io/crates/wasm-bindgen-shared/0.2.91>

- No dependencies
- No 'OPTIONAL' dependencies
- No dev-dependencies

### wasm-bindgen-backend has 3 direct runtime deps that only it uses

#### bumpalo v3.15.2

> A fast bump allocation arena for Rust.

<https://crates.io/crates/bumpalo/3.15.2>

- No dependencies
- 1 'OPTIONAL' direct dependency (allocator-api2), which does not seem to be
  downloaded during compilation
- No dev-dependencies

#### log v0.4.20

> A lightweight logging facade for Rust

<https://crates.io/crates/log/0.4.20>

- No dependencies
- 4 'OPTIONAL' direct deps (serde, sval, sval_ref and value-bag), which do not
  seem to be downloaded during compilation
- No dev-dependencies

#### once_cell v1.19.0

> Single assignment cells and lazy values.

<https://crates.io/crates/once_cell/1.19.0>

- No dependencies
- 3 'OPTIONAL' direct deps (critical-section, parking_lot_core, portable-atomic)
  which do not seem to be downloaded during compilation
- 2 dev-dependencies, including critical-section which is an 'OPTIONAL' dep
