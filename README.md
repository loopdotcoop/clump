# C L U M P

> A synth and sequencer based on soft body physics

- 0.0.1
- January - March 2024
- <https://gitlab.com/loopdotcoop/clump/>
- <https://loopdotcoop.gitlab.io/clump/index.html>

## Dependencies and installation

Once you have [installed Node, NPM, rustup and wasm-pack,](#set-up-your-machine)  
__C L U M P__ has:
- No JavaScript dependencies (in other words, no [NPM packages](
  https://www.npmjs.com/))
- Some Rust dependencies, which will be fetched the first time that the [Rust
  apps and libraries are built](#build-all-the-rust-apps-and-or-libraries)

## Handy commands

```sh
npm update:rs # keep Rust's dependencies up-to-date TODO
npm run dev # start a development server at <http://localhost:4321>
npm run build # build all Rust apps and libraries, and all HTML pages
npm test # run all Rust and pages tests
npm run preflight # check for code mistakes - should be run before deployment
```

## Set up your machine

### Set up Linux and macOS

1. Open the Terminal:  
   Most Linux let you open the Terminal by pressing control-alt-t.  
   On macOS, from the Finder, command-shift-u, type 'term' to select Terminal,
   command-o to open it.
2. Check that Node.js is installed:  
   `node --version`  
   If you see `command not found`  
   instead of installing Node directly,
   [install `nvm`](https://github.com/nvm-sh/nvm#installing-and-updating) and:  
   `nvm install --lts`  
   After that, `node --version` should show the current 'long term support'
   version of Node
3. Check that NPM is installed:  
   `npm --version`  
   This should have been installed at the same time as Node
4. Check that the Rust compiler and associated utilities are installed:  
   `rustup --version`  
   If you see `command not found`  
   [install `rustup`,](https://www.rust-lang.org/tools/install) and choose:  
   `1) Proceed with installation (default)`
5. Check that the `wasm-pack` is installed:  
   `wasm-pack --version`  
   If you see `command not found`  
   [install `wasm-pack`](https://rustwasm.github.io/wasm-pack/installer/)
6. On Linux, check that `cc` is installed (this step is not needed for macOS):  
   `cc --version`  
   If you see `Command 'cc' not found`:  
   `sudo apt-get update # enter your admin password, wait for 'Done'`  
   `sudo apt install gcc # then press Return at the prompt`  

### Set up Windows

If you would prefer to use 'Windows Subsystem for Linux' (wsl), follow the
instructions in the ['Set up Linux and macOS' section](
#set-up-linux-and-macos) above. Otherwise, you can use PowerShell:

1. Open Windows PowerShell:  
   Click the 'Start' icon (usually in the bottom left corner of the screen),
   type 'powershell', and click the 'Windows PowerShell' app
2. Check that Node.js is installed:  
   `node --version`  
   If you see `The term 'node' is not recognized ...`  
   instead of installing Node directly, [install `nvm-windows`
   ](https://github.com/coreybutler/nvm-windows#installation--upgrades) and:  
   `nvm install lts`  
   `nvm use lts`  
   After that, `node --version` should show the current 'long term support'
   version of Node
3. Check that NPM is installed:  
   `npm --version`  
   This should have been installed at the same time as Node
4. Check that the Rust compiler and associated utilities are installed:  
   `rustup --version`  
   If you see `The term 'rustup' is not recognized ...`  
   [install `rustup`,](https://www.rust-lang.org/tools/install) and (possibly
   after installing Visual Studio for the C++ linker and libraries) choose:  
   `1) Proceed with installation (default)`
5. Check that the `wasm-pack` is installed:  
   `wasm-pack --version`  
   If you see `The term 'wasm-pack' is not recognized ...`  
   [install `wasm-pack`](https://rustwasm.github.io/wasm-pack/installer/)

### Set up your code editor

#### VS Code

Install these two helpful extensions for developing Rust apps:

1. [rust-analyzer v0.3.1705
   ](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)
2. [Even Better TOML v0.19.2
   ](https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml)

TODO more code editors

## Develop

TODO write this section

## Build, test and dev

### Build all the pages

To build everything in the src/pages/ folder:

```sh
npm run build:pages
# > clump@0.0.1 build:pages
# > node scripts/build-pages.js
#  OK  All pages were successfully built
```

### Build all the Rust apps

The first time you build Rust apps, `rustup` will fetch all dependencies. After
that, `rustup` will use its locally cached dependencies.

To build everything in the src/rs-apps/ folder:

```sh
npm run build:rs
# > clump@0.0.1 build:rs
# > node scripts/build-rs.js
# ------------------------------------------------------------------------------
# Running buildCliBinary('example_anim_repl')...
# ...
#  OK  All three 'example_anim_repl' builds succeeded
# ==============================================================================
#  Done!  12 builds for 3 Rust apps and libraries
# ==============================================================================
```

It is also possible to build just a single Rust app. See the README.md file in
the Rust app you want to build for details.

### Run all the pages e2e tests

To run all the pages end-to-end tests:

```sh
npm run test:e2e:pages
# > clump@0.0.1 test:e2e:pages
# > node scripts/run-pages-e2e-tests.js
# ------------------------------------------------------------------------------
# ...
# ==============================================================================
# ...
```

### Run all the pages unit and integration tests

To run all the pages unit and integration tests:

```sh
npm run test:internal:pages
# > clump@0.0.1 test:internal:pages
# > node scripts/run-pages-internal-tests.js
# ------------------------------------------------------------------------------
# ...
# ==============================================================================
# ...
```

### Run the end-to-end tests for all Rust apps

The Rust app and library unit tests and integration tests are written in Rust,
but these e2e tests are all written in JavaScript:

```sh
npm run test:e2e:rs
# > clump@0.0.1 test:e2e:rs
# > node scripts/run-rs-e2e-tests.js
# ------------------------------------------------------------------------------
# ...
# ==============================================================================
# ...
```

It is also possible to run the end-to-end tests of just a single Rust app.  
See the README.md file in the Rust app you want to test for details.

### Run unit and integration tests for all Rust apps and libraries

The Rust app and library end-to-end tests are written in JavaScript, but these
'internal' tests are all written in Rust:

```sh
npm run test:internal:rs
# > clump@0.0.1 test:internal:rs
# > node scripts/run-rs-internal-tests.js
# ------------------------------------------------------------------------------
# ...
# ==============================================================================
# ...
```

It is also possible to run the unit tests and integration tests of just a single
Rust app and library. See the README.md file in the crate you want to test for
details.

## Deploy

TODO write this section
