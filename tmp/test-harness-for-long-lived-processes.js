import { spawn } from 'node:child_process';

const path = './dist/example_anim_repl/cli-binary/example_anim_repl';

// Set up a test harness for long-lived processes.
const longLivedTestHarness = (raw_args, actions={}) => {
    return new Promise((resolve, reject) => {
        const proc = spawn(path, raw_args.split(' ')); // TODO fix quoted strings
        proc.stdin.setEncoding('utf-8');

        const stdOuts = [];
        const stdErrs = [];

        proc.stdout.on('data', data => {
            stdOuts.push(data.toString());
            const numStdOuts = stdOuts.length;
            const action = actions[numStdOuts];
            if (action === 'CLOSE') {
                proc.kill('SIGINT')
            } else if (typeof action !== 'undefined') {
                proc.stdin.write(action);
            }
        });

        proc.stderr.on('data', data => {
            stdErrs.push(data.toString());
            proc.kill('SIGINT'); // same as ctrl-c
        });

        proc.on('close', (exitCode, exitSignal) =>
            exitCode === 0 || exitCode === null
                ? resolve({ exitCode, exitSignal, stdErrs, stdOuts })
                : reject({ exitCode, exitSignal, stdErrs, stdOuts })
        );
    });
}

try {
    console.log('Short-lived (just prints version and exits):\n   ',
        await longLivedTestHarness('--version'));
    console.log('Long-lived (well, a few ms at least):\n   ',
        await longLivedTestHarness('-c 8 -f ^ -r 3', {
            3: 'Hullo lil app!\n',
            5: 'CLOSE',
        }));
    // TODO long-lived and exits gracefully without error, so exitCode is 0
    // TODO long-lived and exits gracefully with error, so exitCode is > 0
    // TODO long-lived and exits due to a `proc.stdin.write()` ctrl-c keypress
    // TODO short-lived and sends errors to stderr
    // TODO long-lived and sends errors to stderr
} catch (err) { console.log(err) }
