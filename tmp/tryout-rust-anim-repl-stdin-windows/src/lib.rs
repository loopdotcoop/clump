// From <https://github.com/crossterm-rs/crossterm-winapi/blob/master/src/lib.rs

pub use self::{
    console_mode::ConsoleMode,
    handle::Handle,
};

mod console_mode;
mod handle;
