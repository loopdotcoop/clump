// Standard Rust.
use std::io::{Read,stdin,Write};
use std::str;
use std::sync::mpsc::{channel,Receiver};
use std::thread::{sleep,spawn};
use std::time::Duration;

// Local.
mod console_mode; use console_mode::ConsoleMode;
mod handle; use handle::Handle;

// ### `spawn_stdin_channel()`
// Reads standard-input on a different thread from the main render loop, which
// prevents `read_exact()` from blocking the animation.
// See <https://stackoverflow.com/a/55201400>
fn spawn_stdin_channel() -> Receiver<[u8; 1]> {
    let (tx, rx) = channel::<[u8; 1]>();
    spawn(move || loop {
        let mut buffer = [0];
        stdin().read_exact(&mut buffer).unwrap();
        tx.send(buffer).unwrap();
    });
    rx
}


#[cfg(target_family = "windows")]
fn main() {
    println!("Hello, Windows!");
    println!("Press q to quit");
    let duration = Duration::from_millis(50); // 20fps

    // Put the terminal in 'raw mode', because this app handles all keypresses.
    let was_already_in_raw_mode = is_raw_mode_enabled().unwrap();
    println!("was_already_in_raw_mode: {:?}", was_already_in_raw_mode);
    if ! was_already_in_raw_mode { enable_raw_mode().unwrap() }

    // Start reading standard-input.
    let stdin_channel = spawn_stdin_channel();

    let mut full_text = String::new();

    loop {
        sleep(duration);

        // Get the stdin characters entered since the last 'tick'.
        let mut recent_bytes: Vec<u8> = vec![];
        while let Ok(received) = stdin_channel.try_recv() {
            let byte = received[0];
            recent_bytes.push(if byte < 32 || byte > 126 { 126 } else { byte } );
        }
        let recent_text = match str::from_utf8(&recent_bytes) {
            Ok(val) => val,
            Err(err) => panic!("Invalid UTF-8 sequence: {}", err),
        };

        // Append those characters to `full_text`.
        full_text = format!("{}{}", full_text, recent_text);

        // Delete the current line and re-print it.
        if full_text.len() > 12 {
            print!("\x1b[2K\r{}", &full_text[..12])
        } else {
            print!("\x1b[2K\r{}{}", full_text, str::repeat(".", 12 - full_text.len()))
        }
        std::io::stdout().flush().unwrap();

        // If the user just entered lowercase "q" this tick, exit.
        if recent_text == "q" { break }
    }

    // Return the terminal to its original mode.
    if ! was_already_in_raw_mode { disable_raw_mode().unwrap() }
}

#[cfg(not(target_family = "windows"))]
fn main() {
    println!("Hello, Not Windows!");
}


// From <https://docs.rs/crate/crossterm/latest/source/src/terminal/sys/windows.rs>

use winapi::{
    shared::minwindef::DWORD,
    um::wincon::{ENABLE_ECHO_INPUT, ENABLE_LINE_INPUT, ENABLE_PROCESSED_INPUT},
};

/// bits which can't be set in raw mode
const NOT_RAW_MODE_MASK: DWORD = ENABLE_LINE_INPUT | ENABLE_ECHO_INPUT | ENABLE_PROCESSED_INPUT;

fn is_raw_mode_enabled() -> std::io::Result<bool> {
    let console_mode = ConsoleMode::from(Handle::current_in_handle()?);

    let dw_mode = console_mode.mode()?;

    Ok(
        // check none of the "not raw" bits is set
        dw_mode & NOT_RAW_MODE_MASK == 0,
    )
}

fn enable_raw_mode() -> std::io::Result<()> {
    let console_mode = ConsoleMode::from(Handle::current_in_handle()?);

    let dw_mode = console_mode.mode()?;

    let new_mode = dw_mode & !NOT_RAW_MODE_MASK;

    console_mode.set_mode(new_mode)?;

    Ok(())
}

#[allow(dead_code)]
fn disable_raw_mode() -> std::io::Result<()> {
    let console_mode = ConsoleMode::from(Handle::current_in_handle()?);

    let dw_mode = console_mode.mode()?;

    let new_mode = dw_mode | NOT_RAW_MODE_MASK;

    console_mode.set_mode(new_mode)?;

    Ok(())
}
