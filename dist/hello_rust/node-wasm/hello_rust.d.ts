/* tslint:disable */
/* eslint-disable */
/**
* ### `hello_rust()`
* An example function, which exposes the app() to .wasm users.
* @param {string} raw_args
* @returns {string}
*/
export function hello_rust(raw_args: string): string;
