/* tslint:disable */
/* eslint-disable */
/**
*/
export class ExampleAnimRepl {
  free(): void;
/**
* @param {string} raw_args
* @returns {ExampleAnimRepl}
*/
  static new(raw_args: string): ExampleAnimRepl;
/**
* @param {number} delta_ms
* @param {string} recent_text
* @returns {string}
*/
  tick(delta_ms: number, recent_text: string): string;
/**
* @returns {string}
*/
  render(): string;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_exampleanimrepl_free: (a: number) => void;
  readonly exampleanimrepl_new: (a: number, b: number) => number;
  readonly exampleanimrepl_tick: (a: number, b: number, c: number, d: number, e: number) => void;
  readonly exampleanimrepl_render: (a: number, b: number) => void;
  readonly __wbindgen_malloc: (a: number, b: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number, d: number) => number;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number, c: number) => void;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {SyncInitInput} module
*
* @returns {InitOutput}
*/
export function initSync(module: SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function __wbg_init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
