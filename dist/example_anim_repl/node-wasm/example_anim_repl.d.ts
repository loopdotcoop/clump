/* tslint:disable */
/* eslint-disable */
/**
*/
export class ExampleAnimRepl {
  free(): void;
/**
* @param {string} raw_args
* @returns {ExampleAnimRepl}
*/
  static new(raw_args: string): ExampleAnimRepl;
/**
* @param {number} delta_ms
* @param {string} recent_text
* @returns {string}
*/
  tick(delta_ms: number, recent_text: string): string;
/**
* @returns {string}
*/
  render(): string;
}
