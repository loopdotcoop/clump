// scripts/dev/get-ext.js

/** ### Returns the extension from a filename, path or url
 *
 * Also, converts it to lowercase. Returns undefined if no extension is present.
 *
 * @param   {string}  url
 *     A filename, path or url which may end in an extension
 * @return  {string|void}
 *     Returns the extension, or undefined if no extension is present
 */
export function getExt(url) {
    const pf = 'getExt(): ';

    // Validate the argument.
    if (typeof url !== 'string') throw TypeError(
        `${pf}url is type '${typeof url}' not 'string'`);

    // Remove the query string, if there is one.
    const queryPos = url.lastIndexOf('?');
    if (queryPos !== -1) url = url.slice(0, queryPos);

    const exts = url.split('/').pop().split('.');
    if (exts.length === 1) return void 0;
    return exts.pop().toLowerCase();
}


/* ---------------------------------- TEST ---------------------------------- */

export const testGetExt = (equal, throws) => {
    const pf = 'getExt(): ';
    const te = { name: 'TypeError' };

    // Argument is incorrect type.
    throws(()=>getExt(),
        { ...te, message:pf+"url is type 'undefined' not 'string'" });
    throws(()=>getExt(null),
        { ...te, message:pf+"url is type 'object' not 'string'" });

    // Basic, no extension.
    equal(getExt(''), // empty string
            void 0);
    equal(getExt('a'), // filename
            void 0);
    equal(getExt('a/b'), // path
            void 0);
    equal(getExt('a/b/'), // path
            void 0);
    equal(getExt('https://-.com/'), // url
            void 0);
    equal(getExt('https://-.com/a/b'), // url
            void 0);
    equal(getExt('https://-.com/a/b/'), // url
            void 0);

    // Basic, empty string extension.
    equal(getExt('.'),
            '');
    equal(getExt('a.'), // filename
            '');
    equal(getExt('a/b.'), // path
            '');
    equal(getExt('a/b/.'), // path
            '');
    equal(getExt('https://-.com/.'), // url
            '');
    equal(getExt('https://-.com/a/b.'), // url
            '');
    equal(getExt('https://-.com/a/b/.'), // url
            '');

    // Basic, normal extension.
    equal(getExt('.txt'),
            'txt');
    equal(getExt('a.txt'), // filename
            'txt');
    equal(getExt('a/b.TXT'), // path, UPPERCASE
            'txt');
    equal(getExt('a/b.txt.zip'), // path, zipped
            'zip');
    equal(getExt('a/b/.txt'), // path
            'txt');
    equal(getExt('https://-.com/.Txt'), // url, Title Case
            'txt');
    equal(getExt('https://-.com/a/b.txt'), // url
            'txt');
    equal(getExt('https://-.com/a/b/.tXt'), // url, mIxEdCaSe
            'txt');

    // Query string.
    equal(getExt('?'),
            void 0);
    equal(getExt('?a'),
            void 0);
    equal(getExt('?.a'),
            void 0);
    equal(getExt('?a.'),
            void 0);
    equal(getExt('?a.txt'),
            void 0);
    equal(getExt('.?'),
            '');
    equal(getExt('.A?b'),
            'a');
    equal(getExt('.A?b.c'),
            'a');
    equal(getExt('a/b/c.txt?d.e'),
            'txt');
    equal(getExt('https://-.com/a/b?c.jpg'),
            void 0);
    equal(getExt('https://-.com/a/b.txt?c.jpg'),
            'txt');

    // From https://www.netmeister.org/blog/urls.html
    equal(getExt('https://jschauma:hunter2@www.netmeister.org:443/blog/urls.html?q=s&q2=a+b;q3=sp%0Ace#top'),
            'html');
}
