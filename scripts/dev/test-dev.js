// scripts/dev/test-dev.js

import { equal, throws } from 'node:assert';
import { green, red } from '../utilities/ansi.js';

import { testGetExt } from './get-ext.js';

const showFail = (err, fnName) => {
    const place = err.stack?.split('\n') // split into lines
        .find(line => /^\s+at /.test(line)) // the first line starting "    at "
        .split('/').pop() // text after the last "/", eg "some-file.js:344:5)"
        || 'unknown location)'; // fallback
    console.error(`${red('FAIL')}${err.message}\n(${place}\n`);
    err.where = fnName;
    throw err;
}

export const testDev = () => {
    try { testGetExt(equal, throws) } catch (err) {
        showFail(err, 'testGetExt()') }
    console.log(`${green('PASS')}scripts/dev/ test suite passed`);
};

const filename = import.meta.url.split('/').slice(-1)[0]; // 'test-dev.js'
const isDirectCall = process.argv[1].slice(-filename.length) === filename;
if (isDirectCall) { process.exit(testDev()) }
