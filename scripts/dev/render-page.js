// scripts/dev/render-page.js

import { renderSidebarPage } from '../build-pages/render-sidebar-page.js';

/** ### Xx
 *
 * Xx.
 *
 * @param   {[type]}  content       [content description]
 * @param   {[type]}  i18n          [i18n description]
 * @param   {[type]}  languageCode  [languageCode description]
 * @param   {string}  template
 *     HTML with slots in "{{ ... }}" format
 * @param   {[type]}  templateName  [templateName description]
 * @param   {[type]}  version       [version description]
 *
 * @return  {string}
 *     Returns HTML where the "{{ ... }}" slots have been replaced by content
 */
export function renderPage(content, i18n, languageCode, template, templateName, version) {
    const pf = 'renderPage(): ';

    // Validate the arguments. TODO all arguments
    if (! i18n[version]) throw RangeError(
        `${pf}No i18n.${version}`);
    if (! i18n[version][languageCode]) throw RangeError(
        `${pf}No i18n.${version}.${languageCode}`);
    if (! content[`${version}/${templateName}-${languageCode}.md`]) throw RangeError(
        `${pf}No content['${version}/${templateName}-${languageCode}.md']`);

    // Replace "{{ content.some_identifier}}" with content.
    const { frontmatter, html } = content[`${version}/${templateName}-${languageCode}.md`];
    const renderedPage = renderSidebarPage(pf, frontmatter, html, languageCode, template);

    // Replace "{{t.some_identifier}}" with translated text.
    return insertTranslations(pf, renderedPage, i18n[version][languageCode], templateName);
}


/* --------------------------------- PRIVATE -------------------------------- */

const insertTranslations = (pf, template, t, templateName) => {
    return template
        .split('{{t.')
        .map((part, i) => {
            if (i === 0) return part; // before the first "{{t."
            const identifierEndPos = part.indexOf('}}');
            if (identifierEndPos === -1) throw RangeError(
                `${pf}Cannot find end of "{{t." [${i}] for:\n    ${templateName}`);
            const identifier = part.slice(0, identifierEndPos);
            const replacement = t[identifier];
            if (! replacement) throw RangeError(
                `${pf}Cannot find replacement for '${identifier}' [${i}] for:\n    ${templateName}`);
            return replacement + part.slice(identifierEndPos + 2);
        })
        .join('');
}


/* ---------------------------------- TEST ---------------------------------- */

export const testRenderPage = (equal, throws) => {
    const pf = 'renderPage(): ';
    const te = { name: 'TypeError' };

    // TODO
}
