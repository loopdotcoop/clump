// scripts/build-rs/build-node-wasm.js

import { spawnSync } from 'node:child_process';
import { renameSync, rmSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { green, red } from '../utilities/ansi.js';

export const buildNodeWasm = async rsName => {
    const pf = 'buildNodeWasm(): ';

    // Validate the Rust app's name.
    if (typeof rsName !== 'string') throw TypeError(
        `${pf}rsName is type '${typeof rsName}' not 'string'`);
    if (! /^[a-z][_0-9a-z]{0,23}$/.test(rsName)) throw TypeError(
        `${pf}rsName '${rsName}' fails /^[a-z][_0-9a-z]{0,23}$/`);

    const outDir = join('dist', rsName, 'node-wasm');
    const outDirArg = join('..', '..', '..', outDir);

    try {
        // Delete any files generated during a previous build.
        '.gitignore|%_bg.wasm|%.cjs|%_bg.wasm.d.ts|%.d.ts'
            .replace(/%/g, rsName).split('|')
            .forEach(filename => rmSync(
                join(outDir, filename),
                { force: true }, // don't stop if the file doesn't exist
            )
        );

        // Generate a WebAssembly app targeting Node.js.
        const result = spawnSync(
            'wasm-pack', // TODO describe
            [
                'build', // generate the .js, .ts and .wasm files
                '--target', 'nodejs', // set the target environment
                '--no-pack', // do not generate a 'package.json' file
                '--out-dir', outDirArg, // output dir, relative to the crate
                '--out-name', rsName, // basis for naming the generated files
            ],
            { cwd: 'src/rs-apps/' + rsName }, // path to the Rust crate
        );

        if (result.status) throw Error(pf + result.stderr);
        console.log(
            '    ' +
            result.output[2]
                .toString()
                .trim()
                .split('\n')
                .reduce((lines, line) =>
                    line.slice(0, 8) === '[INFO]: ' && line[9] > '~'
                        ? [ ...lines.slice(0, -1), lines.slice(-1) + line.slice(8, 24) + '...  ' ]
                        : [ ...lines, line ],
                    [])
                .join('\n    ')
        );

        // This project has set `"type": "module"` in 'package.json', so rename
        // 'text-canvas.js' (which uses `require()`) to 'text-canvas.cjs'.
        // Also, the generated .gitignore is not needed, so remove it.
        renameSync(
            join(outDir, rsName + '.js'),
            join(outDir, rsName + '.cjs')
        );
        rmSync(join(outDir, '.gitignore'));

        // Show an 'OK' or 'Error!' message.
        console.log(green('OK') + 'wasm-pack build "--target nodejs" succeeded\n');
    } catch (err) { console.error(red('Error!') + '\n', err); process.exit(1) }

};

const filename = import.meta.url.split('/').slice(-1)[0]; // 'build-node-wasm.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;
if (isDirectCall) { buildNodeWasm(argv[2]) }
