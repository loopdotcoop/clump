// scripts/build-rs/build-cli-binary.js

import { execSync } from 'node:child_process';
import { existsSync, mkdirSync, copyFileSync, rmSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { green, red } from '../utilities/ansi.js';

export const buildCliBinary = async rsName => {
    const pf = 'buildCliBinary(): ';

    // Validate the Rust app's name.
    if (typeof rsName !== 'string') throw TypeError(
        `${pf}rsName is type '${typeof rsName}' not 'string'`);
    if (! /^[a-z][_0-9a-z]{0,23}$/.test(rsName)) throw TypeError(
        `${pf}rsName '${rsName}' fails /^[a-z][_0-9a-z]{0,23}$/`);

    // Get the distribution-folder path, and the compiled binary filename.
    const outDir = join('dist', rsName, 'cli-binary');
    const file = process.platform.slice(0, 3) === 'win'
        ? rsName + '.exe'
        : rsName;

    try {
        // Create the output directory, if missing.
        if (! existsSync(outDir)) mkdirSync(outDir, { recursive: true });

        // Delete any files generated during a previous build.
        [rsName, rsName+'.exe', rsName+'.pdb']
            .forEach(prevFile => rmSync(
                join(outDir, prevFile),
                { force: true }, // don't stop if the file doesn't exist
            )
        );

        // Compile the binary. This will generate the target/debug/ folder, and
        // the  .rustc_info.json and CACHEDIR.TAG files, in the crate.
        const buildResult = execSync(
            'cargo build',
            { cwd: 'src/rs-apps/' + rsName }, // path to the Rust crate
        ).toString();

        // Copy the compiled binary to the dist/cli-binary/ folder.
        const copyResult = copyFileSync(
            `src/rs-apps/${rsName}/target/debug/${file}`,
            join(outDir, file),
        );

        // Compile the binary.
        // const result = execSync(
        //     'rustc ' + // the Rust compiler
        //     `src/rs-apps/${rsName}/src/main.rs ` + // the main source file
        //     `-o ${join(outDir, file)}` // the compiled binary app
        // ).toString();

        // Show an 'OK' or 'Error!' message.
        console.log(green('OK') + (buildResult || copyResult || 'rustc succeeded'));
    } catch (err) { console.error(red('Error!') + '\n', err); process.exit(1) }

};

const filename = import.meta.url.split('/').slice(-1)[0]; // 'build-cli-binary.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;
if (isDirectCall) { buildCliBinary(argv[2]) }
