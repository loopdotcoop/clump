// scripts/dev.js

import { config } from '../src/pages/config.js';
import { runDevServer } from './dev/run-dev-server.js';

// `npm run dev -- --open` or `npm run dev -- -o` should open the dev server in
// a new browser window.
const doOpen = process.argv.includes('--open') || process.argv.includes('-o');

// `npm run dev -- --pt` should use Portuguese content for the dev session.
let languageCode;
['en', 'es', 'pt'].forEach(lc => {
    if (! process.argv.includes('--' + lc)) return;
    if (languageCode) throw Error(`Language --${languageCode} already selected`);
    languageCode = lc });
languageCode = languageCode || 'en'; // default if no language option

runDevServer(config, {
    dirContent: 'src/pages/content',
    dirDist: 'dist',
    dirSrc: 'src/pages/templates',
    doOpen,
    languageCode,
});
