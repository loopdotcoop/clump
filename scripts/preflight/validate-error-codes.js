// scripts/preflight/validate-error-codes.js

import { readdirSync, readFileSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { green, red } from '../utilities/ansi.js';
import {
    hasJsOrRsExtension,
    isAFile,
    isNotInRustTargetDir,
    isNotInTemplatesJsDir,
    isNotInTemplatesStyleDir,
    isNotInvisible,
} from './preflight-utilities.js';

// Searches the src/ folder for problems related to error-codes.
export const validateErrorCodes = (cache, itemsInSrc) => {

    // Refine the src/ listing, to only include .js and .rs files.
    const toCheckInSrc = itemsInSrc
        .filter(isNotInRustTargetDir)
        .filter(isNotInTemplatesJsDir)
        .filter(isNotInTemplatesStyleDir)
        .filter(isAFile)
        .filter(isNotInvisible)
        .filter(hasJsOrRsExtension);

    // Record all error-codes in an object.
    const allMeta = gatherAllErrorCodeMeta(cache, toCheckInSrc);

    // Look for duplicates, inconsistencies and other problems relating to error
    // codes in the src/ folder.
    const { tally, text } = determineErrorCodeProblems(allMeta);

    // Show error messages if any files in src/ failed.
    if (tally !== 0) {
        console.error(text.join('\n'));
        const txt = tally === 1
            ? ' error-code problem'
            : ' error-code problems'
        console.error(`${red('FAIL')}${tally}${txt}`) }

    // If there were no problems, show a success message and return 0.
    // Otherwise, return the number of files which failed.
    if (tally === 0) {
        console.log(`${green('OK')}None of the ${toCheckInSrc.length} checked ` +
            'src/ files have error-code problems');
        return 0;
    } else {
        return tally;
    }
};

// Run `validateErrorCodes()`, if this file was invoked directly.
const filename = import.meta.url.split('/').slice(-1)[0]; // 'validate-error-codes.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;

if (isDirectCall) {
    const readdirOptions = { withFileTypes: true, recursive: true };
    const itemsInSrc = readdirSync('src', readdirOptions);
    validateErrorCodes(null, itemsInSrc);
}


/* --------------------------------- PRIVATE -------------------------------- */

function gatherAllErrorCodeMeta(cache, items) {
    const allMeta = {
        errorCodes: [
            {}, // index 0 should not used
            {}, // index 1 is for typical src/pages/ error codes, 1_**** TODO start using it
            {}, // index 2 is for typical src/rs-app/ error codes, 2_****
            {}, // index 3 is for typical src/rs-lib/ error codes, 3_****
            {}, {}, {}, {}, {}, {}, // indices 4 to 9 should not used
        ],
    };

    // Gather error-code metadata from each .js and .rs file - this will add
    // arrays of sub-objects to the `allMeta` object.
    items.forEach(item => gatherErrorCodeMetaFromItem(allMeta, cache, item));

    return allMeta;
}

function gatherErrorCodeMetaFromItem(allMeta, cache, item) {

    // Read the content from the cache if available, or else from the filesystem.
    const path = join(item.path, item.name).replace(/\\/g, '/'); // POSIX-style
    let lines;
    if (cache && cache[path]) {
        lines = cache[path];
    } else {
        try {
            lines = readFileSync(path).toString().split('\n');
            if (cache) cache[path] = lines;
        } catch (err) { throw Error('validateErrorCodes(): ' + err.message) } }

    // True for Rust .rs files, false for JavaScript .js files.
    const isRust = path.slice(-3) === '.rs';

    // True in 'src/rs-apps/my_app/tests/e2e/my_app-common.js', or after finding
    // the `...--- TEST ---...` crosshead.
    let inTest = path.includes('/tests/');

    // Step through the file, line-by-line.
    let lineNum = 0;
    for (const line of lines) {
        lineNum++;

        // Ignore this line if there's no underscore and we already found the
        // start of a `...--- TEST ---...` section.
        const hasUnderscore = line.includes('_'); // assumed to be faster than regexp
        if (inTest && ! hasUnderscore) continue;

        // If this line is the special `...--- TEST ---...` crosshead, then any
        // error-codes in subsequent lines cannot be the error's origin.
        if (! inTest && line.includes('---- '+'TEST ----')) {
            inTest = true; continue }

        // Look for a 'digit-underscore-digit-digit-digit-digit' sequence,
        // bounded by 'non-word' characters. Ignore this line if none exists.
        const errorCode = line.match(/\b(\d)_(\d{4})\b/);
        if (! errorCode) continue;

        // Get the digit before the underscore, and the four following digits.
        const [ _, codeGroup, codeId ] = errorCode;

        // Look for `Err((` or `Error([` before the error-code, which signifies
        // that this is the error's origin.
        const isOrigin = isRust
            ? /\bErr\s*\(\s*\(\s*$/.test(line.slice(0, errorCode.index))
            : /Error\s*\(\s*\[\s*$/.test(line.slice(0, errorCode.index));

        // Detect when there's more than one error-code on one line.
        // TODO record each error-code as its own meta object
        const andAnother = /\b(\d)_(\d{4})\b/.test(line.slice(errorCode.index+6));

        // Record various metadata to the `allMeta` object.
        const meta = {
            andAnother,
            charPos: errorCode.index + 1,
            inTest,
            isOrigin,
            isRust,
            line,
            lineNum,
            path,
        };
        const existingCodeMeta = allMeta.errorCodes[+codeGroup][codeId];
        if (existingCodeMeta) {
            existingCodeMeta.metas.push(meta);
        } else {
            allMeta.errorCodes[+codeGroup][codeId] = { metas: [meta] }
        }
    }
}

function determineErrorCodeProblems(allMeta) {
    const problems = { tally: 0, text: [] };

    // Step through all ten objects in `allMeta.errorCodes`.
    for (let i=0; i<10; i++) {

        // Deal with error-codes which do not start "1_", "2_" or "3_".
        if (i === 0 || i > 3) {
            const badStartTally = Object.keys(allMeta.errorCodes[i]).length;
            problems.tally += badStartTally;
            if (badStartTally === 1) {
                problems.text.push(`There is 1 error-code starting ${i}_:`);
            } else if (badStartTally > 1) {
                problems.text.push(`There are ${badStartTally} error-codes starting ${i}_:`);
            }
            for(const [codeId, { metas }] of Object.entries(allMeta.errorCodes[i])) {
                problems.text.push(`    ${i}_${codeId}`);
                metas.forEach(({ charPos, lineNum, path }) =>
                    problems.text.push(`        ${path}:${lineNum}:${charPos}`));
            }
            continue; // don't look for further problems with these error-codes
        }

        Object.entries(allMeta.errorCodes[i]).forEach(([ codeId, { metas } ]) => {

            // Deal with two or more error-codes on a single line.
            metas.forEach(({ andAnother, charPos, line, lineNum, path }) => {
                if (! andAnother) return;
                problems.tally++;
                problems.text.push(
                    `${i}_${codeId} is followed by one or more error-codes in:`,
                    `    ${path}:${lineNum}:${charPos}`,
                    `    The line, from the ${i}_${codeId} onwards, is:`,
                    `    ... ${line.slice(charPos-1)}`) })

            // Get the error-code's 'origin' - that is, the place in code that
            // it is created, either with an `Err((...))` or an `Error([...])`.
            const origins = metas.filter(meta => meta.isOrigin);
            const originsTally = origins.length;

            // Deal with a missing origin.
            if (originsTally === 0) {
                problems.tally++;
                const pT = metas.length; // placesTally
                problems.text.push(`${i}_${codeId} has no origin. It's ` +
                    `used in ${pT} place${pT === 1 ? '' : 's'}:`);
                metas.forEach(({ charPos, lineNum, path }) =>
                    problems.text.push(`    ${path}:${lineNum}:${charPos}`));
                return;
            }

            // Deal with two or more origins.
            if (originsTally > 1) {
                problems.tally += originsTally - 1;
                problems.text.push(`${i}_${codeId} has ${originsTally}` +
                    ' origins (should just be 1):');
                origins.forEach(({ charPos, lineNum, path }) =>
                    problems.text.push(`    ${path}:${lineNum}:${charPos}`));
                return;
            }

            // There is exactly one origin for this error-code.
            const origin = origins[0];
            const { charPos, inTest, lineNum, path } = origin;

            // The digit that the error-code starts with should depend on
            // whether it's a Rust app, Rust library, or JavaScript error.
            showProblemIfBadStartDigit(
                codeId,
                (['src/pages/','src/rs-apps/','src/rs-libs/'])[i-1],
                i, // note that this is in a `for (let i=1; i<=3; i++)` loop
                origin,
                problems,
            );

            // The origin is not allowed to be in a '.../tests/...' folder, and
            // not allowed to be below the `...--- TEST ---...` crosshead.
            if (path.includes('/tests/')) {
                problems.tally++;
                problems.text.push(
                    `${i}_${codeId}'s origin is in a '.../tests/...' folder:`,
                    `    ${path}:${lineNum}:${charPos}`);
            } else if (inTest) {
                problems.tally++;
                problems.text.push(
                    `${i}_${codeId}'s origin is below a \`...--- TEST ---...\` crosshead:`,
                    `    ${path}:${lineNum}:${charPos}`);
            }

            // The error-code must appear in at least one testing context.
            const tests = metas.filter(meta => meta.inTest);
            if (tests.length === 0) {
                problems.tally++;
                problems.text.push(
                    `${i}_${codeId} does not appear in any testing contexts. `
                    + 'Its origin is in:',
                    `    ${path}:${lineNum}:${charPos}`);
            }
        });
    }

    return problems;
};

function showProblemIfBadStartDigit(codeId, expectedPrefix, i, originMeta, problems) {
    const { charPos, lineNum, path } = originMeta;
    if (path.slice(0, expectedPrefix.length) !== expectedPrefix) { // eg 'src/pages/'
        problems.tally++;
        problems.text.push(
            `${i}_${codeId} starts "${i}_" so its should be in ` +
            `'${expectedPrefix}', but it's in:`,
            `    ${path}:${lineNum}:${charPos}`);
    }
}
