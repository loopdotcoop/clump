// scripts/preflight/preflight-utilities.js

export const getExtension = item =>
    item.name.split('.').pop();

export const hasJsOrRsExtension = item =>
    ['js','rs'].includes(getExtension(item));

export const hasTypicalSupportedExtension = item =>
    ['css','html','js','md','rs','toml'].includes(getExtension(item));

export const isAFile = item =>
    item.isFile();

export const isNotInRustTargetDir = item =>
    isNotInDir(item, '/target/');

export const isNotInTemplatesJsDir = item =>
    isNotInDir(item, '/templates/js');

export const isNotInTemplatesStyleDir = item =>
    isNotInDir(item, '/templates/style');

export const isNotInvisible = item =>
    item.name[0] !== '.'; // eg .DS_Store


/* --------------------------------- PRIVATE -------------------------------- */

function isNotInDir(item, posixPath) {
    return ! item.path.includes(posixPath) &&
        ! item.path.includes(posixPath.replace(/\//g, '\\')) }
