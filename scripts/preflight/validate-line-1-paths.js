// scripts/preflight/validate-line-1-paths.js

import { readdirSync, readFileSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { green, red } from '../utilities/ansi.js';
import {
    getExtension,
    hasTypicalSupportedExtension,
    isAFile,
    isNotInRustTargetDir,
    isNotInTemplatesJsDir,
    isNotInTemplatesStyleDir,
    isNotInvisible,
} from './preflight-utilities.js';

// Searches the scripts/ and src/ folders for files with invalid first-lines.
export const validateLine1Paths = (cache, itemsInScripts, itemsInSrc) => {

    // Search the scripts/ folder for files with invalid first-lines.
    const toCheckInScripts = itemsInScripts
        .filter(isAFile)
        .filter(isNotInvisible)
        .filter(hasTypicalSupportedExtension);
    const invalidInScripts = toCheckInScripts
        .filter(item => hasNoValidLine1Path(cache, item));

    // Show error messages if any files in scripts/ failed.
    const iiScriptsLen = invalidInScripts.length;
    if (iiScriptsLen !== 0) {
        const txt = iiScriptsLen === 1
            ? '1 file in scripts/ has'
            : `${iiScriptsLen} files in scripts/ have`;
        console.error(`${red('FAIL')}${txt} an invalid 'line 1 path':`) }
    invalidInScripts.map(item => join(item.path, item.name))
        .sort().forEach(item => console.error(item));

    // Search the src/ folder for files with invalid first-lines.
    const toCheckInSrc = itemsInSrc
        .filter(isNotInRustTargetDir)
        .filter(isNotInTemplatesJsDir)
        .filter(isNotInTemplatesStyleDir)
        .filter(isAFile)
        .filter(isNotInvisible)
        .filter(hasTypicalSupportedExtension);
    const invalidInSrc = toCheckInSrc
        .filter(item => hasNoValidLine1Path(cache, item));

    // Show error messages if any files in src/ failed.
    const iiSrcLen = invalidInSrc.length;
    if (iiSrcLen !== 0) {
        if (iiScriptsLen) console.error(); // print a newline
        const txt = iiSrcLen === 1
            ? '1 file in src/ has'
            : `${iiSrcLen} files in src/ have`;
        console.error(`${red('FAIL')}${txt} an invalid 'line 1 path':`) }
    invalidInSrc.map(item => join(item.path, item.name))
        .sort().forEach(item => console.error(item));

    // If there were no problems, show a success message and return 0.
    // Otherwise, return the number of files which failed.
    if (iiScriptsLen + iiSrcLen === 0) {
        const total = toCheckInScripts.length + toCheckInSrc.length;
        console.log(`${green('OK')}All ${total
            } files checked in scripts/ and src/ have valid 'line 1 paths'`);
        return 0;
    } else {
        return iiScriptsLen + iiSrcLen;
    }
};

// Run `validateLine1Paths()`, if this file was invoked directly.
const filename = import.meta.url.split('/').slice(-1)[0]; // 'validate-line-1-paths.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;

if (isDirectCall) {
    const readdirOptions = { withFileTypes: true, recursive: true };
    const itemsInScripts = readdirSync('scripts', readdirOptions);
    const itemsInSrc = readdirSync('src', readdirOptions);
    validateLine1Paths(null, itemsInScripts, itemsInSrc);
}


/* --------------------------------- PRIVATE -------------------------------- */

function hasNoValidLine1Path(cache, item) {

    // Read the content from the cache if available, or else from the filesystem.
    const path = join(item.path, item.name).replace(/\\/g, '/');
    let lines;
    if (cache && cache[path]) {
        lines = cache[path];
    } else {
        try {
            lines = readFileSync(path).toString().split('\n');
            if (cache) cache[path] = lines;
        } catch (err) { throw Error('validateLine1Paths(): ' + err.message) } }

    // Get the file's first line, and remove any leading or trailing whitespace.
    const line1 = lines[0].trim();

    // Ignore an empty file, or where the first line is blank.
    if (! line1) return;

    // Check whether the line is the correct commented-out path.
    switch (getExtension(item)) {
        case 'css':
            if (line1 === `/* ${path} */`) return;
        case 'html':
            if (line1 === `<!-- ${path} -->`) return;
        case 'md':
            if (line1 === '---' || line1 === `<!-- ${path} -->`) return;
        case 'js':
            if (line1 === `// ${path}` || line1 === `/* ${path} */`) return;
        case 'rs':
            if (line1 === `//! ${path}`) return;
        case 'toml':
            if (line1 === `# ${path}`) return;
    }

    // Does not have commented-out path on line 1, or the path is incorrect.
    return true;
};
