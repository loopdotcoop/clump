// scripts/preflight/find-trailing-whitespace.js

import { readdirSync, readFileSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { green, red } from '../utilities/ansi.js';
import {
    getExtension,
    hasTypicalSupportedExtension,
    isAFile,
    isNotInRustTargetDir,
    isNotInvisible,
} from './preflight-utilities.js';

// Searches the scripts/ and src/ folders for lines with trailing whitespace.
export const findTrailingWhitespace = (cache, itemsInScripts, itemsInSrc) => {

    // Search the scripts/ folder for lines with trailing whitespace.
    const toCheckInScripts = itemsInScripts
        .filter(isAFile)
        .filter(isNotInvisible)
        .filter(hasTypicalSupportedExtension);
    const invalidInScripts = toCheckInScripts
        .filter(item => hasOneOrMoreUntrimmedLines(cache, item));

    // Show error messages if any files in scripts/ failed.
    const iiScriptsLen = invalidInScripts.length;
    if (iiScriptsLen !== 0) {
        const txt = iiScriptsLen === 1
            ? '1 file in scripts/ has at least one line'
            : `${iiScriptsLen} files in scripts/ have at least one line`;
        console.error(`${red('FAIL')}${txt} with invalid trailing whitespace:`) }
    invalidInScripts.map(item => join(item.path, item.name))
        .sort().forEach(item => console.error(item));

    // Search the src/ folder for lines with trailing whitespace.
    const toCheckInSrc = itemsInSrc
        .filter(isNotInRustTargetDir)
        .filter(isAFile)
        .filter(isNotInvisible)
        .filter(hasTypicalSupportedExtension);
    const invalidInSrc = toCheckInSrc
        .filter(item => hasOneOrMoreUntrimmedLines(cache, item));

    // Show error messages if any files in src/ failed.
    const iiSrcLen = invalidInSrc.length;
    if (iiSrcLen !== 0) {
        if (iiScriptsLen) console.error(); // print a newline
        const txt = iiSrcLen === 1
            ? '1 file in scripts/ has at least one line'
            : `${iiSrcLen} files in src/ have at least one line`;
        console.error(`${red('FAIL')}${txt} with invalid trailing whitespace':`) }
    invalidInSrc.map(item => join(item.path, item.name))
        .sort().forEach(item => console.error(item));

    // If there were no problems, show a success message and return 0.
    // Otherwise, return the number of files which failed.
    if (iiScriptsLen + iiSrcLen === 0) {
        const total = toCheckInScripts.length + toCheckInSrc.length;
        console.log(`${green('OK')}None of the ${total} checked scripts/ ` +
            'and src/ files have lines with invalid trailing whitespace');
        return 0;
    } else {
        return iiScriptsLen + iiSrcLen;
    }
};

// Run `findTrailingWhitespace()`, if this file was invoked directly.
const filename = import.meta.url.split('/').slice(-1)[0]; // 'validate-line-1-paths.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;

if (isDirectCall) {
    const readdirOptions = { withFileTypes: true, recursive: true };
    const itemsInScripts = readdirSync('scripts', readdirOptions);
    const itemsInSrc = readdirSync('src', readdirOptions);
    findTrailingWhitespace(null, itemsInScripts, itemsInSrc);
}


/* --------------------------------- PRIVATE -------------------------------- */

function hasOneOrMoreUntrimmedLines(cache, item) {

    // Read the content from the cache if available, or else from the filesystem.
    const path = join(item.path, item.name);
    let lines;
    if (cache && cache[path]) {
        lines = cache[path];
    } else {
        try {
            lines = readFileSync(path).toString().split('\n');
            if (cache) cache[path] = lines;
        } catch (err) { throw Error('findTrailingWhitespace(): ' + err.message) } }

    // Markdown files are treated slightly differently.
    const isNotMarkdown = getExtension(item) !== 'md';

    // Check each line for trailing whitespace.
    for (const line of lines) {
        const lastChar = line.slice(-1); // will be "" for a blank line
        if (lastChar === ' ' || lastChar === '\t') {

            // For non-Markdown files, no trailing spaces or tabs are allowed.
            if (isNotMarkdown) return true;

            // For Markdown files, exactly two trailing spaces (not tabs) after
            // certain punctuation is allowed. A double-space at the end of
            // a line is interpreted as a line-break, by most Markdown parsers.
            const lastFiveChars = line.slice(-5);
            const lastThreeChars = line.slice(-3);
            if (
                lastFiveChars !== ':__  ' &&
                lastFiveChars !== '.__  ' &&
                lastThreeChars !== ':  ' &&
                lastThreeChars !== '.  '
            ) return true;
        }
    }
};
