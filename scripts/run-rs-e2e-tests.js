// scripts/run-rs-e2e-tests.js

import { readFileSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { testCliBinary } from './run-rs-e2e-tests/test-cli-binary.js';
import { testNodeWasm } from './run-rs-e2e-tests/test-node-wasm.js';
import { green } from './utilities/ansi.js';

// Runs end-to-end tests on one or more Rust apps.
// The web browser .wasm e2e tests can't be done this way.
const runRsE2eTests = async rsNames => {

    for (const rsName of rsNames) {
        console.log('-'.repeat(78));
        console.log(`Running testCliBinary('${rsName}')...`);
        await testCliBinary(rsName);
        console.log(`Running testNodeWasm('${rsName}')...`);
        await testNodeWasm(rsName);
    }

    const len = rsNames.length;
    console.log('='.repeat(78));
    console.log(`\n${green('Done!')}${len*2} e2e test suites for ${len} `
        + `Rust app${len==1?'':'s'}`);

    console.log(`
        Web browser .wasm files will also need to be tested in the browser,
        as part of the end-to-end and integration tests of the HTML pages.
        Alternatively the web browser .wasm files can be tested using:

        node scripts/run-rs-e2e-tests/test-web-browser-wasm.js ${rsNames.join()}\n`);
    console.log('='.repeat(78));
}

// Run end-to-end tests on the specified Rust apps. If none are specified, run
// end-to-end tests on all Rust apps.
//
// These two invocations will populate `argv[2]`:
//     node scripts/run-rs-e2e-tests.js some_rust_app,another
//     npm run test:e2e:rs -- some_rust_app,another
// These won't:
//     npm run test:e2e -- nope
//     npm run test -- also_nope
const rsNames = argv[2]
    ? argv[2].split(',') // TODO something like chooseCrateNamesFromArgs()
    : readRsAppNamesFromVsCodeSettings(); // eg ['hello_rust', 'another']
runRsE2eTests(rsNames);


/* --------------------------------- PRIVATE -------------------------------- */

// Reads and parses the .vscode/setting.json file from the top level of the repo.
function readRsAppNamesFromVsCodeSettings() { // TODO move to utilities
    const pf = 'readRsAppNamesFromVsCodeSettings(): ';

    const linkRx = /^\.\/src\/(rs-apps|rs-libs)\/([a-z][_0-9a-z]{0,23})\/Cargo\.toml$/;

    // Read and parse the .vscode/settings.json file.
    let linkedProjects; try {
        const raw = readFileSync(join('.vscode', 'settings.json'));
        const parsed = JSON.parse(raw.toString());
        linkedProjects = parsed['rust-analyzer.linkedProjects'];
        if (! Array.isArray(linkedProjects)) throw Error(
            'It has no "rust-analyzer.linkedProjects" array');
        linkedProjects.forEach((lp, i) => {
            if (typeof lp !== 'string') throw Error(
                `linkedProjects[${i}] is type '${typeof lp}' not 'string'`);
            if (! linkRx.test(lp)) throw Error(
                `linkedProjects[${i}] fails ${linkRx}`);
        });
    } catch(err) { throw Error(
        pf + 'Cannot read or parse .vscode/settings.json\n    ' + err.message) }

    // Get an array containing the names of the apps in the src/rs-apps/ folder.
    const rsAppNames = linkedProjects
        .map(lp => lp.match(linkRx))
        .filter(match => match[1] === 'rs-apps')
        .map(match => match[2])
    ;
    return rsAppNames;
}
