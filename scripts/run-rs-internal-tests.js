// scripts/run-rs-internal-tests.js

import { spawnSync } from 'node:child_process';
import { readFileSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { green, red } from './utilities/ansi.js';
import { isPlainObjOrInst } from './utilities/is-type.js';

// Runs unit and integration tests on one or more Rust app and/or library.
const runRsInternalTests = crateNames => {
    const pf = 'runRsInternalTests(): ';

    // Validate each Rust app or library name.
    if (! isPlainObjOrInst(crateNames)) throw TypeError(
        `${pf}crateNames is not a plain object or instance`);
    const { apps, libs } = crateNames;
    if (! Array.isArray(apps)) throw TypeError(
        `${pf}crateNames.apps is type '${typeof apps}' not an array`);
    if (! Array.isArray(libs)) throw TypeError(
        `${pf}crateNames.libs is type '${typeof libs}' not an array`);
    apps.forEach((app, i) => {
        if (typeof app !== 'string') throw TypeError(
            `${pf}crateNames.apps[${i}] is type '${typeof app}' not 'string'`);
        if (! /^[a-z][_0-9a-z]{0,23}$/.test(app)) throw TypeError(
            `${pf}crateNames.apps[${i}] '${rsName}' fails /^[a-z][_0-9a-z]{0,23}$/`);
    });
    libs.forEach((lib, i) => {
        if (typeof lib !== 'string') throw TypeError(
            `${pf}crateNames.libs[${i}] is type '${typeof lib}' not 'string'`);
        if (! /^[a-z][_0-9a-z]{0,23}$/.test(lib)) throw TypeError(
            `${pf}crateNames.libs[${i}] '${rsName}' fails /^[a-z][_0-9a-z]{0,23}$/`);
    });

    // Run each Rust app or library's unit and integration tests.
    const testTally = apps.length + libs.length;
    const failTally =
        libs.reduce((tally, lib) =>
            tally + runInternalTestsOfOneRs(lib, false), 0) +
        apps.reduce((tally, app) =>
            tally + runInternalTestsOfOneRs(app, true), 0);

    // Show a 'PASS' or 'FAIL' message.
    const e = '\n' + '='.repeat(78); // a row of equals signs to end output with
    console.log(e);
    if (failTally) {
        switch (testTally) { // 0 is unreachable
            case 1: return console.error(
                `${red('FAIL')}The Rust crate's internal tests failed${e}`);
            case 2: return console.error(
                failTally === 1
                    ? `${red('FAIL')}One of the two Rust crates' internal tests failed${e}`
                    : `${red('FAIL')}Both Rust crates' internal tests failed${e}`);
            default: return console.error(
                `${red('FAIL')}${failTally} of ${testTally} Rust crates' internal tests failed${e}`);
        }
    } else {
        switch (testTally) {
            case 0: return console.log(
                `${green('PASS')}No Rust apps or libraries were specified${e}`);
            case 1: return console.log(
                `${green('PASS')}The Rust crate's internal tests passed${e}`);
            case 2: return console.log(
                `${green('PASS')}Both Rust crates' internal tests passed${e}`);
            default: return console.log(
                `${green('PASS')}All ${testTally} Rust crates' internal tests passed${e}`);
        }
    }
}

// Determine whether this file has been invoked directly using:
//   node scripts/run-rs-internal-tests.js some_rust_app,some_rust_lib
// ...or indirectly, eg via an NPM package.json script:
//   npm run test:internal:rs
const filename = import.meta.url.split('/').slice(-1)[0]; // 'run-rs-internal-tests.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;

// Read and parse the .vscode/setting.json file from the top level of the repo.
// Get an object like: `{ apps:['hello_rust','another'], libs:['parse'] }`.
const allCrateNames = readAllCrateNamesFromVsCodeSettings();

// Run Rust unit tests and integration tests on the specified Rust apps and
// libraries. If none are specified, run all Rust unit and integration tests.
//
// These two invocations will populate `argv[2]`:
//     node scripts/run-rs-internal-tests.js hello_rust,parse
//     npm run test:internal:rs -- hello_rust,parse
// These won't:
//     npm run test:internal -- nope
//     npm run test -- also_nope
const chosenCrateNames = argv[2]
    ? chooseCrateNamesFromArgs(allCrateNames, argv[2].split(','))
    : allCrateNames; // eg { apps:['hello_rust'], libs:['parse'] }
runRsInternalTests(chosenCrateNames);


/* --------------------------------- PRIVATE -------------------------------- */

// Runs unit and integration tests on one Rust app or library.
function runInternalTestsOfOneRs(rsName, isApp) {
    console.log('-'.repeat(78));

    // Get the path to the Rust crate's 'Cargo.toml' file.
    const path = process.platform.slice(0, 3) === 'win'
        ? `src\\rs-${isApp ? 'app' : 'lib'}s\\${rsName}\\Cargo.toml`
        : `./src/rs-${isApp ? 'app' : 'lib'}s/${rsName}/Cargo.toml`;

    let testResult; try {
        // Run the Rust crate's unit and integration tests.
        const spawnResult = spawnSync(
            'cargo', // the main Rust helper tool
            [ 'test', '--manifest-path', path ] // path to the 'Cargo.toml' file
        );
        // if (spawnResult.stderr) throw Error(spawnResult.stderr); // TODO use it or lose it
        console.log(spawnResult.stdout.toString());

        // Record that the tests passed or failed.
        testResult = spawnResult.status ? 'FAIL' : 'PASS';

    // Show an 'Error!' message and exit.
    } catch (err) { console.error(red('Error!') + '\n', err); process.exit(1) }

    // Show a 'PASS' or 'FAIL' message.
    const description = `${isApp ? 'App' : 'Library'} '${rsName}' internal tests`;
    if (testResult === 'PASS') {
        console.log(`${green('PASS')}${description} passed`);
        return 0;
    } else {
        console.error(`${red('FAIL')}${description} failed`);
        return 1;
    }
}

// Rebuilds a `crateNames` object, with only the user's specified crate names.
function chooseCrateNamesFromArgs({ apps, libs }, rsNames) {
    rsNames.forEach(rsName => {
        if (! apps.includes(rsName) && ! libs.includes(rsName)) throw Error(
            `No crate named '${rsName}' in src/rs-apps/ or src/rs-libs/`) });
    return {
        apps: apps.filter(app => rsNames.includes(app)),
        libs: libs.filter(lib => rsNames.includes(lib)),
    };
}

// Reads and parses the .vscode/setting.json file from the top level of the repo.
// Returns an object like:
//     {
//         apps: ['hello_rust', 'another'],
//         libs: ['constants', 'parse'],
//     }
function readAllCrateNamesFromVsCodeSettings() { // TODO move to utilities
    const pf = 'readAllCrateNamesFromVsCodeSettings(): ';

    const linkRx = /^\.\/src\/(rs-apps|rs-libs)\/([a-z][_0-9a-z]{0,23})\/Cargo\.toml$/;

    // Read and parse the .vscode/settings.json file.
    let linkedProjects; try {
        const raw = readFileSync(join('.vscode', 'settings.json'));
        const parsed = JSON.parse(raw.toString());
        linkedProjects = parsed['rust-analyzer.linkedProjects'];
        if (! Array.isArray(linkedProjects)) throw Error(
            'It has no "rust-analyzer.linkedProjects" array');
        linkedProjects.forEach((lp, i) => {
            if (typeof lp !== 'string') throw Error(
                `linkedProjects[${i}] is type '${typeof lp}' not 'string'`);
            if (! linkRx.test(lp)) throw Error(
                `linkedProjects[${i}] fails ${linkRx}`);
        });
    } catch(err) { throw Error(
        pf + 'Cannot read or parse .vscode/settings.json\n    ' + err.message) }

    // Create two arrays in an object, containing the names of the apps and libs
    // in the src/rs-apps/ and src/rs-libs/ folders.
    const allCrateNames = linkedProjects
        .map(lp => lp.match(linkRx))
        .reduce(
            ({ apps, libs }, match) => match[1] === 'rs-apps'
                ? { apps:[ ...apps, match[2] ], libs }
                : { apps, libs:[ ...libs, match[2] ] },
            { apps:[], libs:[] } // accumulator
        )
    ;

    // TODO Make sure that an app never has the same name as a library.

    // TODO Check that the crates really exist, in the correct src/rs-*/ folder.

    // Return the two arrays.
    return allCrateNames;
}
