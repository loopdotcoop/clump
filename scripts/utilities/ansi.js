// scripts/utilities/ansi.js

/** ### Makes functions which wrap text in ANSI styling
 * See <https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit>
 * @param {number} bg  An ANSI 256-color code */
const ansi = (bg) => (text) => `\x1b[31;97;48;5;${bg}m ${text} \x1b[0m `;

/** ### Wraps text in a blue-background ANSI code, followed by a space
 * @param {string} text */
export const blue = ansi(18);

/** ### Wraps text in a green-background ANSI code, followed by a space
 * @param {string} text */
export const green = ansi(22);

/** ### Wraps text in an orange-background ANSI code, followed by a space
 * @param {string} text */
export const orange = ansi(130);

/** ### Wraps text in a red-background ANSI code, followed by a space
 * @param {string} text */
export const red = ansi(52);

/** ### Wraps text in a grey-background ANSI code, followed by a space
 * @param {string} text */
export const silver = ansi(8);


/* ---------------------------------- TEST ---------------------------------- */

export const testAnsi = (equal) => {

    // Test blue().
    equal(blue('abc'), '\x1B[31;97;48;5;18m abc \x1B[0m ');

    // Test green().
    equal(green('abc'), '\x1B[31;97;48;5;22m abc \x1B[0m ');

    // Test orange().
    equal(orange('abc'), '\x1B[31;97;48;5;130m abc \x1B[0m ');

    // Test red().
    equal(red('abc'), '\x1B[31;97;48;5;52m abc \x1B[0m ');

    // Test silver().
    equal(silver('abc'), '\x1B[31;97;48;5;8m abc \x1B[0m ');

}
