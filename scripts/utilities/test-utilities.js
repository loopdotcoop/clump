// scripts/utilities/test-utilities.js

import { equal } from 'node:assert';
import { green, red } from './ansi.js';

import { testAnsi } from './ansi.js';
import { testIsType } from './is-type.js';

const showFail = (err, fnName) => {
    const place = err.stack?.split('\n') // split into lines
        .find(line => /^\s+at /.test(line)) // the first line starting "    at "
        .split('/').pop() // text after the last "/", eg "some-file.js:344:5)"
        || 'unknown location)'; // fallback
    console.error(`${red('FAIL')}${err.message}\n(${place}\n`);
    err.where = fnName;
    throw err;
}

export const testUtilities = () => {
    try { testAnsi(equal) } catch (err) {
        showFail(err, 'testAnsi()') }
    try { testIsType(equal) } catch (err) {
        showFail(err, 'testIsType()') }
    console.log(`${green('PASS')}scripts/utilities/ test suite passed`);
};

const filename = import.meta.url.split('/').slice(-1)[0]; // 'test-utilities.js'
const isDirectCall = process.argv[1].slice(-filename.length) === filename;
if (isDirectCall) { process.exit(testUtilities()) }
