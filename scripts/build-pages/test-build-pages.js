// scripts/build-pages/test-build-pages.js

import { doesNotThrow, equal, throws } from 'node:assert';
import { green, red } from '../utilities/ansi.js';

import { testImp } from './imp.js';
import { testRuffMarkdownToHtml } from './ruff-markdown-to-html.js';
import { testValidateConfig } from './validate-config.js';

const showFail = (err, fnName) => {
    const place = err.stack?.split('\n') // split into lines
        .find(line => /^\s+at /.test(line)) // the first line starting "    at "
        .split('/').pop() // text after the last "/", eg "some-file.js:344:5)"
        || 'unknown location)'; // fallback
    console.error(`${red('FAIL')}${err.message}\n(${place}\n`);
    err.where = fnName;
    throw err;
}

export const testBuildPages = () => {
    try { testImp(equal, throws) } catch (err) {
        showFail(err, 'testImp()') }
    try { testRuffMarkdownToHtml(equal) } catch (err) {
        showFail(err, 'testRuffMarkdownToHtml()') }
    try { testValidateConfig(doesNotThrow, throws) } catch (err) {
        showFail(err, 'testValidateConfig()') }
    console.log(`${green('PASS')}scripts/build-pages/ test suite passed`);
};

const filename = import.meta.url.split('/').slice(-1)[0]; // 'test-build-pages.js'
const isDirectCall = process.argv[1].slice(-filename.length) === filename;
if (isDirectCall) { process.exit(testBuild()) }
