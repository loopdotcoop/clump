// scripts/build-pages/render-sidebar-page.js

/** ## Xx
 *
 * @param   {[type]}  pf              [pf description]
 * @param   {[type]}  frontmatter     [frontmatter description]
 * @param   {[type]}  html            [html description]
 * @param   {[type]}  languageCode    [languageCode description]
 * @param   {[type]}  translatedPage  [translatedPage description]
 *
 * @return  {[type]}                  [return description]
 */
export const renderSidebarPage = (pf, frontmatter, html, languageCode, translatedPage) => {
    return translatedPage
        .replace(/{{languageCode}}/g, languageCode)
        .replace(/{{title}}/g, // insert U+200A 'HAIR SPACE' between every character
            `LOOP.COOP / ${frontmatter.title}`.split('').join('\u200A'))
        .replace('{{content.topHtmlComment}}', extractTopHtmlComment(pf, html))
        .replace('{{content.dev}}', extractSection(pf, html, '$ DEV', '/dev'))
        .replace('{{content.help}}', extractSection(pf, html, '? HELP', '/help'))
        .replace('{{content.like}}', extractSection(pf, html, '* LIKE', '/like'))
        .replace('{{content.list}}', extractSection(pf, html, '= LIST', '/list'))
        .replace('{{content.user}}', extractSection(pf, html, '^ USER', '/user'))
    ;
}


/* --------------------------------- PRIVATE -------------------------------- */


// TODO unit test
const extractTopHtmlComment = (pf, html) => {
    const beginPos = html.indexOf('<!-- ');
    if (beginPos === -1) throw RangeError(`${pf}Cannot find any comments "<!-- "`);
    const endPos = html.indexOf(' -->', beginPos + 4);
    if (beginPos === -1) throw RangeError(`${pf}Cannot find comment end " -->"`);
    return html.slice(beginPos, endPos + 4);
}

// TODO unit test
const extractSection = (pf, html, h2InnerText, sectionId) => {

    // Get the section's HTML.
    let pos = html.indexOf(`<h2>${h2InnerText}</h2>`);
    if (pos === -1) throw RangeError(
        `${pf}Cannot find section starting "<h2>${h2InnerText}</h2>"`);
    let endPos = html.indexOf('\n<h2>', pos+8);
    if (endPos === -1) endPos = html.length - 1; // it's the last section
    let sectionHtml = html.slice(pos, endPos);

    // Add an id attribute to the first paragraph, and begin the <div>.
    pos = sectionHtml.indexOf('<p>');
    if (pos === -1) throw RangeError(`${pf}Cannot find any paragraphs "<p>"`);
    sectionHtml =
        sectionHtml.slice(0, pos)
        + '<div>\n'
        + `<p id="/0${sectionId}">`
        + sectionHtml.slice(pos + 3);

    // Record the position of the end of the first paragraph - this will be used
    // for inserting the '<nav>'.
    const endOfFirstParaPos = sectionHtml.indexOf('</p>\n\n', pos + 4) + 6;
    if (endOfFirstParaPos === -1) throw RangeError(`${pf}Cannot find "</p>[NL][NL]"`);

    // Add id attributes to each <h3> element.
    // Also, build the 'subsections' array.
    const subsections = [];
    sectionHtml = extractSubsections(pf, sectionId, sectionHtml, subsections);

    // Add a '<nav>' near the top, with links to each subsection and sub-subsection.
    sectionHtml =
        sectionHtml.slice(0, endOfFirstParaPos)
        + generateNav(pf, sectionId, subsections)
        + sectionHtml.slice(endOfFirstParaPos);

    // Add the "\ TOP" button, and finish the container-div.
    return sectionHtml
        + `\n<a href="#/0${sectionId}" class="btn btn-top" `
        + 'title="{{t.sidebar_title_top}}">\\ TOP</a>'
        + '\n</div>\n';
}

const extractSubsections = (pf, sectionId, sectionHtml, subsections) => {
    let pos = 0;
    while (true) {

        // Find the next <h3> element.
        pos = sectionHtml.indexOf('<h3>', pos + 4);
        if (pos === -1) break;
        const endPos = sectionHtml.indexOf('</h3>', pos + 4);
        if (endPos === -1) throw RangeError(`${pf}Cannot find "</h3>"`);
        const h3InnerText = sectionHtml.slice(pos + 4, endPos);

        // Generate its id, and record it in `subsections`.
        const h3Id = textToId(h3InnerText);
        const subSubsections = [];
        subsections.push({ text:h3InnerText, h3Id, subSubsections });

        // Insert the id attribute into the <h3> element.
        sectionHtml =
            sectionHtml.slice(0, pos)
            + `<h3 id="/0${sectionId}/${h3Id}">`
            + sectionHtml.slice(pos + 4);

        // Process the sub-subsections, if any.
        const nextH3Pos = sectionHtml.indexOf('<h3>', pos + 4);
        let subsectionHtml = nextH3Pos === -1
            ? sectionHtml.slice(pos)
            : sectionHtml.slice(pos, nextH3Pos);
        subsectionHtml = extractSubSubsections(pf, sectionId, h3Id, subsectionHtml, subSubsections);
        sectionHtml =
            sectionHtml.slice(0, pos)
            + subsectionHtml
            + (nextH3Pos === -1 ? '' : sectionHtml.slice(nextH3Pos));
    }
    return sectionHtml;
}

const extractSubSubsections = (pf, sectionId, h3Id, subsectionHtml, subSubsections) => {
    let pos = 0;
    while (true) {

        // Find the next <h4> element.
        pos = subsectionHtml.indexOf('<h4>', pos + 4);
        if (pos === -1) break;
        const endPos = subsectionHtml.indexOf('</h4>', pos + 4);
        if (endPos === -1) throw RangeError(`${pf}Cannot find "</h4>"`);
        const h4InnerText = subsectionHtml.slice(pos + 4, endPos);

        // Generate its id, and record it in `subSubsections`.
        const h4Id = textToId(h4InnerText);
        subSubsections.push({ text:h4InnerText, h4Id });

        // Insert the id attribute into the <h4> element.
        subsectionHtml =
            subsectionHtml.slice(0, pos)
            + `<h4 id="/0${sectionId}/${h3Id}/${h4Id}">`
            + subsectionHtml.slice(pos + 4);
    }
    return subsectionHtml;
}

const textToId = text => text
    .replace(/\s+/g, '-') // replace whitespace with "-"
    .replace(/[^-_0-9a-z]/ig, '') // delete punctuation, apart from "-" and "_"
    .toLowerCase()

const generateNav = (pf, sectionId, subsections) => {
    const links = subsections.map(({ text, h3Id, subSubsections }) =>
        `<h6><a href="#/0${sectionId}/${h3Id}">${text}</a></h6>`
            + subSubsections.map(({ text, h4Id }) =>
                `\n    <h6><a href="#/0${sectionId}/${h3Id}/${h4Id}">${text}</a></h6>`
            ).join('')
    );
    return `<nav>\n    ${links.join('\n    ')}\n</nav>\n\n`;
}
