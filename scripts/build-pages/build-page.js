// scripts/build-pages/build-page.js

import { renderSidebarPage } from './render-sidebar-page.js';

/** ### Generates an HTML file
 *
 * @param   {Page}    page
 * @param   {object}  frontmatter
 * @param   {string}  html
 * @param   {object}  i18n
 * @param   {object}  templates
 * @param   {import('node:fs').dirname}  dirname
 * @param   {import('node:fs').mkdirSync}  mkdirSync
 * @param   {import('node:fs').writeFileSync}  writeFileSync
 * @param   {string}  outputPath
 */
export const buildPage = (page, frontmatter, html, i18n, languageCode, templates, dirname, mkdirSync, writeFileSync, outputPath) => {
    const pf = 'buildPage(): ';
    const template = templates[page.template];
    const languages = page.languages ? Object.keys(page.languages) : [];

    //
    const renderedPage = typeof page.redirectTo === 'string'
        ? renderRedirectPage(page.redirectTo, languages, template)
        : typeof page.content !== 'string'
            ? renderGenericPage(template)
            : page.hasSidebar
                ? renderSidebarPage(pf, frontmatter, html, languageCode, template)
                : renderContentPage(frontmatter, html, languageCode, template);

    // Replace "{{ t.some_identifier }}" with translated text.
    const out = insertTranslations(pf, renderedPage, i18n[languageCode], outputPath);

    try {
        mkdirSync(dirname(outputPath), { recursive: true });
        writeFileSync(outputPath, out, {});
    } catch (err) {
        throw Error(`${pf}Cannot write '${outputPath}':\n    ${err.message}`) }
};


/* --------------------------------- PRIVATE -------------------------------- */

const assemblePage = (template) => {
    return template; // TODO
}

const insertTranslations = (pf, template, t, outputPath) => {
    return template
        .split('{{t.')
        .map((part, i) => {
            if (i === 0) return part; // before the first "{{t."
            const identifierEndPos = part.indexOf('}}');
            if (identifierEndPos === -1) throw RangeError(
                `${pf}Cannot find end of "{{t." [${i}] for:\n    ${outputPath}`);
            const identifier = part.slice(0, identifierEndPos);
            const replacement = t[identifier];
            if (! replacement) throw RangeError(
                `${pf}Cannot find replacement for '${identifier}' [${i}] for:\n    ${outputPath}`);
            return replacement + part.slice(identifierEndPos + 2);
        })
        .join('');
}

const renderRedirectPage = (redirectTo, languages, template) => {
    return template
        .replace(/{{languageCode}}/g, 'en')
        .replace(/{{redirectTo}}/g, redirectTo)
        .replace('{{languages}}', languages.join())
    ;
}

const renderContentPage = (frontmatter, html, languageCode, template) => {
    return template
        .replace(/{{languageCode}}/g, languageCode)
        .replace(/{{title}}/g, // insert U+200A 'HAIR SPACE' between every character
            `LOOP.COOP / ${frontmatter.title}`.split('').join('\u200A'))
        .replace('{{content}}', html)
    ;
}

const renderGenericPage = (template) => {
    return template
        .replace(/{{languageCode}}/g, 'en')
    ;
}


/* ---------------------------------- TEST ---------------------------------- */

export const testBuildPage = (doesNotThrow, throws) => {
    const pf = 'buildPage(): ';
    const er = { name:'Error' };

    // TODO
}

