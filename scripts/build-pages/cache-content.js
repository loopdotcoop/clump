// scripts/build-pages/cache-content.js

import { ruffMarkdownToHtml } from './ruff-markdown-to-html.js';

/** ### Reads all of the Markdown content from file into memory
 *
 * Also checks that the .md files referred to in `config` exist, and are valid.
 *
 * @param   {Config}  config  Build configuration object
 * @param   {string}  contentDir  Path to the 'content/' folder
 * @param   {import('node:fs').readFileSync}  readFileSync  From 'node:fs'
 * @param   {import('node:path').resolve}  resolve  From 'node:path'
 * @throws  {Error}
 * @returns {object}
 */
export const cacheContent = (config, readFileSync, resolve, contentDir) => {
    const pf = 'cacheContent(): ';

    // Sometimes content names will be repeated in different places in `config`.
    // Making them property-names gets rid of duplications.
    const contentNames = {};
    config.versions.forEach((version, _i) => {
        Object.entries(version.languages).forEach(([langCode, routesObj]) =>
            Object.entries(routesObj).forEach(([_route, page]) =>
                contentNames[page.content.replace('{{languageCode}}', langCode)] = true
            )
        );
    });

    // Try to read each Markdown file, and roughly validate its syntax.
    return Object.keys(contentNames).reduce((cache, contentName) => ({
        ...cache,
        [contentName]: readValidateAndParseMarkdown(
            pf,
            readFileSync,
            resolve,
            contentDir,
            contentName,
        ),
    }), {});
};


/* --------------------------------- PRIVATE -------------------------------- */

const defaultFrontmatter = {
    title: 'Untitled',
};

const readValidateAndParseMarkdown = (pf, readFileSync, resolve, contentDir, contentName) => {
    const path = resolve(contentDir, contentName);

    // Try to read the Markdown file.
    let md; try {
        md = String(readFileSync(path));
    } catch (err) {
        throw Error(`${pf}Cannot read '${path}':\n    ${err.message}`);
    }
    const trimmed = md.trim() + '\n';
    if (trimmed.length === 1) throw Error(
        `${pf}'${path}' is empty`);

    // Do some basic validation of the frontmatter.
    if (trimmed.slice(0, 4) !== '---\n') throw Error(
        `${pf}'${path}' does not start "---" followed by a newline`);
    const frontmatterEndPos = trimmed.indexOf('\n---\n', 3);
    if (frontmatterEndPos === -1) throw Error(
        `${pf}'${path}' has no "---" surrounded by newlines to end the frontmatter`);
    if (frontmatterEndPos === 3) throw Error(
        `${pf}'${path}' has no frontmatter`);
    const frontmatterRaw = trimmed.slice(4, frontmatterEndPos).trim();
    if (frontmatterRaw.length === 0) throw Error(
        `${pf}'${path}' has empty frontmatter`);

    // Parse and further validate the frontmatter.
    const frontmatter = {
        ...defaultFrontmatter,
        ...Object.fromEntries(
            frontmatterRaw
                .split('\n')
                .map(line => line.trim())
                .filter(line => line.length !== 0)
                .map(line => (line.match(/^([a-z]+):\s*(.+)$/) || []).slice(1, 3))
                .filter(entry => entry.length === 2)
        )
    };
    validateFrontmatterObject(`${pf}${contentName} `, frontmatter);

    // Transform the Markdown to HTML.
    const html =
        `<!-- ${contentName} -->\n` +
        ruffMarkdownToHtml(
            trimmed.slice(frontmatterEndPos + 5).trim() + '\n\n'
        );

    return { frontmatter, html};
};

const validateFrontmatterObject = (pf, frontmatter) => {
    if (frontmatter.title.length < 3) throw RangeError(
        `${pf}frontmatter title is less than 3 characters`);
    // TODO more validations
}

/* ---------------------------------- TEST ---------------------------------- */

export const testCacheContent = (doesNotThrow, throws) => {
    const pf = 'cacheContent(): ';
    const er = { name:'Error' };

    // TODO
}

