// scripts/build-pages/ruff-markdown-to-html.js

/** ### Roughly transforms Markdown to HTML
 *
 * A cheap-n-cheerful Markdown renderer, which relies on the markdown conforming
 * to a particular happy path. For example, stick to ASCII characters.
 *
 * @param {string} markdown
 * @returns {string}
 */
export const ruffMarkdownToHtml = markdown =>
    ('\n'+markdown+'\n\n') // wrap in newline, to simplify logic
        .replace(/\n###### ([^\n]+)/g, (_, text) =>
            `\n<h6>${text}</h6>`)
        .replace(/\n##### ([^\n]+)/g, (_, text) =>
            `\n<h5>${text}</h5>`)
        .replace(/\n#### ([^\n]+)/g, (_, text) =>
            `\n<h4>${text}</h4>`)
        .replace(/\n### ([^\n]+)/g, (_, text) =>
            `\n<h3>${text}</h3>`)
        .replace(/\n## ([^\n]+)/g, (_, text) =>
            `\n<h2>${text}</h2>`)
        .replace(/\n# ([^\n]+)/g, (_, text) =>
            `\n<h1>${text}</h1>`)
        .replace(/\n---\n\n/g,
            `\n<hr />\n\n`)
        .replace(/`([^`\n]+)`/g, (_, code) =>
            `<code>${code}</code>`)
        .replace(/\b__([^_\n]+)__\b/g, (_, strong) =>
            `<strong>${strong}</strong>`)
        .replace(/\*\*([^*\n]+)\*\*/g, (_, strong) =>
            `<strong>${strong}</strong>`)
        .replace(/~~([^~\n]+)~~/g, (_, del) =>
            `<del>${del}</del>`)
        .replace(/\b_([^_\n]+)_\b/g, (_, emphasis) =>
            `<em>${emphasis}</em>`)
        .replace(/\*([^*\n]+)\*/g, (_, emphasis) =>
            `<em>${emphasis}</em>`)
        .replace(/<(https?:\/\/[^>]+)>/g, (_, url) =>
            `§a href="${url}"¶${url}§/a¶`)
        .replace(/\n```([a-z]*)\n([ -~\n]+)\n```\n\n/g, (_, syntax, pre) =>
            `\n<pre data-syntax="${syntax || 'plain'}">${pre.replace(/\n\n/g, '\n∞\n')}\n</pre>\n\n`)
        .replace(/\n(> [ -~\n]+?)\n\n/g, (_, blockquote) => // '?' for a non-greedy capture
            `\n<blockquote>${blockquoteMdToHtml(blockquote)}\n</blockquote>\n\n`)
        .replace(/\n(- [ -~\n]+?)\n\n/g, (_, ul) =>
            `\n<ul>${ulMdToHtml(ul)}\n</ul>\n\n`)
        .replace(/\n(1\. [ -~\n]+?)\n\n/g, (_, ol) =>
            `\n<ol>${olMdToHtml(ol)}\n</ol>\n\n`)
        .replace(/!\[([ -~]+)\]\(\s*([-?#:_./0-9a-z]+)\)/gi, (_, alt, url) =>
            `<img src="${url}" alt="${alt}" />`)
        .replace(/!\[([ -~]+)\]\(\s*([-?#:_./0-9a-z]+)\s+"([ -~]+)"\)/gi, (_, alt, url, title) =>
            `<img src="${url}" alt="${alt}" title="${title}" />`)
        .replace(/\[([ -~]+)\]\(\s*([-?#:_./0-9a-z]+)\)/gi, (_, text, url) =>
            `<a href="${url}">${text}</a>`)
        .replace(/\[([ -~]+)\]\(\s*([-?#:_./0-9a-z]+)\s+"([ -~]+)"\)/gi, (_, text, url, title) =>
            `<a href="${url}" title="${title}">${text}</a>`)
        .replace(/§/g, '<')
        .replace(/¶/g, '>')
        .split('\n')
        .reduce((acc, line, i, lines) => {
            const [prevOut, isInParagraph] = acc;
            if (isInParagraph) {
                if (line === '') {
                    prevOut[i-1] += '</p>';
                    // console.log('HERE!', i, lines, acc);
                    return [[...prevOut, ''], false]; // finish paragraph
                }
                return [[...prevOut, line], true]; // continue the paragraph
            }
            if (line !== '' && (i === 0 || lines[i-1] === '')) {
                if (line[0] !== ' ' && line[0] !== '\t' && line[0] !== '<')
                    return [[...prevOut, '<p>' + line], true]; // begin a paragraph
                if (line[0] === '<' && /^<(a |code|del|em|img |strong)/.test(line))
                    return [[...prevOut, '<p>' + line], true]; // begin a paragraph
            }
            return [[ ...prevOut, line], false]; // continue not being in a paragraph
        }, [[], false])[0]
        .join('\n')
        .replace(/∞/g, '') // prevented <p> being added in a <pre> with blank lines
        .slice(1, -2) // remove the newlines added at the start
;


/* --------------------------------- PRIVATE -------------------------------- */

const blockquoteMdToHtml = blockquoteMarkdown =>
    '\n  ' +
    blockquoteMarkdown
        .split('\n')
        .map(line => line.replace(/^>\s+/, '')) // remove leading ">" and space(s)
        .join('\n  ')
    ;

const olMdToHtml = orderedListMarkdown =>
    '\n  <li>' +
    orderedListMarkdown
        .split('\n')
        .map(li => li.replace(/^\d+\.\s+/, '')) // remove "1. " or "97. "
        .join('</li>\n  <li>') +
    '</li>';

const ulMdToHtml = unorderedListMarkdown =>
    '\n  <li>' +
    unorderedListMarkdown
        .split('\n')
        .map(li => li.replace(/^-\s+/, '')) // remove leading dash and space(s)
        .join('</li>\n  <li>') +
    '</li>';


/* ---------------------------------- TEST ---------------------------------- */

export const testRuffMarkdownToHtml = (equal) => {
    const fn = ruffMarkdownToHtml;
    
    // Test that headings work.
    equal(fn('###### abc'), '<h6>abc</h6>');
    equal(fn('##### abc'), '<h5>abc</h5>');
    equal(fn('#### abc'), '<h4>abc</h4>');
    equal(fn('### abc'), '<h3>abc</h3>');
    equal(fn('## abc'), '<h2>abc</h2>');
    equal(fn('# abc'), '<h1>abc</h1>');
    
    // Test that horizontal rules work.
    equal(fn('---\n\n'), '<hr />\n\n');
    
    // Test that monospace (code) works.
    equal(fn('`abc def`'), '<p><code>abc def</code></p>');
    equal(fn('No word boundary`abc`'), '<p>No word boundary<code>abc</code></p>');
    equal(fn('a`b`c`d`e'), '<p>a<code>b</code>c<code>d</code>e</p>');
    
    // Test that italic (emphasis) works.
    equal(fn('_abc def_'), '<p><em>abc def</em></p>');
    equal(fn('_abc_ before space'), '<p><em>abc</em> before space</p>');
    equal(fn('After space _abc_'), '<p>After space <em>abc</em></p>');
    equal(fn('No word boundary_abc_'), '<p>No word boundary_abc_</p>');
    equal(fn('_abc_nope'), '<p>_abc_nope</p>');
    equal(fn('*abc def*'), '<p><em>abc def</em></p>');
    equal(fn('*abc* before space'), '<p><em>abc</em> before space</p>');
    equal(fn('After space *abc*'), '<p>After space <em>abc</em></p>');
    equal(fn('No word boundary*abc*'), '<p>No word boundary<em>abc</em></p>');
    equal(fn('*abc*yep'), '<p><em>abc</em>yep</p>');
    
    // Test that bold (strong) works.
    equal(fn('__abc def__'), '<p><strong>abc def</strong></p>');
    equal(fn('__abc__ before space'), '<p><strong>abc</strong> before space</p>');
    equal(fn('After space __abc__'), '<p>After space <strong>abc</strong></p>');
    equal(fn('No word boundary__abc__'), '<p>No word boundary__abc__</p>');
    equal(fn('__abc__nope'), '<p>__abc__nope</p>');
    equal(fn('**abc def**'), '<p><strong>abc def</strong></p>');
    equal(fn('**abc** before space'), '<p><strong>abc</strong> before space</p>');
    equal(fn('After space **abc**'), '<p>After space <strong>abc</strong></p>');
    equal(fn('No word boundary**abc**'), '<p>No word boundary<strong>abc</strong></p>');
    equal(fn('**abc**yep'), '<p><strong>abc</strong>yep</p>');
    
    // Test that strikethrough (del) works.
    equal(fn('~~abc def~~'), '<p><del>abc def</del></p>');
    equal(fn('~~abc~~ before space'), '<p><del>abc</del> before space</p>');
    equal(fn('After space ~~abc~~'), '<p>After space <del>abc</del></p>');
    equal(fn('No word boundary~~abc~~'), '<p>No word boundary<del>abc</del></p>');
    equal(fn('~~abc~~yep'), '<p><del>abc</del>yep</p>');
    
    // Test that code blocks work.
    equal(fn('```\nOk\n```\n\n'),
        '<pre data-syntax="plain">Ok\n</pre>\n\n');
    equal(fn('```js\nlet a = "ok";\nconsole.log(a);\n```\n\n'),
        '<pre data-syntax="js">let a = "ok";\nconsole.log(a);\n</pre>\n\n');
    
    // Test that blockquotes and lists work.
    equal(fn('> abc'),
        '<blockquote>\n  abc\n</blockquote>');
    equal(fn('> one\n> two\n> three'),
        '<blockquote>\n  one\n  two\n  three\n</blockquote>');
    equal(fn('- abc'),
        '<ul>\n  <li>abc</li>\n</ul>');
    equal(fn('- one\n- two\n- three'),
        '<ul>\n  <li>one</li>\n  <li>two</li>\n  <li>three</li>\n</ul>');
    equal(fn('- one\n- two\n- three\n'),
        '<ul>\n  <li>one</li>\n  <li>two</li>\n  <li>three</li>\n</ul>\n');
    equal(fn('1. abc'),
        '<ol>\n  <li>abc</li>\n</ol>');
    equal(fn('1. one\n99. two\n3. three'),
        '<ol>\n  <li>one</li>\n  <li>two</li>\n  <li>three</li>\n</ol>');
    
    // Test that links work.
    equal(fn('<http://example.com/>'),
        '<p><a href="http://example.com/">http://example.com/</a></p>');
    equal(fn('<https://foo.co/a/b/c.html>'),
        '<p><a href="https://foo.co/a/b/c.html">https://foo.co/a/b/c.html</a></p>');
    equal(fn('[Text](http://example.com/)'),
        '<p><a href="http://example.com/">Text</a></p>');
    equal(fn('[More text](https://foo.co/a/b/c.html)'),
        '<p><a href="https://foo.co/a/b/c.html">More text</a></p>');
    equal(fn('[Text](\nhttp://example.com/ "Title here")'),
        '<p><a href="http://example.com/" title="Title here">Text</a></p>');
    equal(fn('[More text](\n    https://foo.co/a/b/c.html " ")'),
        '<p><a href="https://foo.co/a/b/c.html" title=" ">More text</a></p>');
    
    // Test that images work.
    equal(fn('![Text](some/local/image.png)'),
        '<p><img src="some/local/image.png" alt="Text" /></p>');
    equal(fn('![More alt text](https://foo.co/a/b/c.jpeg)'),
        '<p><img src="https://foo.co/a/b/c.jpeg" alt="More alt text" /></p>');
    equal(fn('![Text](some/local/image.png ".")'),
        '<p><img src="some/local/image.png" alt="Text" title="." /></p>');
    equal(fn('![More alt text](https://foo.co/a/b/c.jpeg "Title here")'),
        '<p><img src="https://foo.co/a/b/c.jpeg" alt="More alt text" title="Title here" /></p>');

    // Test that paragraphs work.
    equal(fn('A'),
        '<p>A</p>');
    equal(fn('A\n\n'),
        '<p>A</p>\n\n');
    equal(fn('\n\n\nSome <b>multiline</b> text\nin here.\n\nAnd more\nhere.\n\n'),
        '\n\n\n<p>Some <b>multiline</b> text\nin here.</p>\n\n<p>And more\nhere.</p>\n\n');

    // Test that a paragraph is not inserted inside a blockquote which has an
    // empty line.
    equal(fn('```\nOne\n\nThree\n```'),
        '<pre data-syntax="plain">One\n\nThree\n</pre>');

    // Test that a paragraph is not inserted inside an unordered list.
    equal(fn('- Lorum ipsum\n- Sit `delor et wisi`\n- Nonummy\n'),
        '<ul>\n' +
        '  <li>Lorum ipsum</li>\n' +
        '  <li>Sit <code>delor et wisi</code></li>\n' +
        '  <li>Nonummy</li>\n' +
        '</ul>\n');

    // TODO test common and unusual combinations
    
}