// scripts/build-pages/validate-config.js

import { isPlainObjOrInst } from '../utilities/is-type.js';

/** ### Checks that the build `config` object appears correct
 *
 * @TODO validate i18n
 *
 * @param   {Config}  config  Build configuration object
 * @throws  {RangeError}
 * @throws  {TypeError}
 */
export const validateConfig = (config) => {
    const pf = 'validateConfig(): ';
    if (! isPlainObjOrInst(config)) throw TypeError(
        pf+'config is not a plain object or instance');
    if (! isPlainObjOrInst(config.landingPage)) throw TypeError(
        pf+'config.landingPage is not a plain object or instance');
    if (! isPlainObjOrInst(config.notFound)) throw TypeError(
        pf+'config.notFound is not a plain object or instance');
    if (! Array.isArray(config.versions)) throw TypeError(
        pf+'config.versions is not an array');

    // Validate the landing and not-found pages.
    validatePageDescriptor(pf+'config.landingPage', config.landingPage);
    validatePageDescriptor(pf+'config.notFound', config.notFound);

    // Validate the versions.
    config.versions.forEach((version, i) => {
        if (! isPlainObjOrInst(version)) throw TypeError(
            `${pf}config.versions[${i}] is not a plain object or instance`);
        validatePageDescriptor(`${pf}config.versions[${i}]`, version);
        if (! isPlainObjOrInst(version.languages)) throw TypeError(
            `${pf}config.versions[${i}].languages is not a plain object or instance`);
        Object.entries(version.languages).forEach(([langCode, routes]) => {
            if (! /^[a-z][a-z]$/.test(langCode)) throw RangeError(
                `${pf}config.versions[${i}].languages.${langCode} fails /^[a-z][a-z]$/`);
            validateRoutesObject(`${pf}config.versions[${i}].languages.${langCode}`, routes);
        })
    });

};


/* --------------------------------- PRIVATE -------------------------------- */

const defaultRules = {
    content: 'NOT_ALLOWED',
}

const validatePageDescriptor = (pf, page, rules=defaultRules) => {

    // TODO test validation of i18n

    // Every page-descriptor must specify a `template`.
    if (! isPlainObjOrInst(page)) throw TypeError(
        `${pf} is type '${typeof page}' not a plain object or instance`);
    if (typeof page.template !== 'string') throw TypeError(
        `${pf}.template is type '${typeof page.template}' not 'string'`);
    if (page.template.slice(-5) !== '.html') throw RangeError(
        `${pf}.template '${page.template}' does not end '.html'`);

    // If the `template` is "redirect.html" or "redirect-to-language.html", then
    // there must be a `redirectsTo` value. If not, the `redirectsTo` property
    // is not allowed.
    if (page.template.slice(0, 8) === 'redirect') {
        if (typeof page.redirectTo !== 'string') throw TypeError(
            `${pf}.redirectTo is type '${typeof page.redirectTo}' not 'string'`);
        if (page.redirectTo.slice(-5) !== '.html') throw RangeError(
            `${pf}.redirectTo '${page.redirectTo}' does not end '.html'`);
    } else {
        if (typeof page.redirectTo !== 'undefined') throw TypeError(
            `${pf}.redirectTo is type '${typeof page.redirectTo}' not 'undefined'`);
    }

    // Some page-descriptors must have a `content` property, and others must not.
    if (rules.content === 'MANDATORY') {
        if (typeof page.content !== 'string') throw TypeError(
            `${pf}.content is type '${typeof page.content}' not 'string'`);
        if (page.content.slice(-3) !== '.md') throw RangeError(
            `${pf}.content '${page.content}' does not end '.md'`);
    } else if (rules.content === 'NOT_ALLOWED') {
        if (typeof page.content !== 'undefined') throw TypeError(
            `${pf}.content is type '${typeof page.content}' not 'undefined'`);
    } else {
        throw RangeError(`${pf} rules.content '${rules.content}' not recognised`) }
};

const validateRoutesObject = (pf, routes) => {
    Object.entries(routes).forEach(([routeName, page]) => {
        if (routeName.slice(-5) !== '.html') throw RangeError(
            `${pf}['${routeName}'] does not end '.html'`);
            validatePageDescriptor(`${pf}['${routeName}']`, page, {
                content: 'MANDATORY', // all routes must have a content .md file
            });
    })
};

/* ---------------------------------- TEST ---------------------------------- */

export const testValidateConfig = (doesNotThrow, throws) => {
    const pf = 'validateConfig(): ';
    const re = { name:'RangeError' };
    const te = { name:'TypeError' };

    // Test validateConfig() where `config` is an empty object, or not an object.
    throws(() => validateConfig(),
        { ...te, message:pf+'config is not a plain object or instance' });
    throws(() => validateConfig(123),
        { ...te, message:pf+'config is not a plain object or instance' });

    // Test validateConfig() where `config` has missing properties.
    const config = {};
    throws(() => validateConfig(config),
        { ...te, message:pf+'config.landingPage is not a plain object or instance' });
    config.landingPage = {};
    throws(() => validateConfig(config),
        { ...te, message:pf+'config.notFound is not a plain object or instance' });
    config.notFound = {};
    throws(() => validateConfig(config),
        { ...te, message:pf+'config.versions is not an array' });

    // Test validateConfig() where `config.landingPage` has invalid properties.
    config.versions = [];
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.landingPage.template is type 'undefined' not 'string'" });
    config.landingPage.template = '';
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.landingPage.template '' does not end '.html'" });
    config.landingPage.template = 'redirect.html';
    config.landingPage.redirectTo = [];
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.landingPage.redirectTo is type 'object' not 'string'" });
    config.landingPage.redirectTo = 'nope.txt';
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.landingPage.redirectTo 'nope.txt' does not end '.html'" });
    config.landingPage.redirectTo = '/some/abs/path.html';
    config.landingPage.content = 'nope.md';
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.landingPage.content is type 'string' not 'undefined'" });
    delete config.landingPage.content;

    // Test validateConfig() where `config.notFound` has invalid properties.
    config.notFound.template = 'nope';
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.notFound.template 'nope' does not end '.html'" });
    config.notFound.template = '404.html';
    config.notFound.redirectTo = '/nope.html';
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.notFound.redirectTo is type 'string' not 'undefined'" });
    delete config.notFound.redirectTo;
    config.notFound.content = 'nope.md';
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.notFound.content is type 'string' not 'undefined'" });
    delete config.notFound.content;

    // Test validateConfig() where `config.versions` has invalid items.
    config.versions = [{ template:'a.html', languages:{} },
        { template:'b.html', languages:{} }, null, { template:'d.html', languages:{} }];
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2] is not a plain object or instance" });
    config.versions[2] = { nope:'c.html' };
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].template is type 'undefined' not 'string'" });
    config.versions[2] = { template:'c.htm' };
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.versions[2].template 'c.htm' does not end '.html'" });
    config.versions[2] = { template:'c.html' };
    config.versions[2].content = 'nope.md';
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].content is type 'string' not 'undefined'" });
    delete config.versions[2].content;
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].languages is not a plain object or instance" });
    config.versions[2].languages = { en:{ 'route.html': 'nope' } };
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].languages.en['route.html'] is type 'string' not a plain object or instance" });
    config.versions[2].languages = { en:{ 'route.html': {} } };
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].languages.en['route.html'].template is type 'undefined' not 'string'" });
    config.versions[2].languages.en['route.html'].template = 'tmpt.html';
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].languages.en['route.html'].content is type 'undefined' not 'string'" });
    config.versions[2].languages.en['route.html'].content = 'nope.txt';
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.versions[2].languages.en['route.html'].content 'nope.txt' does not end '.md'" });
    config.versions[2].languages.en['route.html'].content = 'ok.md';
    config.versions[2].languages.PT = {};
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.versions[2].languages.PT fails /^[a-z][a-z]$/" });
    delete config.versions[2].languages.PT;
    config.versions[2].languages.pt = { 'nope': {} };
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.versions[2].languages.pt['nope'] does not end '.html'" });
    config.versions[2].languages.pt = { 'route.html': {} };
    throws(() => validateConfig(config),
        { ...te, message:pf+"config.versions[2].languages.pt['route.html'].template is type 'undefined' not 'string'" });
    config.versions[2].languages.pt['route.html'].template = 'TMPT.HTML';
    throws(() => validateConfig(config),
        { ...re, message:pf+"config.versions[2].languages.pt['route.html'].template 'TMPT.HTML' does not end '.html'" });

    // Test validateConfig() with a valid `config` argument.
    config.versions[2].languages.pt['route.html'].template = 'tmpt.html';
    config.versions[2].languages.pt['route.html'].content = 'ok-{{languageCode}}.md';
    doesNotThrow(() => validateConfig(config));

}
