// scripts/run-scripts-internal-tests.js

import { green, red } from './utilities/ansi.js';
import { testBuildPages } from './build-pages/test-build-pages.js';
import { testDev } from './dev/test-dev.js';
import { testUtilities } from './utilities/test-utilities.js';

try {
    console.log('-'.repeat(78));
    testBuildPages();
    testDev();
    testUtilities();
    console.log('='.repeat(78));
    console.log(`${green('PASS')}All 3 'scripts' internal test suites passed`);
    console.log('='.repeat(78));
    process.exit(0);
} catch (err) {
    console.log('='.repeat(78));
    console.error(`${red('FAIL')}A 'scripts' internal test suite failed`);
    console.log('='.repeat(78));
    process.exit(1);
}
