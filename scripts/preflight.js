// scripts/preflight.js

import { readdirSync } from 'node:fs';
import { findTrailingWhitespace } from './preflight/find-trailing-whitespace.js';
import { validateErrorCodes } from './preflight/validate-error-codes.js';
import { validateLine1Paths } from './preflight/validate-line-1-paths.js';

// Initialise an object which will be used for caching file content.
const cache = {};

// Get flat lists of all files and folders in scripts/ and src/
const readdirOptions = { withFileTypes: true, recursive: true };
const itemsInScripts = readdirSync('scripts', readdirOptions);
const itemsInSrc = readdirSync('src', readdirOptions);

// Search the scripts/ and src/ folders for files with:
// - Lines which have trailing whitespace
// - Invalid first-lines
findTrailingWhitespace(cache, itemsInScripts, itemsInSrc);
validateLine1Paths(cache, itemsInScripts, itemsInSrc);

// Search just the src/ folder for files with:
// - Problems related to error-codes.
validateErrorCodes(cache, itemsInSrc);

console.log('\nTODO next finish preflight.js');
