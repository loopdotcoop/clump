// scripts/build-pages.js

import { existsSync, mkdirSync, readFileSync, renameSync, rmSync, writeFileSync } from 'node:fs';
import { dirname, resolve } from 'node:path';

import { config } from '../src/pages/config.js';
import { buildPage } from './build-pages/build-page.js';
import { cacheContent } from './build-pages/cache-content.js';
import { cacheTemplates } from './build-pages/cache-templates.js';
import { validateConfig } from './build-pages/validate-config.js';
import { green, red } from './utilities/ansi.js';

export const buildPages = (config, pwd) => {
    // Delete the temporary output folder, if it still exists after a previous
    // failed run of buildPages().
    const tmpDir = resolve(pwd, 'public-tmp');
    if (existsSync(tmpDir)) rmSync(tmpDir, { recursive:true, force:true });

    // Validate the configuration object (throws an exception if invalid).
    validateConfig(config);

    // Read all of the content from .md files into memory. This also checks
    // that the content referred to in `config` exists, and is valid.
    const contentDir = resolve(pwd, 'src/pages/content');
    const content = cacheContent(config, readFileSync, resolve, contentDir);

    // Read all of the templates from .html files into memory. This also checks
    // that the templates referred to in `config` exist, and are valid.
    const templatesDir = resolve(pwd, 'src/pages/templates');
    const templates = cacheTemplates(config, dirname, readFileSync, resolve, templatesDir);

    // Create a temporary folder for the generated files and folders.
    mkdirSync(tmpDir);

    // Generate each page, and write it to the temporary folder.
    const languageCode = 'en';
    buildPage(config.landingPage, null, null, config.landingPage.i18n, languageCode, templates, dirname, mkdirSync, writeFileSync,
        resolve(tmpDir, 'index.html'));
    buildPage(config.notFound, null, null, config.notFound.i18n, languageCode, templates, dirname, mkdirSync, writeFileSync,
        resolve(tmpDir, '404.html'));
    config.versions.forEach((version, i) => {
        buildPage(version, null, null, version.i18n, languageCode, templates, dirname, mkdirSync, writeFileSync,
            resolve(tmpDir, `v${i}/index.html`));
        Object.entries(version.languages).forEach(([langCode, routes]) => {
            Object.entries(routes).forEach(([routeName, page]) => {
                const { frontmatter, html } =
                    content[page.content.replace('{{languageCode}}', langCode)];
                buildPage(page, frontmatter, html, version.i18n, langCode, templates, dirname, mkdirSync, writeFileSync,
                    resolve(tmpDir, `v${i}/${langCode}/${routeName}`));
            });
        });
    });

    // Delete the public output folder, if it still exists after a previous run
    // of buildPages(). Then rename the temporary folder to be the new output
    // folder.
    const outputDir = resolve(pwd, 'public');
    if (existsSync(outputDir)) rmSync(outputDir, { recursive:true, force:true });
    renameSync(tmpDir, outputDir);

};

// Run the build.
try {
    buildPages(config, './');
} catch (err) {
    const place = err.stack?.split('\n') // split into lines
        .find(line => /^\s+at /.test(line)) // the first line starting "    at "
        .split('/').pop() // text after the last "/", eg "some-file.js:344:5)"
        || 'unknown location'; // fallback
    console.error(`${red('ERROR')}${err.message}\n(${place})\n`);
    const tmpDir = resolve('./', 'public-tmp');
    if (existsSync(tmpDir)) rmSync(tmpDir, { recursive:true, force:true });
    process.exit(1);
}

console.info(`${green('OK')}All pages were successfully built`);
