// scripts/build-rs.js

import { readFileSync } from 'node:fs';
import { join } from 'node:path';
import { argv } from 'node:process';
import { buildCliBinary } from './build-rs/build-cli-binary.js';
import { buildNodeWasm } from './build-rs/build-node-wasm.js';
import { buildWebBrowserWasm } from './build-rs/build-web-browser-wasm.js';
import { green } from './utilities/ansi.js';

// Creates the CLI binary and WebAssembly builds of one or more Rust app.
const buildRs = async rsNames => {

    for (const rsName of rsNames) {
        console.log('-'.repeat(78));
        console.log(`Running buildCliBinary('${rsName}')...`);
        await buildCliBinary(rsName);
        console.log(`\nRunning buildNodeWasm('${rsName}')...`);
        await buildNodeWasm(rsName);
        console.log(`Running buildWebBrowserWasm('${rsName}')...`);
        await buildWebBrowserWasm(rsName);
        console.log(`${green('OK')}All three '${rsName}' builds succeeded`);
    }

    const len = rsNames.length;
    console.log('='.repeat(78));
    console.log(`${green('Done!')}${len*3} builds for ${len} Rust app${len==1?'':'s'}`);
    console.log('='.repeat(78));
}

// Build the specified Rust apps. If none are specified, build all Rust apps.
//
// These two invocations will populate `argv[2]`:
//     node scripts/build-rs.js some_rust_app,another
//     npm run build:rs -- some_rust_app,another
// This won't:
//     npm run build -- nope
const rsNames = argv[2]
    ? argv[2].split(',') // TODO something like chooseCrateNamesFromArgs()
    : readRsAppNamesFromVsCodeSettings(); // eg ['hello_rust', 'another']
buildRs(rsNames);


/* --------------------------------- PRIVATE -------------------------------- */

// Reads and parses the .vscode/setting.json file from the top level of the repo.
function readRsAppNamesFromVsCodeSettings() { // TODO move to utilities
    const pf = 'readRsAppNamesFromVsCodeSettings(): ';

    const linkRx = /^\.\/src\/(rs-apps|rs-libs)\/([a-z][_0-9a-z]{0,23})\/Cargo\.toml$/;

    // Read and parse the .vscode/settings.json file.
    let linkedProjects; try {
        const raw = readFileSync(join('.vscode', 'settings.json'));
        const parsed = JSON.parse(raw.toString());
        linkedProjects = parsed['rust-analyzer.linkedProjects'];
        if (! Array.isArray(linkedProjects)) throw Error(
            'It has no "rust-analyzer.linkedProjects" array');
        linkedProjects.forEach((lp, i) => {
            if (typeof lp !== 'string') throw Error(
                `linkedProjects[${i}] is type '${typeof lp}' not 'string'`);
            if (! linkRx.test(lp)) throw Error(
                `linkedProjects[${i}] fails ${linkRx}`);
        });
    } catch(err) { throw Error(
        pf + 'Cannot read or parse .vscode/settings.json\n    ' + err.message) }

    // Get an array containing the names of the apps in the src/rs-apps/ folder.
    const rsAppNames = linkedProjects
        .map(lp => lp.match(linkRx))
        .filter(match => match[1] === 'rs-apps')
        .map(match => match[2])
    ;
    return rsAppNames;
}
