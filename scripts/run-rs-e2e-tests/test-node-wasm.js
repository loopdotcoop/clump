// scripts/run-rs-e2e-tests/test-node-wasm.js

import { equal } from 'node:assert';
import { argv } from 'node:process';
import { green, red } from '../utilities/ansi.js';

export const testNodeWasm = async (rsName, beVerbose=false) => {
    const pf = 'testNodeWasm(): ';

    // Validate the Rust app's name.
    if (typeof rsName !== 'string') throw TypeError(
        `${pf}rsName is type '${typeof rsName}' not 'string'`);
    if (! /^[a-z][_0-9a-z]{0,23}$/.test(rsName)) throw TypeError(
        `${pf}rsName '${rsName}' fails /^[a-z][_0-9a-z]{0,23}$/`);

    // Read the e2e node-wasm tests from the Rust app's crate, and run them.
    try {
        const { testE2eNodeWasm } = await import(
            `./../../src/rs-apps/${rsName}/tests/e2e/node-wasm.js`);
        const wasm = await import(
            `../../dist/${rsName}/node-wasm/${rsName}.cjs`);
        const results = testE2eNodeWasm({ equal }, wasm);
        const failTally = results.filter(result => result.err).length;

        // If any tests fail, show those test results verbosely and then exit.
        // Also, if the --verbose flag was set, show passing test results.
        if (failTally || beVerbose) {
            results.forEach(({ actually, err, expected, kind }, i) => {
                if (err) {
                    const location = (err.stack?.split('\n') // split into lines
                        .find(line => /^\s+at /.test(line)) // the first line starting "    at "
                        .split('/').slice(-4).join('/') // text after 4th-from-last "/", eg "dir/some-file.js:344:5)"
                            || 'unknown location)') // fallback
                        .slice(0, -1) // remove the trailing ")"
                    console.error(
                        `${red('FAIL')}'${rsName}' e2e node-wasm test ${i+1}:\n` +
                        `    Actually: ${formatActually(actually)}\n` +
                        `    Expected: ${formatExpected(expected)}\n` +
                        `    Location: ${kind}() at ${location}\n` +
                        `    Message:  ${err.message}\n`);
                } else if (beVerbose) {
                    console.log('TODO');
                }
            });

            if (failTally) throw Error(
                `${failTally} of ${results.length} '${rsName}' e2e node-wasm tests failed`);
        }

        // Show a 'PASS' or 'FAIL' message.
        switch (results.length) {
            case 0:
                console.log(`${green('PASS')}'${rsName}' has no e2e node-wasm tests`);
                break;
            case 1:
                console.log(`${green('PASS')}The '${rsName}' e2e node-wasm test passed`);
                break;
            case 2:
                console.log(`${green('PASS')}Both '${rsName}' e2e node-wasm tests passed`);
                break;
            default:
                console.log(`${green('PASS')}All ${results.length} '${rsName}' e2e node-wasm tests passed`);
        }
    } catch (err) { console.error(red('FAIL') + '\n', err); process.exit(1) }

};

const filename = import.meta.url.split('/').slice(-1)[0]; // 'test-node-wasm.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;
if (isDirectCall) { testNodeWasm(argv[2]) }


/* --------------------------------- PRIVATE -------------------------------- */

function formatActually(actually) {
    if (typeof actually === 'undefined') return '-= undefined =-';
    return formatActuallyOrExpected(actually);
}

function formatExpected(expected) {
    if (typeof expected === 'undefined') return '-= undefined =-';
    return formatActuallyOrExpected(expected);
}

function formatActuallyOrExpected(actuallyOrExpected) {
    if (! actuallyOrExpected.includes('\n')) return actuallyOrExpected;
    return '\n        ' + actuallyOrExpected.split('\n').join('\n        ');
}
