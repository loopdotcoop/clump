// scripts/run-rs-e2e-tests/test-web-browser-wasm.js

import { readFileSync } from 'node:fs';
import { createServer } from 'node:http';
import { join } from 'node:path';
import { argv } from 'node:process';
import { blue, red } from '../utilities/ansi.js';

export const testWebBrowserWasm = async rsNames => {
    const pf = 'testWebBrowserWasm(): ';

    // Validate each Rust app name.
    if (! Array.isArray(rsNames)) throw TypeError(
        `${pf}rsName is type '${typeof rsNames}' not an array`);
    rsNames.forEach((rsName, i) => {
        if (typeof rsName !== 'string') throw TypeError(
            `${pf}rsNames[${i}] is type '${typeof rsName}' not 'string'`);
        if (! /^[a-z][_0-9a-z]{0,23}$/.test(rsName)) throw TypeError(
            `${pf}rsNames[${i}] '${rsName}' fails /^[a-z][_0-9a-z]{0,23}$/`);
    });

    // Convert the .wasm files to Data URL strings. These will be used to test
    // an alternative way of loading WebAssembly into JavaScript.
    const dataUrls = rsNames.reduce((dus, rsName) => {
        const wasm = readFileSync(
            `./dist/${rsName}/web-browser-wasm/${rsName}_bg.wasm`);
        const encoded = Buffer.from(wasm, 'binary').toString('base64');
        const chunks = encoded.split('').reduce((acc, char, i) => i % 1000
            ? (acc[acc.length-1] = acc.at(-1)+char) && acc : [...acc, [char]], []);
        dus[rsName] = `data:application/wasm;base64,\n${chunks.join('\n')}`;
        return dus;
    }, {});

    try {
        createServer((req, res) => {
            // Serve the homepage, which shows a list of links.
            const { url } = req;
            if (url === 'index.html' || url === '/')
            return respond(res, 200, 'text/html', renderHomepage(rsNames));

            // From "/my_rust_app.html?query", get "my_rust_app" and "html".
            const filenameAndQuery = url.split('/').pop();
            const filename = filenameAndQuery.split('?')[0]; // discard the query
            const [ basename, ext] = filename.split('.');

            // Serve any other content, or fall back to a "Not Found" message.
            switch (ext) {
                case 'html':
                    return respond(res, 200, 'text/html',
                        renderTestPage(dataUrls, basename));
                case 'wasm':
                    return respond(res, 200, 'application/wasm',
                        fromDist(filename, basename.replace('_bg', '')));
                case 'js':
                    if (filename.slice(-10) === '-common.js') {
                        return respond(res, 200, 'text/javascript',
                            fromSrcCommon(filename.slice(0, -10)));
                    } else if (filename.includes('-web-browser-wasm')) {
                        return respond(res, 200, 'text/javascript',
                            fromSrcWBW(basename.replace('-web-browser-wasm', '')));
                    } else {
                        return respond(res, 200, 'text/javascript',
                            fromDist(filename, basename));
                    }
                default:
                    return respond(res, 200, 'text/plain', 'Not Found');
            }
        }).listen(1234);
        console.log(blue('Ready!')
            + '\nOpen http://localhost:1234/ to run web browser wasm tests.'
            + '\nPress ctrl-c to close this server.');
    } catch (err) { console.error(red('Error!') + '\n', err); process.exit(1) }

};

// Determine whether this file has been invoked directly using:
//   node scripts/run-rs-e2e-tests/test-web-browser-wasm.js my_rust_app,another
// ...or indirectly, eg via an NPM package.json script:
//   npm run test:e2e:rs
const filename = import.meta.url.split('/').slice(-1)[0]; // 'test-web-browser-wasm.js'
const isDirectCall = argv[1].slice(-filename.length) === filename;

// Test the specified browser .wasm files if invoked directly, or else the
// default .wasm files.
const rsNames = isDirectCall && argv[2]
    ? argv[2].split(',')
    : readRsAppNamesFromVsCodeSettings(); // eg ['hello_rust', 'another']
testWebBrowserWasm(rsNames);


/* --------------------------------- PRIVATE -------------------------------- */

// Reads and parses the .vscode/setting.json file from the top level of the repo.
function readRsAppNamesFromVsCodeSettings() { // TODO move to utilities
    const pf = 'readRsAppNamesFromVsCodeSettings(): ';

    const linkRx = /^\.\/src\/(rs-apps|rs-libs)\/([a-z][_0-9a-z]{0,23})\/Cargo\.toml$/;

    // Read and parse the .vscode/settings.json file.
    let linkedProjects; try {
        const raw = readFileSync(join('.vscode', 'settings.json'));
        const parsed = JSON.parse(raw.toString());
        linkedProjects = parsed['rust-analyzer.linkedProjects'];
        if (! Array.isArray(linkedProjects)) throw Error(
            'It has no "rust-analyzer.linkedProjects" array');
        linkedProjects.forEach((lp, i) => {
            if (typeof lp !== 'string') throw Error(
                `linkedProjects[${i}] is type '${typeof lp}' not 'string'`);
            if (! linkRx.test(lp)) throw Error(
                `linkedProjects[${i}] fails ${linkRx}`);
        });
    } catch(err) { throw Error(
        pf + 'Cannot read or parse .vscode/settings.json\n    ' + err.message) }

    // Get an array containing the names of the apps in the src/rs-apps/ folder.
    const rsAppNames = linkedProjects
        .map(lp => lp.match(linkRx))
        .filter(match => match[1] === 'rs-apps')
        .map(match => match[2])
    ;
    return rsAppNames;
}

function renderHomepage(rsNames) { return `
<!-- /index.html -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Test web browser .wasm files</title>
        <style>
            body { background:#111; color:#ccc;
                font: 18px/30px Arial, sans-serif;
                transition: background-color 1s, color 1s }
            a, a:visited { color: #3ff }
        </style>
    </head>
    <body>
        <h1>Test web browser .wasm files</h1>
        <ul>
            ${rsNames
    .map(wn => `<li><a href="./${wn}.html">${wn}</a></li>`)
    .join('\n            ')}
        </ul>
    </body>
</html>
`}

function renderTestPage(dataUrls, rsName) {
    const dataUrl = dataUrls[rsName];
    // const testJs = testJses[rsName];

    return `
<!-- /${rsName}.html -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Test ${rsName} web browser .wasm</title>
        <style>
            body { background: #222; color: #ccc;
                font: 18px/30px Arial, sans-serif;
                transition: background-color 1s, color 1s }
            h4 { margin: 0 0 -1rem 0 }
            pre { margin: 0 0 4rem 0 }
            a, a:visited { color: #3ff }
            .green, .red { display: inline-block; padding: 0 .5em; color: #eee }
            .green { background: #161 }
            .red { background: #911 }
            h2 code { padding: .3rem .5rem; border-radius: .3rem; background: #000;
                color: #808080; transition: color 1s }

            /* If the Standard or DataUrl test fails, switch to red. */
            body.standard-fail, body.dataurl-fail { background: #211; color: #fcc }
            body.standard-fail h2 code, body.dataurl-fail h2 code { color: #f66 }

            /* If the Standard and DataUrl tests both pass, switch to green. */
            body.standard-pass.dataurl-pass { background: #121; color: #cfc }
            body.standard-pass.dataurl-pass h2 code { color: #3c3 }
        </style>
    </head>
    <body>
        <a href="/">Home</a>
        <h2>Test <code>${rsName}</code> web browser .wasm</h2>

        <p>'dist/${rsName}/web-browser-wasm/${rsName}_bg.wasm' is
        ${dataUrl.length} bytes as a data URL.</p>

        <h4>Standard:</h4>
        <pre id="results-standard"></pre>
        <h4>DataUrl:</h4>
        <pre id="results-dataurl"></pre>

        <script>!function(){
const beVerbose = window.location.hash === '#beVerbose';

const equal = (actually, expected) => {
    if (actually !== expected) throw Error('Not equal!') };

const formatActually = actually => {
    if (typeof actually === 'undefined') return '-= undefined =-';
    return formatActuallyOrExpected(actually);
}

const formatExpected = expected => {
    if (typeof expected === 'undefined') return '-= undefined =-';
    return formatActuallyOrExpected(expected);
}

const formatActuallyOrExpected = actuallyOrExpected => {
    if (! actuallyOrExpected.includes('\\n')) return actuallyOrExpected;
    return '\\n        ' + actuallyOrExpected.split('\\n').join('\\n        ') };

const getLocation = err => // TODO fix in Safari (not straightforward)
    err.stack?.split('\\n') // split into lines
        .map(line => line.slice(-1) === ')' ? line.slice(0, -1) : line) // remove possible trailing bracket
        .find(line => /\\.js:\\d+:\\d+?$/.test(line)) // the first line ending ".js:123:45"
        .split('/').pop() // text after last "/", eg "hello_rust-some-file.js:123:45"
        .replace('common.js', '${rsName}-common.js') // eg "hello_rust/tests/e2e/hello_rust-common.js.js:123:45"
        .replace('-', '/tests/e2e/') // eg "hello_rust/tests/e2e/some-file.js:123:45"
            || 'unknown location'; // fallback

const green = msg => \`<b class="green">\${msg}</b> \`;

const red = msg => \`<b class="red">\${msg}</b> \`;

window.CLUMP_HELPERS = {

    runTests: (
        log,
        standardOrDataurl,
        testE2eWebBrowserWasm,
        wasm,
    ) => {

        // Run the e2e tests, and count the number of 'FAIL' results.
        const results = testE2eWebBrowserWasm({ equal }, wasm);
        const fails = results.filter(result => result.err);
        const failTally = fails.length;

        // Switch to a red theme if any tests failed, or otherwise a green theme.
        document.body.classList.add(standardOrDataurl + (failTally ? '-fail' : '-pass'));

        // If any tests fail, show those test results verbosely and then exit.
        // Also, if the #verbose URL hash was set, show passing test results.
        if (failTally || beVerbose) {
            results.forEach(({ actually, err, expected, kind }, i) => {
                if (err) {
                    log(\`\${red('FAIL')}'${rsName}' e2e node-wasm test \${i+1}:\\n\` +
                        \`    Actually: \${formatActually(actually)}\\n\` +
                        \`    Expected: \${formatExpected(expected)}\\n\` +
                        \`    Location: \${kind}() at \${getLocation(err)}\\n\` +
                        (actually && expected ? '' : \`    Message:  \${err.message}\\n\`)
                    );
                } else if (beVerbose) {
                    log('TODO');
                }
            });

            if (failTally) throw Error(
                \`\${failTally} of \${results.length} '${rsName}' e2e web-browser-wasm tests failed\`);
        }

        switch (results.length) {
            case 0: return log(
                green('PASS')+"'${rsName}' has no e2e web-browser-wasm tests");
            case 1: return log(
                green('PASS')+"The '${rsName}' e2e web-browser-wasm test passed");
            case 2: return log(
                green('PASS')+"Both '${rsName}' e2e web-browser-wasm tests passed");
            default: return log(
                green('PASS')+"All " + results.length + " '${rsName}' e2e web-browser-wasm tests passed");
        }
    },

}
        }()</script>

        <script type="module">(async function() {
const $log = document.getElementById('results-standard');
const log = msg => $log.innerHTML += '\\n' + msg;

try {

    // Load the WebAssembly code from file, in the usual way.
    const wasm = await import('./${rsName}.js');
    await wasm.default('./${rsName}_bg.wasm');

    // Load the end-to-end tests.
    const { testE2eWebBrowserWasm } = await import(
        '/${rsName}-web-browser-wasm.js');

    // Run the e2e tests.
    window.CLUMP_HELPERS.runTests(log, 'standard', testE2eWebBrowserWasm, wasm);

} catch (err) {
    log(\`\\nStandard Error!\\n\${err.message}\`);
    document.body.className = 'standard-fail';
    console.error('Standard Error!', err);
}
        })();</script>

        <script type="module">(async function() {
const $log = document.getElementById('results-dataurl');
const log = msg => $log.innerHTML += '\\n' + msg;

try {

    // Use the Data URL version of the WebAssembly code.
    const wasm = await import('./${rsName}.js');
    await wasm.default(\`
${dataUrl}
    \`);

    // Load the end-to-end tests.
    const { testE2eWebBrowserWasm } = await import(
        '/${rsName}-web-browser-wasm.js');

    // Run the e2e tests.
    window.CLUMP_HELPERS.runTests(log, 'dataurl', testE2eWebBrowserWasm, wasm);

} catch (err) {
    log(\`\\nDataUrl Error!\\n\${err.message}\`);
    document.body.className = 'dataurl-fail';
    console.error('DataUrl Error!', err);
}
        })();</script>

    </body>
</html>
`}

function fromDist(filename, rsName) {
    return readFileSync(`./dist/${rsName}/web-browser-wasm/${filename}`);
}

function fromSrcCommon(rsName) {
    return readFileSync(`./src/rs-apps/${rsName}/tests/e2e/${rsName}-common.js`);
}

function fromSrcWBW(rsName) {
    return readFileSync(`./src/rs-apps/${rsName}/tests/e2e/web-browser-wasm.js`);
}

function respond(res, status, mime, content) {
    res.writeHead(status, { 'Content-Type': mime });
    res.end(content);
}
